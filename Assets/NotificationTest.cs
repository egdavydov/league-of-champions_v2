﻿using System;
using System.Collections;
using Services;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
#endif
public class NotificationTest : MonoBehaviour
{
    [SerializeField] private Text _log17;
    [SerializeField] private Text _logBy;
    [SerializeField] private Text _logLevel;
    
    public static NotificationTest Notification = null;
    public Text Text;
    
    private bool _isEnable;
    public bool GetIsEnable => _isEnable;
    public bool SetIsEnable
    {
        set
        {
            _isEnable = value;
            PlayerPrefs.SetString("Notification", _isEnable ? "true" : "false");
        }
    }

    void Awake()
    {
        string value = PlayerPrefs.GetString("Notification");
        _isEnable = value == "" || value == "true";
        if (Notification == null)
        {
            Notification = this;
        }
        else if (Notification == this)
        {
            Destroy(gameObject);
        }
        var message = string.Empty;
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidNotificationIntentData androidNotificationData = AndroidNotificationCenter.GetLastNotificationIntent();
        message = androidNotificationData.Notification.Title;
#elif UNITY_IOS && !UNITY_EDITOR
        iOSNotification iOSNotification = iOSNotificationCenter.GetLastRespondedNotification();
        message = iOSNotification.Body;
#endif
        if (message.Contains("Team needs your help to win"))
        {
            FacebookEventSubscribers.LogPush_FiveEvent();
        }
        if (message.Contains("It’s time to buy a new upgrade"))
        {
            FacebookEventSubscribers.LogPush_New_BuyEvent();
        }
        if (message.Contains("You got a new level. Go and level up!"))
        {
            FacebookEventSubscribers.LogPush_New_LevelEvent();
        }
        
        NotificationManager.ClearNotifications();
    }

    public void SeventeenPush() //1-ый push
    {
        if (!_isEnable)
            return;
        
        DateTime timeNow = DateTime.Now; //время сейчас 
        DateTime timePush;
        if (timeNow.Hour >= 17) //17 часов прошло или нет
        {
            timePush = new DateTime(timeNow.Year, timeNow.Month, timeNow.Day + 1, 17, 0, 0); //17 часов завтра
        }
        else
        {
            timePush = new DateTime(timeNow.Year, timeNow.Month, timeNow.Day, 17, 0, 0); //17 часов сегодня
        }

        TimeSpan timeDiff = timePush - timeNow; //осталось до 17 часов

        string emoji = "\uD83D\uDE4F";
        NotificationManager.SendNotification
        (
            id: 1,
            delay: (int) timeDiff.TotalSeconds,
            title: "League Of Champions",
            message: $"{emoji} Team needs your help to win {emoji}",
            color: new Color32(0xff, 0x44, 0x44, 255)
        );

        MyDebug.Log($"push will show at {timePush.Hour}:{timePush.Minute}");
    }

    public void UpgradePush(float coins, float coinsInSecond, float minPrice) //2-ой push
    {
        if (!_isEnable)
            return;
        
        var timeNow = DateTime.Now;
        DateTime timePush;

        if (timeNow.Hour >= 20) //не выпускать, если это будет за 20
        {
            timePush = timeNow.AddDays(1).Date + new TimeSpan(10, 0, 0);
        }
        else
        {
            timePush = timeNow.AddHours(3); //через 3 часа
        }
        
        TimeSpan timeDiff = timePush - timeNow;
        int timePushSecond = (int) timeDiff.TotalSeconds;
        int timeNeedBuy;

        if (minPrice < float.MaxValue)
        {
            if (minPrice <= coins) //если есть возможность купить
            {
                timeNeedBuy = 10800;
            }
            else
            {
                timeNeedBuy = (int) ((minPrice - coins) / coinsInSecond); //через какое время буду деньги на покупку
            }

            if (timeNeedBuy < timePushSecond)
            {
                string emoji = "\uD83D\uDC4F";
                
                NotificationManager.SendNotification
                (
                    id: 2,
                    delay: timePushSecond,
                    title: "League Of Champions",
                    message: "It’s time to buy a new upgrade " + emoji + " Go!",
                    color: new Color32(0xff, 0x44, 0x44, 255)
                );
                
                MyDebug.Log($"UpgradePush scheduled at {timePush.Hour}:{timePush.Minute}");
            }
        }
    }

    public void LevelPush(float curExp, float expInSecond, float required)
    {
        if (!_isEnable)
            return;
        
        DateTime timeNow = DateTime.Now;
        DateTime timePush;
        TimeSpan timeDiff;
        int timePushSecond = 0;
        int timeNeed = 0;

        if (timeNow.Hour >= 23) //уже за двадцатьтри
        {
            timePush = new DateTime(timeNow.Year, timeNow.Month, timeNow.Day + 1, 9, 0, 0); //в 9 утра
            timeDiff = timePush - timeNow;
            timePushSecond = (int) timeDiff.TotalSeconds;
        }

        timeNeed = (int) ((required - curExp) / expInSecond);
        timeNeed += (int) (timeNeed * 0.2f);
        timePushSecond = timeNeed > timePushSecond ? timeNeed : timePushSecond;

        if (timePushSecond > 40)
        {
            NotificationManager.SendNotification
            (
                id: 3,
                delay: timePushSecond,
                title: "League Of Champions",
                message: "You got a new level. Go and level up!",
                color: new Color32(0xff, 0x44, 0x44, 255)
            );
        
            _logLevel.text = $"second {timePushSecond}";
        }
        else
        {
            _logLevel.text = $"less 60 {timePushSecond}";
        }
    }

    public void OnBtn()
    {
        string emoji = "\uD83D\uDC4F"; //U+1F918
        string emoji2 = "\uD83D\uDE4F"; //

        NotificationManager.SendNotification(2, 5, "League Of Champions",
            $"It’s time to buy a new upgrade Go! {emoji} {emoji2}", new Color32(0xff, 0x44, 0x44, 255));
    }
}