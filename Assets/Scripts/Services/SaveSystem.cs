﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DB;
using Game.Managers;
using Game.States;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.SceneManagement;

/////  http://jsonviewer.stack.hu  /////

namespace Services
{
    public class SaveSystem : MonoBehaviour
    {
        private RoomsDB[] _roomsDB;
        private BaseDB _baseDB;
        private UltraBonusDB _ultraBonusDB;

        public bool ShowTutorial;
        public bool AdvConnecInInspecor;

        private List<Room> m_rooms = new List<Room>();
        private Base m_base = new Base();
        private string pathRooms;
        private string pathBase;

        public Action SavedDataLoaded;

        public void LoadData()
        {
            _baseDB = ApplicationManager.Instance.CollectionManagerRef.Base;
            _roomsDB = ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB;
            _ultraBonusDB = ApplicationManager.Instance.CollectionManagerRef.UltraBonus;

            GetPath();

            if (!File.Exists(pathRooms) || !File.Exists(pathBase))
            {
                ResetDefault();
                Save();
            }
            
            if (File.Exists(pathBase))
            {
                ReadDataBase();
            }
            if (File.Exists(pathRooms))
            {
                ReadDataRooms();
            }
            
            SavedDataLoaded.Invoke();
        }

        ////////////////////////read the file and write to the database//////////////////////////////
        private void ReadDataRooms()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            m_rooms = JsonConvert.DeserializeObject<List<Room>>(File.ReadAllText(pathRooms), settings);

            for (var i = 0; i < _roomsDB.Length; i++)
            {
                _roomsDB[i].Activate = m_rooms[i].Activate;
                _roomsDB[i].advShow = m_rooms[i].AdvShow;
                _roomsDB[i].curLevelSpeed = m_rooms[i].SpeedLevel;
                _roomsDB[i].curLevelSponsors = m_rooms[i].SponsorsLevel;
                _roomsDB[i].curLevelLeague = m_rooms[i].LeagueLevel;
                _roomsDB[i].TournamentCount = m_rooms[i].TournamentCount;
                _roomsDB[i].TournamentWins = m_rooms[i].TournamentWins;
                _roomsDB[i].TournamentWinsInRow = m_rooms[i].TournamentWinsInRow;
                _roomsDB[i].TournamentLosesInRow = m_rooms[i].TournamentLosesInRow;
                _roomsDB[i].TournamentReadyTimer = m_rooms[i].TournamentReadyTimer;

                _roomsDB[i].GetTrainer.Activate = m_rooms[i].Trainer.Activate;
                _roomsDB[i].GetTrainer.TimeLimitLevel = m_rooms[i].Trainer.TimeLimitLevel;
                _roomsDB[i].GetTrainer.RevenueLevel = m_rooms[i].Trainer.RevenueLevel;

                _roomsDB[i].CustomLogo = m_rooms[i].CustomLogo;
                _roomsDB[i].CustomTeamName = m_rooms[i].CustomTeamName;
                
                ////Gamers
                for (var j = 0; j < _roomsDB[i].GetGamers.Length; j++)
                {
                    _roomsDB[i].GetGamerByNumber(j).activate = m_rooms[i].Gamers[j].Activate;
                    _roomsDB[i].GetGamerByNumber(j).Variant = m_rooms[i].Gamers[j].Variant;
                    for (var k = 0; k < _roomsDB[i].GetGamerByNumber(j).skills.Length; k++)
                    {
                        _roomsDB[i].GetGamerByNumber(j).skills[k].curLevel = m_rooms[i].Gamers[j].Skills[k];
                    }

                    _roomsDB[i].GetGamerByNumber(j).Perks = m_rooms[i].Gamers[j].Perks;
                }

                _roomsDB[i].SetUpgradesPrice();
            }
        }

        private void ReadDataBase()
        {
            m_base = JsonConvert.DeserializeObject<Base>(File.ReadAllText(pathBase));

            _baseDB.levelBonus = m_base.LevelBonus;
            _baseDB.curCoins = m_base.Coins;
            _baseDB.curSkillPoints = m_base.SkillPoints;
            _baseDB.curExperience = m_base.Experience;
            _baseDB.curLevel = m_base.Level;
            _baseDB.SecondMoneyMultiplier = m_base.SecondMoneyMultiplier;
            _baseDB.LastMoneyMultiplier = m_base.LastMoneyMultiplier;
            _baseDB.TimeStop = m_base.TimeStop;
            _baseDB.DailyBonusDays = m_base.DailyBonusDays;
            _baseDB.LastDailyBonusTime = m_base.LastDailyBonusTime;
            _baseDB.LastUltraBonusTime = m_base.LastUltraBonusTime;
            _baseDB.PurchasedCustomLogos = m_base.PurchasedCustomLogos;
            _baseDB.SetNextLevelPrice();

            for (var i = 0; i < m_base.UltraBonuses.Count; i++)
            {
                _ultraBonusDB.UltraBonuses[i].active = m_base.UltraBonuses[i].Active;
                if(_ultraBonusDB.UltraBonuses[i].type == UltraBonusDB.UltraBonusType.Gold && _ultraBonusDB.UltraBonuses[i].active)
                    _ultraBonusDB.UltraBonuses[i].reward = m_base.UltraBonuses[i].Reward;
            }

            MyDebug.Log(_baseDB.TimeStop);
            MyDebug.Log(_baseDB.curExperience);
            MyDebug.Log(_baseDB.PriceNextLevel);
        }

        ////////////////////////read the database and write to a text file//////////////////////////////

        private void WriteDataRooms()
        {
            m_rooms.Clear();
            for (var i = 0; i < _roomsDB.Length; i++)
            {
                var room = new Room
                {
                    Number = i,
                    Activate = _roomsDB[i].Activate,
                    AdvShow = _roomsDB[i].advShow,
                    SpeedLevel = _roomsDB[i].curLevelSpeed,
                    SponsorsLevel = _roomsDB[i].curLevelSponsors,
                    LeagueLevel = _roomsDB[i].curLevelLeague,
                    TournamentCount = _roomsDB[i].TournamentCount,
                    TournamentWins = _roomsDB[i].TournamentWins,
                    TournamentWinsInRow = _roomsDB[i].TournamentWinsInRow,
                    TournamentLosesInRow = _roomsDB[i].TournamentLosesInRow,
                    TournamentReadyTimer = _roomsDB[i].TournamentReadyTimer,
                    Trainer = new Trainer
                    {
                        Activate = _roomsDB[i].GetTrainer.Activate,
                        TimeLimitLevel = _roomsDB[i].GetTrainer.TimeLimitLevel,
                        RevenueLevel = _roomsDB[i].GetTrainer.RevenueLevel
                    },
                    CustomLogo = _roomsDB[i].CustomLogo,
                    CustomTeamName = _roomsDB[i].CustomTeamName,
                    Gamers = new List<Gamer>()
                };

                for (var j = 0; j < _roomsDB[i].GetGamers.Length; j++)
                {
                    var gamer = new Gamer
                    {
                        Number = j,
                        Activate = _roomsDB[i].GetGamerByNumber(j).activate,
                        Variant = _roomsDB[i].GetGamerByNumber(j).Variant,
                        Perks = _roomsDB[i].GetGamerByNumber(j).Perks,
                        Skills = new List<int>()
                    };
                    
                    foreach (var skill in _roomsDB[i].GetGamerByNumber(j).skills)
                    {
                        gamer.Skills.Add(skill.curLevel);
                    }
                
                    room.Gamers.Add(gamer);
                }

                m_rooms.Add(room);
            }
        }

        private void WriteDataBase()
        {
            m_base.LevelBonus = _baseDB.levelBonus;
            m_base.Coins = _baseDB.curCoins;
            m_base.SkillPoints = _baseDB.curSkillPoints;
            m_base.Experience = _baseDB.curExperience;
            m_base.Level = _baseDB.curLevel;
            m_base.SecondMoneyMultiplier = _baseDB.SecondMoneyMultiplier;
            m_base.LastMoneyMultiplier = _baseDB.LastMoneyMultiplier;
            m_base.TimeStop = _baseDB.TimeStop;
            m_base.DailyBonusDays = _baseDB.DailyBonusDays;
            m_base.LastDailyBonusTime = _baseDB.LastDailyBonusTime;
            m_base.LastUltraBonusTime = _baseDB.LastUltraBonusTime;
            m_base.PurchasedCustomLogos = _baseDB.PurchasedCustomLogos;

            m_base.UltraBonuses.Clear();
            foreach (var ultraBonusDb in _ultraBonusDB.UltraBonuses)
            {
                var ultraBonus = new UltraBonus
                {
                    Active = ultraBonusDb.active, 
                    Reward = ultraBonusDb.reward
                };
                m_base.UltraBonuses.Add(ultraBonus);
            }
        }

        ////////////////////////create a save path//////////////////////////////

        private void GetPath()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        pathRooms = Path.Combine(Application.persistentDataPath, "Rooms.json");
        pathBase = Path.Combine(Application.persistentDataPath, "Base.json");
#else
            pathRooms = Path.Combine(Application.dataPath, "Rooms.json");
            pathBase = Path.Combine(Application.dataPath, "Base.json");
#endif
        }

        ////////////////////////Save//////////////////////////////

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            _baseDB.TimeStop = DateTime.Now.ToString();
            Save();
            StartPush();
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            _baseDB.TimeStop = DateTime.Now.ToString();
            Save();
            StartPush();
        }
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus)
        {
            NotificationManager.ClearNotifications();
        }
    }
    
#endif

        private void StartPush()
        {
            NotificationTest.Notification.SeventeenPush();
            NotificationTest.Notification.UpgradePush(ApplicationManager.Instance.CollectionManagerRef.Base.curCoins, SaveCurCoinsInSecond(), SaveMinPrice());
            NotificationTest.Notification.LevelPush(ApplicationManager.Instance.CollectionManagerRef.Base.curExperience, ExpInSecond(), ApplicationManager.Instance.CollectionManagerRef.Base.PriceNextLevel);
        }
  
#if UNITY_EDITOR
        public static void Reset()
        {
            var instance = new SaveSystem();
            instance.ResetDefault();
        }
        
        private void OnApplicationQuit()
        {
            Debug.Log("SaveOnQuit");
            _baseDB.TimeStop = DateTime.Now.ToString();
            //StartPush();
            Save();
        }
#endif
        public void Save()
        {
            if(SceneManager.GetActiveScene().name == GameState.SceneName)
                PlayerPrefs.SetFloat("Camera", Camera.main.transform.position.y);

            WriteDataRooms();
            var jsonRooms = JsonConvert.SerializeObject(m_rooms);
            File.WriteAllText(pathRooms, jsonRooms);

            WriteDataBase();
            var jsonBase = JsonConvert.SerializeObject(m_base);
            File.WriteAllText(pathBase, jsonBase);
        }

        private float ExpInSecond()
        {
            return ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB
                .Where(room => room.Activate)
                .Sum(room => room.GetGamers
                    .Where(gamer => gamer.activate)
                    .Sum(gamer => gamer.winMatchInSecond)
                );
        }

        private float SaveMinPrice()
        {
            var minPrice = float.MaxValue;
            foreach (var room in ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB)
            {
                if (!room.Activate) continue;
                
                if (room.CurPriceSpeed <= ApplicationManager.Instance.CollectionManagerRef.Base.curCoins)
                    return minPrice;
                
                if (room.CurPriceSponsors <= ApplicationManager.Instance.CollectionManagerRef.Base.curCoins)
                    return minPrice;
                
                if (room.CurPriceLeague <= ApplicationManager.Instance.CollectionManagerRef.Base.curCoins)
                    return minPrice;
                
                minPrice = room.CurPriceSpeed < minPrice ? room.CurPriceSpeed : minPrice;
                minPrice = room.CurPriceSponsors < minPrice ? room.CurPriceSponsors : minPrice;
                minPrice = room.CurPriceLeague < minPrice ? room.CurPriceLeague : minPrice;
            }

            return minPrice;
        }

        private float SaveCurCoinsInSecond()
        {
            var coinsInSecond = ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB
                .Where(room => room.Activate)
                .Sum(room => room.GetGamers
                    .Where(gamer => gamer.activate)
                    .Sum(gamer => gamer.coinsInSecond)
                );

            return coinsInSecond / 3;
        }

        [ContextMenu("Save")]
        public void SaveInInspector()
        {
            GetPath();
            Save();
            Debug.Log("SaveInInspector");
        }

        [ContextMenu("ResetDefault")]
        public void ResetDefault()
        {
            PlayerPrefs.SetInt("Tutor", 0);
            PlayerPrefs.DeleteKey("TutorialStep");
            PlayerPrefs.SetFloat("Camera", -3f);
        
            _baseDB = ScriptableObject.CreateInstance<BaseDB>();
            
            _baseDB.levelBonus = 100f;
            _baseDB.curCoins = 0f;
            _baseDB.curSkillPoints = 0f;
            _baseDB.curExperience = 0f;
            _baseDB.curLevel = 1;
            _baseDB.TimeStop = "";
            _baseDB.SecondMoneyMultiplier = 0;
            _baseDB.LastMoneyMultiplier = 0;
            _baseDB.DailyBonusDays = 0;
            _baseDB.LastDailyBonusTime = "";
            _baseDB.LastUltraBonusTime = "";

            _ultraBonusDB = ScriptableObject.CreateInstance<UltraBonusDB>();
            for (var i = 0; i < _ultraBonusDB.UltraBonuses.Length; i++)
            {
                _ultraBonusDB.UltraBonuses[i] = new UltraBonusDB.UltraBonus {active = false};
                if (_ultraBonusDB.UltraBonuses[i].type == UltraBonusDB.UltraBonusType.Gold)
                    _ultraBonusDB.UltraBonuses[i].reward = 0;
            }

            _roomsDB = new RoomsDB[6];
            for (var i = 0; i < _roomsDB.Length; i++)
            {
                var room = ScriptableObject.CreateInstance<RoomsDB>();
                room.Activate = false;
                room.advShow = 0;
                room.curLevelSpeed = 1f;
                room.curLevelSponsors = 1f;
                room.curLevelLeague = 1f;
                room.TournamentCount = 0;
                room.TournamentWins = 0;
                room.TournamentWinsInRow = 0;
                room.TournamentLosesInRow = 0;
                room.TournamentReadyTimer = 0;
                room.CustomLogo = null;
                room.CustomTeamName = null;

                var trainer = room.GetTrainer;
                trainer.Activate = false;
                trainer.TimeLimitLevel = 1;
                trainer.RevenueLevel = 1;

                for (var j = 0; j < room.GetGamers.Length; j++)
                {
                    var gamer = new RoomsDB.Gamer {activate = false, Perks = 0, Variant = 0};

                    for (var k = 0; k < gamer.skills.Length; k++)
                    {
                        gamer.skills[k] = new RoomsDB.Skill {curLevel = 0};
                    }
                    
                    room.GetGamers[j] = gamer;
                }

                _roomsDB[i] = room;
            }

            _roomsDB[0].Activate = true;
            _roomsDB[0].GetGamers[0].activate = true;

            GetPath();
            Save();
            Debug.Log("SaveDefault");
        }
    }

////////////////////////classes for writing//////////////////////////////

    public class Room
    {
        public int Number;
        public bool Activate;
        public int AdvShow;

        public float SpeedLevel;
        public float SponsorsLevel;
        public float LeagueLevel;
        public int TournamentCount;
        public int TournamentWins;
        public int TournamentWinsInRow;
        public int TournamentLosesInRow;
        public float TournamentReadyTimer;
        
        public Trainer Trainer = new Trainer();
        public List<Gamer> Gamers = new List<Gamer>();
        public int? CustomLogo;
        public string CustomTeamName;
    }

    public class Trainer
    {
        public bool Activate = false;
        public int TimeLimitLevel = 0;
        public int RevenueLevel = 0;
    }

    public class Gamer
    {
        public int Number;
        public bool Activate;
        public List<int> Skills;
        public int Perks;
        public int Variant;
    }

    public class UltraBonus
    {
        public float Reward;
        public bool Active;
    }

    public class Base
    {
        public float LevelBonus;
        public float Coins;
        public float SkillPoints;
        public float Experience;
        public int Level;
        public float SecondMoneyMultiplier;
        public int LastMoneyMultiplier;
        public string TimeStop;
        public int DailyBonusDays;
        public string LastDailyBonusTime;
        public string LastUltraBonusTime;
        public List<int> PurchasedCustomLogos = new List<int>();
        public List<UltraBonus> UltraBonuses = new List<UltraBonus>();
    }
}