﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIOverride : MonoBehaviour
{
    private bool _isOpen = false;

    public bool IsOpen
    {
        get => _isOpen;
        set => _isOpen = value;
    }

//    void Update()
//    {
//        isUIOverride = EventSystem.current.IsPointerOverGameObject ();
//    }

    public void Open()
    {
        _isOpen = true;
        Debug.Log(_isOpen);
    }
    
    public void Close()
    {
        _isOpen = false;
        Debug.Log(_isOpen);
    }
}
