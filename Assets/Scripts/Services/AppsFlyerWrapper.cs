﻿using Game.Managers;
using UnityEngine;

namespace Services
{
    public class AppsFlyerWrapper
    {
        private const string CallbackObjectName = "AppsFlyerTrackerCallbacks";
        public void Initialize()
        {
            var callbacksGo = new GameObject(CallbackObjectName);
            callbacksGo.AddComponent<AppsFlyerTrackerCallbacks>();
            Object.DontDestroyOnLoad(callbacksGo);
            
            AppsFlyer.setAppsFlyerKey(ApplicationManager.Instance.CollectionManagerRef.Base.AppsFlyerAppId);
            
#if APPFLYER_DEBUG
            AppsFlyer.setIsDebug (true);
#endif
            
#if UNITY_IOS
            AppsFlyer.setAppID(ApplicationManager.Instance.CollectionManagerRef.Base.AppsFlyerIosAppId);
            AppsFlyer.trackAppLaunch();
#elif UNITY_ANDROID
            AppsFlyer.setAppID (Application.identifier);
            AppsFlyer.init (ApplicationManager.Instance.CollectionManagerRef.Base.AppsFlyerAppId, CallbackObjectName);
#endif
        }
    }
}