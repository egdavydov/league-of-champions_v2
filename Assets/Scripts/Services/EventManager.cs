﻿using System.Collections;
using System.Collections.Generic;
using Tutorial.Steps;
using UnityEngine;
using UnityEngine.Events;

public class GamerWindowEvent : UnityEvent<RoomsDB, RoomsDB.Gamer>{}
public class RoomWindowEvent : UnityEvent<RoomsDB>{}
public class GamerEvent : UnityEvent<int, RoomsDB.Gamer>{}
public class CoinsEvent : UnityEvent<float>{}

public class RoomEvent : UnityEvent<int>{}
public class TutorialEvent : UnityEvent<TutorialStep>{}

public class EventManager : MonoBehaviour
{
    public static EventManager instance = null;

    public static GamerWindowEvent OpenGamerWindow = new GamerWindowEvent();
    public static GamerEvent BuyGamer = new GamerEvent();
    
    public static UnityEvent Click = new UnityEvent();
 
    public static UnityEvent AddLeagueUpgrade = new UnityEvent();
    
    public static UnityEvent ChangeCoins = new UnityEvent();
    public static UnityEvent ChangeSkill = new UnityEvent();
    public static UnityEvent ChangeLevel = new UnityEvent();
    public static UnityEvent ChangeExperience = new UnityEvent();
    public static UnityEvent ChangeSkillPoints = new UnityEvent();
    public static UnityEvent ChangePerks = new UnityEvent();
    public static UnityEvent ChangeRoomCustomization = new UnityEvent();
    
    public static UnityEvent BoostEnergetic = new UnityEvent();
    public static UnityEvent BoostEnergeticStart = new UnityEvent();
    public static UnityEvent BoostConcentration = new UnityEvent();
    public static UnityEvent BoostConcentrationStart = new UnityEvent();
    
    public static UnityEvent BoostSuperdonatStart = new UnityEvent();
    public static UnityEvent BoostSuperdonatEnd = new UnityEvent();

    public static UnityEvent UltraBonusFulled = new UnityEvent();
    
    public static RoomEvent ChangeRoom = new RoomEvent();
    public static RoomEvent BuyRoom = new RoomEvent();
    public static RoomEvent TournamentRoomReady = new RoomEvent();
    
    public static UnityEvent ChangeMultiplier = new UnityEvent();
    public static UnityEvent VibrateLight = new UnityEvent();
    public static UnityEvent VibrateMedium = new UnityEvent();
    public static UnityEvent VibrateHeavy = new UnityEvent();
    
    public static TutorialEvent TutorialStepBegin = new TutorialEvent();

    private void Awake()
    {
        //////////////////////// Singleton release //////////////////////////////

        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }
    }

}