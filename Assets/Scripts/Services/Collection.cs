using System;
using DB;
using Game.Managers;
using UnityEngine;

namespace Services
{
    public class Collection : MonoBehaviour
    {
        [SerializeField] private BaseDB _base;
        [SerializeField] private PerksDB _perks;
        [SerializeField] private RoomsDB[] _roomsDbs;
        [SerializeField] private UltraBonusDB _ultraBonusDb;

        private void Awake()
        {
            ApplicationManager.Instance.CollectionManagerRef.SetDatabases(_base, _perks, _roomsDbs, _ultraBonusDb);
        }
    }
}