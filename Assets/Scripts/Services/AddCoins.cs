﻿using System.Collections;
using System.Collections.Generic;
using Game.Managers;
using Services;
using UnityEngine;
using UnityEngine.UI;

public class AddCoins : MonoBehaviour
{
    [SerializeField] private InputField m_inputField;
    private int m_result = 0;
    private float _coins = 0;
    private float _points = 0;
    
    public void Coins()
    {
        if (!float.TryParse(m_inputField.text, out _coins))
        {
            Debug.Log("Enter number");
            return;
        }
            
        _coins = float.Parse(m_inputField.text);
        ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(_coins);
    }
    
    public void AddExp()
    {
        if (!int.TryParse(m_inputField.text, out m_result))
        {
            Debug.Log("Enter number");
            return;
        }
            
        m_result = int.Parse(m_inputField.text);
        ApplicationManager.Instance.CollectionManagerRef.Base.curExperience += m_result;
    }
    
    public void AddSkillPoints()
    {
        if (!float.TryParse(m_inputField.text, out _points))
        {
            Debug.Log("Enter number");
            return;
        }
            
        _points = float.Parse(m_inputField.text);
        ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(_points);
    }
    
    public void ChangeLevel()
    {
        if (!int.TryParse(m_inputField.text, out m_result))
        {
            Debug.Log("Enter number");
            return;
        }
            
        m_result = int.Parse(m_inputField.text);
        ApplicationManager.Instance.CollectionManagerRef.Base.curLevel = m_result;
        ApplicationManager.Instance.CollectionManagerRef.Base.SetNextLevelPrice();
        EventManager.ChangeLevel.Invoke();
    }
    
}
