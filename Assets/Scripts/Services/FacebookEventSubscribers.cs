﻿using Facebook.Unity;
using UnityEngine;

namespace Services
{
    public class FacebookEventSubscribers : MonoBehaviour
    {
        void Awake()
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                FB.Init(() =>
                {
                    FB.ActivateApp();
                });
            }
        }
        private void Start()
        {
            EventManager.BoostEnergeticStart.AddListener(LogEnergeticBoostEvent);
            EventManager.BoostSuperdonatStart.AddListener(LogDonatesBoostEvent);
            EventManager.BoostConcentrationStart.AddListener(LogWinrateBoostEvent);
            EventManager.ChangeLevel.AddListener(LogGotNewLevelEvent);
            EventManager.BuyRoom.AddListener(LogUnlockRoomNEvent);
        }
        static void LogGotNewLevelEvent()
        {
            FB.LogAppEvent("got_new_level");
        }
        static void LogEnergeticBoostEvent()
        {
            FB.LogAppEvent("energetic_boost");
        }
        static void LogWinrateBoostEvent()
        {
            FB.LogAppEvent("winrate_boost");
        }
        static void LogDonatesBoostEvent()
        {
            FB.LogAppEvent("donates_boost");
        }
        public static void LogLvlUpX3BoostEvent()
        {
            FB.LogAppEvent("lvlupx3_boost");
        }
        public static void LogOfflineBoostEvent()
        {
            FB.LogAppEvent("offlineX3_boost");
        }
        
        public static void LogAdUnlockRoomEvent(int roomNumber)
        {
            FB.LogAppEvent($"ad_unlock_room_{roomNumber}");
        }
        public static void LogUnlockRoomNEvent(int roomNumber)
        {
            FB.LogAppEvent($"unlock_room_{roomNumber}");
        }
        public static void LogUnlock_New_Gamer_N_MEvent(int room, int gamerNumber)
        {
            FB.LogAppEvent($"unlock_new_gamer_{room}_{gamerNumber}");
        }
        public static void LogUpgrade_Gamer_Max_N_MEvent(int room, int gamerNumber)
        {
            FB.LogAppEvent($"upgrade_gamer_max_{room}_{gamerNumber}");
        }
        public static void LogSpeed_Room_NEvent(int room)
        {
            FB.LogAppEvent($"speed_room_{room}");
        }
        public static void LogSponsor_Room_NEvent(int room)
        {
            FB.LogAppEvent($"sponsor_room_{room}");
        }
        public static void LogLeague_Room_NEvent(int room)
        {
            FB.LogAppEvent($"league_room_{room}");
        }
        public static void LogUpgrade_Room_N_MaxEvent(int room)
        {
            FB.LogAppEvent($"upgrade_room_max_{room}");
        }
        public static void LogPush_New_LevelEvent()
        {
            FB.LogAppEvent("push_new_level");
        }
        public static void LogPush_FiveEvent()
        {
            FB.LogAppEvent("push_five");
        }
        public static void LogPush_New_BuyEvent()
        {
            FB.LogAppEvent("push_new_buy");
        }

        public static void LogPrestigeEvent(int room, int gamer, int prestige)
        {
            FB.LogAppEvent( $"gamer_prestige_{room}_{gamer}_{prestige}");
        }
        
        public static void LogSpAdvEvent()
        {
            FB.LogAppEvent("ad_sp_view");
        }
        
        public static void LogTournamentStartEvent(int room)
        {
            FB.LogAppEvent($"tournament_start_{room}");
        }
        
        public static void LogTournamentWinEvent(int room, int win)
        {
            FB.LogAppEvent($"tournament_win_{room}_{win}");
        }
        
        public static void LogTournamentWinX2Event(int room, int win)
        {
            FB.LogAppEvent($"tournament_win_prizeX2_{room}_{win}");
        }
        
        public static void LogTournamentTryAgainEvent(int room)
        {
            FB.LogAppEvent($"tournament_deafeat_try_again_{room}");
        }
        public static void LogTournamentDefeatEvent(int room)
        {
            FB.LogAppEvent($"tournament_deafeat_{room}");
        }
        
        public static void LogTournamentDefeat7Event(int room)
        {
            FB.LogAppEvent($"tournament_deafet_7times_{room}");
        }
        
        public static void LogTrainerUnlockEvent(int room)
        {
            FB.LogAppEvent($"unlock_trainer_{room}");
        }
        
        public static void LogTrainerUpgradeTimeEvent(int room)
        {
            FB.LogAppEvent($"buy_trainer_time_{room}");
        }
        
        public static void LogTrainerUpgradePercentEvent(int room)
        {
            FB.LogAppEvent($"buy_trainer_percent_{room}");
        }
        
        public static void LogTrainerUpgradeMaxEvent(int room)
        {
            FB.LogAppEvent($"upgrade_trainer_max_{room}");
        }
        
        public static void LogRoomUpgradeAdvEvent()
        {
            FB.LogAppEvent("ad_parametrs");
        }
        
        public static void LogDailyBonusEvent(int day)
        {
            FB.LogAppEvent($"daily_bonus_collect_{day}");
        }
        
        public static void LogDailyBonusX2Event(int day)
        {
            FB.LogAppEvent($"daily_bonus_x2_{day}");
        }
        
        public static void LogMoneyX2BoostEvent(int min)
        {
            FB.LogAppEvent($"moneyx2_boost_{min}min");
        }
        
        public static void LogRoomCustomizationSaveEvent()
        {
            FB.LogAppEvent("castomization_save");
        }
        public static void LogRoomCustomizationBuyLogoEvent(int logoId)
        {
            FB.LogAppEvent($"castomization_buy_{logoId}");
        }
        public static void LogRoomBuyGamerByCoins(int room, int gamerVariant)
        {
            FB.LogAppEvent($"unlock_new_gamer_gold_{room}_{gamerVariant}");
        }

        public static void LogRoomBuyGamerBySkillPoints(int room, int gamer)
        {
            FB.LogAppEvent($"unlock_new_gamer_hard_{room}_{gamer}");
        }

        public static void LogRoomBuyGamerByAdv(int room, int gamer)
        {
            FB.LogAppEvent($"unlock_new_gamer_ad_{room}_{gamer}");
        }

        public static void LogRoomDismissGamer(int room, int gamer)
        {
            FB.LogAppEvent($"dismiss_gamer_{room}_{gamer}");
        }
        
        public static void LogRewardUltraBonus(int number)
        {
            FB.LogAppEvent($"ad_ulta_{number}");
        }
        
        public static void LogFillTournament(int room)
        {
            FB.LogAppEvent($"ad_tournament_scale_speedup_{room}");
        }
    }
}
