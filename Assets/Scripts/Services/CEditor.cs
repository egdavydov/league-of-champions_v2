﻿
using Services;
#if UNITY_EDITOR
using UnityEngine;
   using System.Collections;
   using UnityEditor;
   
   
   [CustomEditor(typeof(SaveSystem))]
   public class CEditor : Editor
   {
       public override void OnInspectorGUI()
       {
           DrawDefaultInspector();
   
           SaveSystem saveSystem = (SaveSystem) target;
           if (GUILayout.Button("Save"))
           {
               saveSystem.SaveInInspector();
           }
   
           if (GUILayout.Button("Reset Default"))
           {
               saveSystem.ResetDefault();
           }
       }
   }
#endif