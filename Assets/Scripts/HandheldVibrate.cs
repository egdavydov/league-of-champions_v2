﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandheldVibrate : MonoBehaviour
{
    private bool _isEnable;

    public bool GetIsEnable => _isEnable;
    public bool SetIsEnable
    {
        set
        {
            _isEnable = value;
            PlayerPrefs.SetString("Vibrate", _isEnable ? "true" : "false");
        }
    }
    private void Awake()
    {
        _isEnable = false;
        string value = PlayerPrefs.GetString("Vibrate");
        _isEnable = value == "" || value == "true";

        EventManager.VibrateLight.AddListener(Vibrate);
    }

    public void Vibrate()
    {
        if (_isEnable)
        {
            Handheld.Vibrate();
        }
    }

    public void Vibrate5Sec()
    {
        StartCoroutine(CorVibrate());
    }

    public void VibrateEvent()
    {
        EventManager.VibrateLight.Invoke();
    }

    private IEnumerator CorVibrate()
    {
        WaitForSeconds delay = new WaitForSeconds(0.1f);
        float timer = 0;
        while (true)
        {
            if (timer >= 5f)
                yield break;

            Vibrate();
            yield return delay;
            timer += 0.1f;
        }
    }

    private void OnDestroy()
    {
        EventManager.VibrateLight.RemoveListener(Vibrate);
    }
}