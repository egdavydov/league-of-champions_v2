﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Advertising
{
    public class AdvertisingBtn : MonoBehaviour
    {
        public UnityEvent Reward = new UnityEvent();

        [SerializeField] private GameObject _enable;
        [SerializeField] private GameObject _disable;
        [SerializeField] private TextMeshProUGUI _price;

        public void ShowBtn()
        {
            Refresh();
            MaxAppLovin.Instance.CashRevard.AddListener(Refresh);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshOnLoad;
        }

        void RefreshOnLoad(string str)
        {
            Refresh();
        }

        private void Refresh()
        {
            bool enable = MaxAppLovin.Instance.IsRewardedAdReady;
            _enable.SetActive(enable);
            _disable.SetActive(!enable);
        }

        public void SetPrice(string price)
        {
            _price.text = price;
        }

        public void OnBtn()
        {
            if (MaxAppLovin.Instance.IsRewardedAdReady)
            {
                MaxAppLovin.Instance.HiddenRevard.AddListener(GetReward);
                MaxAppLovin.Instance.ShowRewardedInterstitial();
            }
        }

        private void GetReward()
        {
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(GetReward);
            Reward.Invoke();
        }

        private void OnDisable()
        {
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshOnLoad;
            MaxAppLovin.Instance.CashRevard.RemoveListener(Refresh);
        }
    }
}
