﻿using Game.Managers;
using Rooms;
using Services;
using UnityEngine;

namespace Advertising
{
    public class AdvertisingActivationRoom : MonoBehaviour
    {
        [SerializeField] private AdvertisingBtn _levelAdvBtn;
        [SerializeField] private AdvertisingBtn _priceAdvBtn;
        private ActivateRoom _activateRoom;
        private RoomsDB _roomDB;
        private bool _showLevelAdvBtn = false;
    
        private void Start()
        {
            _activateRoom = GetComponentInParent<ActivateRoom>();
            _roomDB = _activateRoom.GetRoomDB;
        
            if (_roomDB.Activate)
                return;
        
            _levelAdvBtn.gameObject.SetActive(false);
            _priceAdvBtn.gameObject.SetActive(false);

            if (_roomDB.LevelAccess > ApplicationManager.Instance.CollectionManagerRef.Base.curLevel)
            {
                Refresh();
                EventManager.ChangeLevel.AddListener(Refresh);
                return;
            }
        
            if (_roomDB.LevelAccess <= ApplicationManager.Instance.CollectionManagerRef.Base.curLevel)
            {
                ShowAdvPrice();
            }
        }

        private void Refresh()
        {
            CheckVideoCount();
        
            if (_roomDB.LevelAccess <= ApplicationManager.Instance.CollectionManagerRef.Base.curLevel)
            {
                EventManager.ChangeLevel.RemoveListener(Refresh);
            
                if (_showLevelAdvBtn)
                {
                    HideAdvLevels();
                }

                ShowAdvPrice();
                return;
            }
        
            if ((_roomDB.LevelAccess <= (ApplicationManager.Instance.CollectionManagerRef.Base.curLevel + 5)) && !_showLevelAdvBtn)
            {
                ShowAdvLevels();
                _showLevelAdvBtn = true;
            }
        }
    

        public void ShowAdvLevels()
        {
            _levelAdvBtn.gameObject.SetActive(true);
            _levelAdvBtn.SetPrice(_roomDB.advShow + "/3");
            _levelAdvBtn.ShowBtn();
            _levelAdvBtn.Reward.AddListener(Reward);
        }

        public void ShowAdvPrice()
        {
            _priceAdvBtn.gameObject.SetActive(true);
            _priceAdvBtn.SetPrice(_roomDB.advShow + "/2");
            _priceAdvBtn.ShowBtn();
            _priceAdvBtn.Reward.AddListener(Reward);
        }

        private void Reward()
        {
            _roomDB.advShow++;
            _levelAdvBtn.SetPrice(_roomDB.advShow + "/3");
            _priceAdvBtn.SetPrice(_roomDB.advShow + "/2");
    
            FacebookEventSubscribers.LogAdUnlockRoomEvent(_roomDB.Number);
            
            CheckVideoCount();
        }

        private void CheckVideoCount()
        {
            if (_roomDB.LevelAccess <= ApplicationManager.Instance.CollectionManagerRef.Base.curLevel)
            {
                if (_roomDB.advShow >= 2)
                {
                    EventManager.ChangeLevel.RemoveListener(Refresh);
                    _activateRoom.OnBuyRoomBtn(false);
                }
            }
            else
            {
                if (_roomDB.advShow >= 3)
                {
                    EventManager.ChangeLevel.RemoveListener(Refresh);
                    _activateRoom.OnBuyRoomBtn(false);
                }
            }
        }

        public void HideAdvLevels()
        {
            _levelAdvBtn.Reward.RemoveListener(Reward);
            _levelAdvBtn.gameObject.SetActive(false);
        }
    
        public void HideAdvPrice()
        {
            _priceAdvBtn.Reward.RemoveListener(Reward);
            _priceAdvBtn.gameObject.SetActive(false);
        }
    }
}
