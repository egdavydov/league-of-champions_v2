﻿using Game.Managers;
using Game.States;
using Services;
using UnityEngine;
using UnityEngine.Events;

namespace Advertising
{
    public class MaxAppLovin : MonoBehaviour
    {
        public static MaxAppLovin Instance;

        private SaveSystem _saveSystem;
        private AdvertisinInInspector _advertisinInInspector;

        private bool _isPreloadingRewardedVideo = false;
        private bool _loading = false;
        private float _delayBetweenAd;
        private string _interstitialAdId;
        private float _interstitialTimer;
        private bool _interstitialShown;
        private bool _rewardedAdShown;

        public bool IsPreloadingRewardedVideo
        {
            get => _isPreloadingRewardedVideo;
            set
            {
                _isPreloadingRewardedVideo = value;
                CashRevard.Invoke();
            }
        }

        public bool IsPreloadingInterstitialVideo { get; private set; }

        public UnityEvent CashRevard = new UnityEvent();
        public UnityEvent HiddenRevard = new UnityEvent();

        public bool IsRewardedAdReady => MaxSdk.IsRewardedAdReady(RewardedAdUnitId);

        private string RewardedAdUnitId
        {
            get
            {
#if UNITY_ANDROID
                return ApplicationManager.Instance.CollectionManagerRef.Base.AndroidRV;
#else
                return ApplicationManager.Instance.CollectionManagerRef.Base.IosRv;
#endif
            }
        }
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance == this)
            {
                Destroy(gameObject);
            }

#if UNITY_EDITOR
            _saveSystem = ApplicationManager.Instance.SaveSystemRef;
            _advertisinInInspector = GameObject.Find("AdvertisingInInspector").GetComponent<AdvertisinInInspector>();
            _advertisinInInspector.FinishAdv.AddListener(() =>
            {
                MyDebug.Log("Max Hidden Inspector");
                HiddenRevard.Invoke();
                if (_interstitialShown)
                {
                    _interstitialShown = false;
                    _interstitialTimer = 0;
                }

                if (_rewardedAdShown)
                {
                    _rewardedAdShown = false;
                }
            });
#endif
            Init();
        }

        private void Init()
        {
            _delayBetweenAd = ApplicationManager.Instance.CollectionManagerRef.Base.DelayBetweenAd;
#if UNITY_ANDROID
            _interstitialAdId = ApplicationManager.Instance.CollectionManagerRef.Base.AndroidInterstitialAdId;
#else
            _interstitialAdId = ApplicationManager.Instance.CollectionManagerRef.Base.IosInterstitialAdId;
#endif

            
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            // Set SDK key and initialize SDK
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                // AppLovin SDK is initialized, start loading ads
                InitializeRewardedAds();
                InitializeInterstitialAds();
                MaxSdk.VariableService.LoadVariables();
                var maxVar = MaxSdk.VariableService.GetString("Variable");
                MyDebug.Log($"Max variable = '{maxVar}'");
                int.TryParse(MaxSdk.VariableService.GetString("Variable"), out var maxParseVar);
                if (maxParseVar == 1)
                {
                    _delayBetweenAd = 2000;
                }
            };

            MaxSdk.SetSdkKey(ApplicationManager.Instance.CollectionManagerRef.Base.SdkKey);
            MaxSdk.InitializeSdk();
#endif
            
            LoadRewardedAd();
            LoadInterstitialAdv();
        }

        private void Update()
        {
            if(_interstitialShown || _rewardedAdShown)
                return;
            
            _interstitialTimer += Time.deltaTime;
            if (_interstitialTimer >= _delayBetweenAd && ApplicationManager.Instance.StatesManager.ActiveState.Id == (int)GameStates.GameState)
            {
                ShowInterstitial();
            }
            
#if DEVELOPMENT
            if (Input.touchCount >= 5)
            {
                MaxSdk.ShowMediationDebugger();
            }
#endif
        }

        private void InitializeRewardedAds()
        {
            // Attach callback
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
            MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
            MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
            MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
            MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

            // Load the first RewardedAd
            LoadRewardedAd();
        }

        private void InitializeInterstitialAds()
        {
            MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
            MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialLoadFailedEvent;
            MaxSdkCallbacks.OnInterstitialDisplayedEvent += OnInterstitialDisplayedEvent;
            MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialHiddenEvent;
            
            LoadInterstitialAdv();
        }

        private void LoadRewardedAd()
        {
            MaxSdk.LoadRewardedAd(RewardedAdUnitId);

#if UNITY_EDITOR
            ApplicationManager.Instance.AudioManager.UnMute();
            if (!_saveSystem.AdvConnecInInspecor)
            {
                return;
            }
#endif
            IsPreloadingRewardedVideo = true;
            MyDebug.Log("Max Preloading rewarded ad...");
        }

        private void LoadInterstitialAdv()
        {
            MyDebug.Log("LoadInterstitial");
            MaxSdk.LoadInterstitial(_interstitialAdId);
#if UNITY_EDITOR
            ApplicationManager.Instance.AudioManager.UnMute();
            if (!_saveSystem.AdvConnecInInspecor)
            {
                return;
            }
#endif
            IsPreloadingInterstitialVideo = true;
        }

        private void OnRewardedAdLoadedEvent(string adUnitId)
        {
            // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
        }

        private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
        {
            // Rewarded ad failed to load. We recommend re-trying in 3 seconds.
            Invoke(nameof(LoadRewardedAd), 3f);
        }

        private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
        {
            // Rewarded ad failed to display. We recommend loading the next ad
            LoadRewardedAd();
        }

        private void OnRewardedAdDisplayedEvent(string adUnitId)
        {
        }

        private void OnRewardedAdClickedEvent(string adUnitId)
        {
        }

        private void OnRewardedAdDismissedEvent(string adUnitId)
        {
            ApplicationManager.Instance.AudioManager.UnMute();
            // Rewarded ad is hidden. Pre-load the next ad
            MyDebug.Log("Max Hidden");
            _rewardedAdShown = false;
            _interstitialTimer = 0f;
            HiddenRevard.Invoke();
            LoadRewardedAd();
        }

        private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
        {
            // Rewarded ad was displayed and user should receive the reward
        }

        private void OnInterstitialDisplayedEvent(string adUnitId)
        {
            MyDebug.Log("OnInterstitialDisplayedEvent");
        }

        private void OnInterstitialHiddenEvent(string adUnitId)
        {
            ApplicationManager.Instance.AudioManager.UnMute();
            _interstitialTimer = 0f;
            _interstitialShown = false;
            MyDebug.Log("OnInterstitialHiddenEvent");
            LoadInterstitialAdv();
        }

        private void OnInterstitialLoadedEvent(string adUnitId)
        {
            MyDebug.Log("OnInterstitialLoadedEvent");
        }
        
        private void OnInterstitialLoadFailedEvent(string adUnitId, int errorCode)
        {
            MyDebug.Log($"OnInterstitialLoadFailedEvent error - {errorCode}");
            Invoke(nameof(LoadInterstitialAdv), 3);
        }

        private void ShowInterstitial()
        {
            ApplicationManager.Instance.AudioManager.Mute();
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            if (MaxSdk.IsInterstitialReady(_interstitialAdId))
            {
                MaxSdk.ShowInterstitial(_interstitialAdId);
                _interstitialShown = true;
                IsPreloadingInterstitialVideo = false;
            }
#endif
#if UNITY_EDITOR
            _advertisinInInspector.StartAdv();
            _interstitialShown = true;
            IsPreloadingInterstitialVideo = false;
            LoadInterstitialAdv();
#endif
        }
        
        public void ShowRewardedInterstitial()
        {
            ApplicationManager.Instance.AudioManager.Mute();
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        if (MaxSdk.IsRewardedAdReady(RewardedAdUnitId))
        {
            MaxSdk.ShowRewardedAd(RewardedAdUnitId);
            IsPreloadingRewardedVideo = false;
            _rewardedAdShown = true;
            _interstitialTimer = 0f;
        }
#endif
#if UNITY_EDITOR
            _advertisinInInspector.StartAdv();
            IsPreloadingRewardedVideo = false;
            _rewardedAdShown = true;
            _interstitialTimer = 0f;
            LoadRewardedAd();
#endif
        }
    }
}