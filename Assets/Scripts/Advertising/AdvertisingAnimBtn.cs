﻿using System;
using Game.Managers;
using UnityEngine;
using UnityEngine.Events;

namespace Advertising
{
    public class AdvertisingAnimBtn : MonoBehaviour
    {
        public UnityEvent Reward = new UnityEvent();
        public UnityEvent Show = new UnityEvent();
        public UnityEvent Hide = new UnityEvent();

        private Animator _animator;

        private ABoostWidget _boostWidget;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _boostWidget = GetComponent<ABoostWidget>();
        }

        public void ShowBtn()
        {
            Refresh();
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshOnLoad;
        }

        void RefreshOnLoad(string str)
        {
            Refresh();
        }
        private void Refresh()
        {
            if (MaxAppLovin.Instance.IsRewardedAdReady)
            {
                if (!_animator.GetBool("Open"))
                {
                    _animator.SetBool("Open", true);
                }
                //AdvertisingSystem.Adv.CashRevard.RemoveListener(Refresh);
                Show.Invoke();
            }
            else
            {
                if (_animator.GetBool("Open"))
                {
                    _animator.SetBool("Open", false);
                }
                Hide.Invoke();
            }
        }

        public void OnBtn()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
            
            EventManager.VibrateLight.Invoke();
            if (_boostWidget == null)
            {
                ShowAdvertisement();
                return;
            }

            var isActivated = PlayerPrefs.GetInt(_boostWidget.BoostType().ToString(), 0);
            if (isActivated == 0)
            {
                ApplicationManager.Instance.UISystem.Show(ViewType.BoostInfoView, new BoostsInfoWindowParams(_boostWidget, ShowAdvertisement));    
            }
            else
            {
                ShowAdvertisement();
            }
        }

        private void ShowAdvertisement()
        {
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshOnLoad;
            MaxAppLovin.Instance.CashRevard.RemoveListener(Refresh); 
            MaxAppLovin.Instance.HiddenRevard.AddListener(GetReward);
            MaxAppLovin.Instance.ShowRewardedInterstitial();

            _animator.SetBool("Open", false);
            Hide.Invoke();
        }

        private void GetReward()
        {
            MyDebug.Log("GetReward");
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(GetReward);


            switch (_boostWidget.BoostType())
            {
                case BoostsInfoWindowController.BoostType.Energetic:
                    ApplicationManager.Instance.AudioManager.PauseAmbient(AmbientType.MainTheme);
                    ApplicationManager.Instance.AudioManager.PlaySound(SoundType.BoostEnergetic);
                    break;
                case BoostsInfoWindowController.BoostType.Concentration:
                    ApplicationManager.Instance.AudioManager.PauseAmbient(AmbientType.MainTheme);
                    ApplicationManager.Instance.AudioManager.PlaySound(SoundType.BoostWinrate);
                    break;
                case BoostsInfoWindowController.BoostType.Superdonat:
                    ApplicationManager.Instance.AudioManager.PauseAmbient(AmbientType.MainTheme);
                    ApplicationManager.Instance.AudioManager.PlaySound(SoundType.BoostDonate);
                    break;
            }
            
            Reward.Invoke();
        }

        public void HideBtn()
        {
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshOnLoad;
            MaxAppLovin.Instance.CashRevard.RemoveListener(Refresh); 
            if (_animator.GetBool("Open"))
            {
                _animator.SetBool("Open", false);
            }
        }
        private void OnDisable()
        {
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshOnLoad;
            MaxAppLovin.Instance.CashRevard.RemoveListener(Refresh);
        }
    }
}
