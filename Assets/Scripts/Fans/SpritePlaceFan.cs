﻿using UnityEngine;

namespace Fans
{
    public class SpritePlaceFan : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer m_torso;
        [SerializeField] private SpriteRenderer m_head;
        [SerializeField] private SpriteRenderer[] m_hands;

        [SerializeField] private Sprite[] m_spritesHeads;

        public void AssignFan()
        {
            Color color = new Color
            (
                Random.Range(0f, 1f),
                Random.Range(0f, 1f),
                Random.Range(0f, 1f)
            );
            m_torso.color = color;
            foreach (var hand in m_hands)
            {
                hand.color = color;
            }

            m_head.sprite = m_spritesHeads[Random.Range(0, m_spritesHeads.Length)];

        }
    }
}