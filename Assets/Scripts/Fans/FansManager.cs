﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Fans
{
    public class FansManager : MonoBehaviour
    {
        private FanRelease[] _allFans;
        private List<FanRelease> _playFan = new List<FanRelease>();
        private List<FanRelease> _limitFan = new List<FanRelease>();

        private Coroutine _fans;
        private int _limitFanCount;
        private bool _boostEnergetic = false;

        private RoomsDB _roomDB;
        public RoomsDB GetRoom => _roomDB;

        public List<FanRelease> GetPlayFans => _playFan;

        public void Enable(RoomsDB roomDB)
        {
            _roomDB = roomDB;
            //EventManager.AddLeagueUpgrade.AddListener(LimitFan);
            EventManager.BoostEnergetic.AddListener(BoostEnergetic);
            EventManager.BoostSuperdonatStart.AddListener(SuperDonat);
            _allFans = GetComponentsInChildren<FanRelease>();
        
            _fans = StartCoroutine(Fans());
        }

        public void LimitFan()
        {
            _limitFan.Clear();
            _limitFanCount = (int) Math.Ceiling((_roomDB.curLevelLeague / 2f)) + 1;
            if (_limitFanCount > 7)
            {
                for (int i = 0; i < _allFans.Length; i++)
                {
                    _limitFan.Add(_allFans[i]);
                }
            }
            else
            {
                for (int i = 0; i < 7; i++)
                {
                    _limitFan.Add(_allFans[i]);
                }
            }

            foreach (var playFan in _playFan)
            {
                _limitFan.Remove(playFan);
            }
        }

        private void SuperDonat()
        {
            foreach (var playFan in _playFan)
            {
                playFan.ReleaseFan(true);
            }
        
            while (_playFan.Count < _limitFanCount)
            {
                int i;
                i = Random.Range(0, _limitFan.Count);
                
                var fan = _limitFan[i];
                fan.ReleaseFan(true);
                _limitFan.Remove(fan);
                _playFan.Add(fan);
            }
        }

        private IEnumerator Fans()
        {
            LimitFan();
            while (true)
            {
                if (_playFan.Count < _limitFanCount && !_boostEnergetic && _limitFan.Count > 0)
                {
                    var i = Random.Range(0, _limitFan.Count-1);
                    var fan = _limitFan[i];
                    fan.ReleaseFan(false);
                    _limitFan.Remove(fan);
                    _playFan.Add(fan);
                }

                var delay = Random.Range(1, 3);
                yield return new WaitForSeconds(delay);
            }
        }

        public void ReturnFan(FanRelease fan)
        {
            _limitFan.Insert(0, fan);
            _playFan.Remove(fan);
        }

        private void BoostEnergetic()
        {
            _boostEnergetic = !_boostEnergetic;
        }

        private void OnDestroy()
        {
            //EventManager.AddLeagueUpgrade.RemoveListener(LimitFan);
            EventManager.BoostSuperdonatStart.RemoveListener(SuperDonat);
            EventManager.BoostEnergetic.AddListener(BoostEnergetic);
        }
    }
}