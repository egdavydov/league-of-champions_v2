﻿using System.Collections;
using UnityEngine;

namespace Fans
{
    public class StarsFX : MonoBehaviour
    {
        [SerializeField] private float _delay;
        private Animator[] _anims;
        private WaitForSeconds _delayWait;
        private Transform[] _transforms;


        private void Start()
        {
            _anims = GetComponentsInChildren<Animator>();
            _transforms = new Transform [_anims.Length];
            for (int i = 0; i < _transforms.Length; i++)
            {
                _transforms[i] = _anims[i].GetComponent<Transform>();
            }

            _delayWait = new WaitForSeconds(_delay);
        }
    
        public IEnumerator Play()
        {
            Vector3 position;
            for (int i = 0; i < _anims.Length; i++)
            {
                position = new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(-0.2f, 0.2f), 0f);
                _transforms[i].localPosition = position;
                _anims[i].SetTrigger("Play");
                yield return _delayWait;
            }
        }
    }
}