﻿using System.Collections;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;

namespace Fans
{
    public class FanRelease : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_donatTxt;
        [SerializeField] private TextMeshProUGUI m_commentTxt;
        [SerializeField] private GameObject m_textWin;
        [SerializeField] private GameObject m_donatWin;
        [SerializeField] private string[] _comments;

        [Header("SuperDonatSeup")]
        [SerializeField] private float _minDelay = 0.5f;
        [SerializeField] private float _maxDelay = 1.5f;

        private Animator _animator;
        private SpritePlaceFan _spriteFan;
        private FansManager _fansManager;

        private Coroutine _applause;
        private Coroutine _comment;
        private Coroutine _nextLeague;
        private Coroutine _release;

        private StarsFX _starsFx;

        private float m_donat;

        private bool IsAnimationStop()
        {
            var animatorStateInfo = _animator.GetCurrentAnimatorStateInfo(1);
            if (animatorStateInfo.IsName("Close") || animatorStateInfo.IsName("Idle"))
                return true;

            return false;
        }

        private void Awake()
        {
            Destroy(GetComponentInChildren<CircleCollider2D>().gameObject);
            Destroy(GetComponent<Collider2D>());
            _animator = GetComponent<Animator>();
            _spriteFan = GetComponent<SpritePlaceFan>();
            _fansManager = GetComponentInParent<FansManager>();
            _starsFx = GetComponentInChildren<StarsFX>();
        }

        public void PlayStarsFX()
        {
            StartCoroutine(_starsFx.Play());
        }

        private void GenerateDonat()
        {
            float value = Mathf.Clamp(
                value: _fansManager.GetRoom.curBaseBetLeague * Random.Range(0.3f, 1.3f),
                min: 1,
                max: float.MaxValue);

            m_donatTxt.text = Reduction.ReductionNumber(value, 0, true);
            m_donat = value;
        }

        public void ReleaseFan(bool boost)
        {
            if (_release != null)
            {
                StopCor(_release);
                StopCor(_comment);
                StopCor(_applause);
            }

            _release = StartCoroutine(Release());
            _applause = StartCoroutine(Applause());
            _comment = StartCoroutine(Comment(boost));
        }

        private IEnumerator Release()
        {
            EventManager.BoostEnergetic.AddListener(MoveBack);
            EventManager.BoostSuperdonatEnd.AddListener(() =>
            {
                StopCor(_comment);
                _comment = StartCoroutine(Comment(false));
            });

            //_spriteFan.AssignFan();
            _animator.SetBool("Release", true);

            int delay = Random.Range(20, 31);
            yield return new WaitForSeconds(delay);

            MoveBack();
        }

        private IEnumerator Applause(bool onBtn = false)
        {
            int delay;
        
            if (onBtn)
            {
                _animator.SetBool("Applause", true);
                delay = Random.Range(1, 5);
                yield return new WaitForSeconds(delay);
                _animator.SetBool("Applause", false);
                yield break;
            }

            while (true)
            {
                delay = Random.Range(1, 5);
                yield return new WaitForSeconds(delay);

                delay = Random.Range(1, 5);
                _animator.SetBool("Applause", true);
                yield return new WaitForSeconds(delay);
                _animator.SetBool("Applause", false);
            }
        }

        private IEnumerator Comment(bool boost, bool onBtn = false)
        {
            if (onBtn)
            {
                Debug.Log("Play");
                PlayComment();

                yield return new WaitForSeconds(5f);
                _animator.SetBool("Comment", false);
                yield break;
            }

            if (boost)
            {
                if (_animator.GetBool("Comment"))
                {
                    _animator.SetBool("Comment", false);
                }

                float delay = Random.Range(_minDelay, _maxDelay);
                yield return new WaitForSeconds(delay);

                m_donatWin.SetActive(true);
                m_textWin.SetActive(false);

                while (true)
                {
                    GenerateDonat();
                    _animator.SetTrigger("Boost");
                    ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(m_donat);

                    delay = Random.Range(_minDelay, _maxDelay);
                    yield return new WaitForSeconds(delay);
                }
            }
            else
            {
                float delay = Random.Range(10, 15);
                yield return new WaitForSeconds(delay);

                PlayComment();

                yield return new WaitForSeconds(5f);
                _animator.SetBool("Comment", false);
            }
        }

        private void PlayComment()
        {
            _animator.SetBool("Comment", true);

            int value = Random.Range(0, 101);
            bool activate = value <= 80;
            m_textWin.SetActive(activate);
            m_donatWin.SetActive(!activate);

            if (activate)
            {
                value = Random.Range(0, _comments.Length - 1);
                m_commentTxt.text = _comments[value];
            }
            else
            {
                GenerateDonat();
            }
        }

        private void StopCor(Coroutine cor)
        {
            if (cor == null) return;
            StopCoroutine(cor);
            //cor = null;
        }

        private bool IsCoroutinePlay(Coroutine cor, bool stopCor = true)
        {
            if (cor == null)
            {
                return false;
            }

            if (stopCor)
            {
                StopCoroutine(cor);
            }

            return true;
        }

        public void OnDonatBtn()
        {
            StopCor(_comment);
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(m_donat);
            _animator.SetBool("Comment", false);
        }

        public void OnFanBtn()
        {
            EventManager.VibrateLight.Invoke();

            if (!IsAnimationStop())
                return;

            StopCor(_comment);
            StopCor(_applause);

            _comment = StartCoroutine(Comment(false, true));
            _applause = StartCoroutine(Applause(true));
        }


        public void NextLeague()
        {
            _nextLeague = StartCoroutine(NextLeagueCor());
        }

        private IEnumerator NextLeagueCor()
        {
            StopCor(_applause);
            _animator.SetBool("Applause", true);
            yield return new WaitForSeconds(3);
            _animator.SetBool("Applause", false);
            _applause = StartCoroutine(Applause());
        }

        public void MoveBack()
        {
            StopCor(_release);
            StopCor(_applause);
            StopCor(_comment);
            StopCor(_nextLeague);

            _animator.SetBool("Applause", false);
            _animator.SetBool("Comment", false);
            _animator.SetBool("Release", false);

            _fansManager.ReturnFan(this);

            EventManager.BoostEnergetic.RemoveListener(MoveBack);
            EventManager.BoostSuperdonatEnd.RemoveListener(() =>
            {
                StopCor(_comment);
                _comment = StartCoroutine(Comment(false));
            });
        }
    }
}