using System;
using System.Collections;
using System.Collections.Generic;
using Core.Common.Managers;
using Game.Managers;
using Game.States;
using Services;
using UI.Tournament;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Tournament.Managers
{
    public class TournamentManager : Manager
    {
        public static TournamentManager Instance => _instance ?? (_instance = new TournamentManager());
        private static TournamentManager _instance;
        
        public float EnemyTapWaitTime { get; private set; }
        public int TournamentGameCounts { get; set; }
        public bool TournamentStarted { get; set; }
        public TournamentUI UI { get; set; }
        public int LostCount { get; private set; }
        
        private int _yourScore;
        private int _enemyScore;
        private int _enemyTapInSeconds;
        
        private List<int> _yourGamersWinRate;
        private List<int> _enemyGamersWinRate;
        
        public Action WinAction; 
        public Action LoseAction;
        public Action TournamentAgainAction;
        
        public static Action<int> GamerWinAction;
        public static Action<int> GamerLoseAction;

        public override void Init()
        {
            _yourGamersWinRate = new List<int>();
            _enemyGamersWinRate = new List<int>();
            ApplicationManager.Instance.TournamentManagerRef = this;
            foreach (var gamer in ApplicationManager.Instance.CurrentRoom.GetGamers)
            {
                if (!gamer.activate)
                    continue;
                
                _yourGamersWinRate.Add(gamer.curWinRate);
                _enemyGamersWinRate.Add(GenerateEnemyGamerWinrate(gamer.curWinRate));
            }

            _enemyTapInSeconds = GetEnemyTapInSecond();
            EnemyTapWaitTime = 1 / (float)_enemyTapInSeconds;
            LostCount = 0;
        }

        public override IEnumerator InitAsync()
        {
            yield return null;
        }

        public override void Deinit()
        {
        }
        
        private int GetEnemyTapInSecond()
        {
            if (TournamentGameCounts == 0)
                return 1;
                    
            if (ApplicationManager.Instance.CurrentRoom.TournamentLosesInRow >= 3)
            {
                return Random.Range(2, 5);
            }

            if (ApplicationManager.Instance.CurrentRoom.TournamentWinsInRow == 5)
                return Random.Range(5, 8);
            if (ApplicationManager.Instance.CurrentRoom.TournamentWinsInRow == 6)
                return Random.Range(6, 10);
            if (ApplicationManager.Instance.CurrentRoom.TournamentWinsInRow >= 7)
                return Random.Range(7, 12);

            return Random.Range(2, 7);
        }
        
        private static int GenerateEnemyGamerWinrate(int winRate)
        {
            var variant = Random.Range(0, 2);
            switch (variant)
            {
                case 0:
                    return winRate - Random.Range(ApplicationManager.Instance.CollectionManagerRef.Base.TournamentEnemyWinRateLessDeltaMin, ApplicationManager.Instance.CollectionManagerRef.Base.TournamentEnemyWinRateLessDeltaMax);
                case 1:
                    return winRate + Random.Range(ApplicationManager.Instance.CollectionManagerRef.Base.TournamentEnemyWinRateLargerDeltaMin, ApplicationManager.Instance.CollectionManagerRef.Base.TournamentEnemyWinRateLargerDeltaMax);
                default:
                    return winRate;
            }
        }
        
        public void ResultYourMatch()
        {
            for (var i = 0; i < _yourGamersWinRate.Count; i++)
            {
                var winrate = _yourGamersWinRate[i];
                var chance = Random.Range(0, 101);

                if (chance <= winrate)
                {
                    _yourScore++;
                    UI.SetYourScores(_yourScore);
                    GamerWinAction.Invoke(i);
                }
                else
                {
                    GamerLoseAction.Invoke(i);
                }
            }
        }

        public void WinYourMatch()
        {
            for (var i = 0; i < _yourGamersWinRate.Count; i++)
            {
                _yourScore++;
                UI.SetYourScores(_yourScore);
                GamerWinAction.Invoke(i);
            }
        }

        public void ResultEnemyMatch()
        {
            foreach (var winrate in _enemyGamersWinRate)
            {
                var chance = Random.Range(0, 101);

                if (chance > winrate) continue;
                
                _enemyScore++;
                UI.SetEnemyScores(_enemyScore);
            }
        }
        
        public void StartTournament()
        {
            if(TournamentStarted)
                return;
            
            Debug.Log("Start Tournament");
            PrepareScore();
            TournamentStarted = true;
            UI.ShowCritManager(true);
            UI.GamerAnimate(true);
            FacebookEventSubscribers.LogTournamentStartEvent(ApplicationManager.Instance.CurrentRoom.Number);
            ApplicationManager.Instance.AudioManager.PlayAmbient(AmbientType.TournamentMain, true);
        }

        public void PrepareScore()
        {
            _yourScore = 0;
            _enemyScore = 0;
            UI.SetYourScores(_yourScore);
            UI.SetEnemyScores(_enemyScore);
        }

        public void StopTournament()
        {
            if(!TournamentStarted)
                return;
            
            Debug.Log("Stop Tournament");
            TournamentStarted = false;
            TournamentGameCounts++;
            UI.ShowCritManager(false);
            UI.GamerAnimate(false);
            
            ApplicationManager.Instance.CurrentRoom.TournamentCount++;
            
            if (_yourScore == _enemyScore)
            {
                _yourScore++;
                UI.SetYourScores(_yourScore);
            }
            
            if (_yourScore > _enemyScore)
            {
                ApplicationManager.Instance.CurrentRoom.TournamentWins++;
                ApplicationManager.Instance.CurrentRoom.TournamentWinsInRow++;
                ApplicationManager.Instance.CurrentRoom.TournamentLosesInRow = 0;
                WinAction.Invoke();

                var winCount = ApplicationManager.Instance.CurrentRoom.TournamentWinCount();
                FacebookEventSubscribers.LogTournamentWinEvent(ApplicationManager.Instance.CurrentRoom.Number, winCount);
                
                ApplicationManager.Instance.AudioManager.PauseAmbient(AmbientType.TournamentMain);
                ApplicationManager.Instance.AudioManager.PlaySound(SoundType.TournamentWin);
            }
            else
            {
                ApplicationManager.Instance.CurrentRoom.TournamentWinsInRow = 0;
                ApplicationManager.Instance.CurrentRoom.TournamentLosesInRow++;
                LostCount++;
                LoseAction.Invoke();
                
                FacebookEventSubscribers.LogTournamentDefeatEvent(ApplicationManager.Instance.CurrentRoom.Number);
                
                if(ApplicationManager.Instance.CurrentRoom.TournamentLosesInRow == 7)
                    FacebookEventSubscribers.LogTournamentDefeat7Event(ApplicationManager.Instance.CurrentRoom.Number);
                
                ApplicationManager.Instance.AudioManager.PauseAmbient(AmbientType.TournamentMain);
                ApplicationManager.Instance.AudioManager.PlaySound(SoundType.TournamentLost);
            }
        }

        public static void TournamentExit()
        {
            ApplicationManager.Instance.CurrentRoom.TournamentReadyTimer = 0f;
            ApplicationManager.Instance.SaveSystemRef.Save();
            ApplicationManager.Instance.StatesManager.ActiveState.Exit();
            ApplicationManager.Instance.StatesManager.SwitchTo((int) GameStates.GameState);
            
            ApplicationManager.Instance.AudioManager.StopSound();
            ApplicationManager.Instance.AudioManager.UnPauseAmbient(AmbientType.MainTheme);
        }
    }
}