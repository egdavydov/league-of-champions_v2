using Game.Managers;
using Game.States;
using Tournament.Managers;
using UnityEngine;
using Core.StatesMachine.States;

namespace Tournament.States
{
    public sealed class LoseState : State
    {
        private readonly TournamentManager _tournamentManager;
        
        public LoseState(int id, TournamentManager manager) : base(id)
        {
            _tournamentManager = manager;
        }
        
        public override void Enter()
        {
            base.Enter();
            Debug.Log("Tournament Lose enter");
            _tournamentManager.UI.ShowLoseOverlay();
        }

        public override void Update()
        {
        }
    }
}