namespace Tournament.States
{
    public enum TournamentStates
    {
        PrepareState,
        TournamentGameState,
        WinState,
        LoseState
    }
}