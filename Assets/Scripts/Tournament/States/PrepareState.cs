using Core.StatesMachine.States;
using Game.Managers;
using Game.States;
using Tournament.Managers;
using UnityEngine;

namespace Tournament.States
{
    public sealed class PrepareState : State
    {
        private readonly TournamentManager _tournamentManager;
        
        public PrepareState(int id, TournamentManager manager) : base(id)
        {
            _tournamentManager = manager;
        }
        
        public override void Enter()
        {
            base.Enter();
            Debug.Log("Tournament Prepare enter");
            _tournamentManager.UI.ShowPrepareOverlay();
        }
        
        public override void Update()
        {
            
        }

        public void NextState()
        {
            Exit();
            StateMachine.SwitchTo((int) TournamentStates.TournamentGameState);
        }
    }
}