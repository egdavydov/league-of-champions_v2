using System.Collections;
using System.Collections.Generic;
using Core.CoroutineSystem;
using Game.Managers;
using Tournament.Managers;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using Core.StatesMachine.States;
using Services;
using UI.Tournament;

namespace Tournament.States
{
    public sealed class TournamentGameState : State
    {
        private float _gameTimer;
        private float _prepareGameTimer;
        private float _prepareGameTimout;
        private float _enemyTapTimer;
        private Camera _camera;

        public TournamentManager TournamentManager { get; }

        public TournamentGameState(int id, TournamentManager manager) : base(id)
        {
            TournamentManager = manager;
        }

        public override void Enter()
        {
            base.Enter();
            Debug.Log("Tournament Tournament enter");
            _gameTimer = 0f;
            _prepareGameTimout = ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].TournamentCount == 0
                ? float.MaxValue
                : 2f;
            TournamentManager.UI.HideAll();
            TournamentManager.PrepareScore();
            TournamentManager.UI.ShowTopBar();
            TournamentManager.UI.ShowTapToWin();
            
            TournamentManager.UI.clickCritAction += TournamentManager.WinYourMatch;
            _camera = Camera.main;
        }

        public override void Exit()
        {
            TournamentManager.UI.clickCritAction -= TournamentManager.WinYourMatch;
            base.Exit();
        }

        public override void Update()
        {
            if (!TournamentManager.TournamentStarted)
            {
                _prepareGameTimer += Time.deltaTime;

#if UNITY_EDITOR
                var hasTap = Input.GetMouseButtonDown((int) MouseButton.LeftMouse);
#else
                var hasTap = Input.touchCount > 0;
#endif
                // start tournament on tap or after 2 seconds
                if (!hasTap && !(_prepareGameTimer >= _prepareGameTimout)) return;
                
                TournamentManager.UI.HideTapToWin();
                TournamentManager.StartTournament();
                TournamentManager.UI.IncreaseParticlesSpeed();

                return;
            }

            _gameTimer += Time.deltaTime;
            if (_gameTimer >= ApplicationManager.Instance.CollectionManagerRef.Base.TournamentGameTime)
            {
                TournamentManager.StopTournament();
            }

            TournamentManager.UI.SetTimeSlider(_gameTimer);
            
            _enemyTapTimer += Time.deltaTime;
            if (TournamentManager.EnemyTapWaitTime <= _enemyTapTimer)
            {
                TournamentManager.ResultEnemyMatch();
                _enemyTapTimer = 0f;
            }

#if UNITY_EDITOR            
            if (!Input.GetMouseButtonDown((int) MouseButton.LeftMouse)) return;
            
            TournamentManager.UI.IncreaseParticlesSpeed();
            
            var critTap = false;
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hitInfo))
            {
                var crit = hitInfo.transform.GetComponent<Crit>();
                if(crit == null)
                    return;
                
                critTap = !crit.IsDead;
            }

            if (critTap)
                return;
            
            TournamentManager.ResultYourMatch();
#else
            if (Input.touchCount == 0)
                return;
            
            // limit 2 taps
            for (var i = 0; i < Input.touchCount && i < 2; ++i)
            {
                if (Input.GetTouch(i).phase != TouchPhase.Began) continue;

                TournamentManager.UI.IncreaseParticlesSpeed();
                
                var critTap = false;
                var ray = _camera.ScreenPointToRay(Input.GetTouch(i).position);
                if (Physics.Raycast(ray, out var hitInfo))
                {
                    var crit = hitInfo.transform.GetComponent<Crit>();
                    if(crit == null)
                        continue;
                    
                    critTap = !crit.IsDead;
                }

                if (critTap) continue;
                
                if (i > 0)
                {
                    ApplicationManager.Instance.CoroutineSystem.StartCoroutine(this, TapEnumerator());
                }
                else
                {
                    TournamentManager.ResultYourMatch();
                }
            }
#endif
        }

        private IEnumerator TapEnumerator()
        {
            yield return new CoroutineSystemWaitForSeconds(.1f);
            TournamentManager.ResultYourMatch();
        }
    }
}