using Core.StatesMachine.States;
using Game.Managers;
using Game.States;
using Tournament.Managers;
using UnityEngine;

namespace Tournament.States
{
    public sealed class WinState : State
    {
        private readonly TournamentManager _tournamentManager;

        public WinState(int id, TournamentManager manager) : base(id)
        {
            _tournamentManager = manager;
        }

        public override void Enter()
        {
            base.Enter();
            Debug.Log("Tournament Win enter");
            _tournamentManager.UI.ShowWinOverlay();
        }
        
        public override void Update()
        {
        }
    }
}