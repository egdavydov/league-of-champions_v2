﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Managers;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;


public enum SoundType
{
    TournamentLost,
    TournamentWin,
    BoostWinrate,
    BoostEnergetic,
    BoostDonate
}

public enum AmbientType
{
    MainTheme,
    TournamentMain,
}

public sealed class AudioClipExtended
{
    public string Name { get; }
    public AudioSource Source { get; }
    public AudioClip Clip { get; }
    public bool IsAmbient { get; }

    public bool IsPaused => _isPaused;

    private bool _isPaused;
    public AudioClipExtended(string name, AudioSource source, AudioClip clip, bool isAmbient = false)
    {
        Name = name;
        Clip = clip;
        Source = source;
        Source.clip = Clip;
        IsAmbient = isAmbient;
        if (IsAmbient)
        {
            Source.loop = true;
        }
    }

    public void Play()
    {
        _isPaused = false;
        Source.Play();
    }
    
    public void Stop()
    {
        _isPaused = false;
        Source.Stop();
    }
    public void Pause()
    {
        _isPaused = true;
        Source.Pause();
    }
    
    public void UnPause()
    {
        _isPaused = false;
        Source.UnPause();
    }
    
    public void Mute()
    {
        Source.volume = 0f;
    }
    
    public void UnMute()
    {
        Source.volume = 1f;
    }
}

public class AudioManager
{
    private AudioSource _soundSource;
    private Dictionary<string, AudioClipExtended> _clips = new Dictionary<string, AudioClipExtended>();
    
    public bool MusicEnabled { get; set; }
    
    public AudioManager()
    {
        Initialization();
    }

    private void Initialization()
    {
        MusicEnabled = PlayerPrefs.GetInt("Music") == 1;
        
        var go = new GameObject
        {
            name = "AudioManager"
        };
        
        _soundSource = go.AddComponent<AudioSource>();
        _soundSource.loop = true;
        
        Object.DontDestroyOnLoad(go);

       var sounds = Enum.GetNames(typeof(SoundType));
       foreach (var sound in sounds)
       {
           var clip = Resources.Load<AudioClip>($"Audio/{sound}");
           SoundType.TryParse(sound, out SoundType soundEnum);
           _clips.Add(soundEnum.ToString(), new AudioClipExtended(sound, _soundSource, clip));
       }
       
       var ambients = Enum.GetNames(typeof(AmbientType));
       foreach (var ambient in ambients)
       {
           var clip = Resources.Load<AudioClip>($"Audio/{ambient}");
           SoundType.TryParse(ambient, out AmbientType ambientEnum);
           _clips.Add(ambientEnum.ToString(), new AudioClipExtended(ambient,go.AddComponent<AudioSource>(), clip, true));
       }

       PlayAmbient(AmbientType.MainTheme);
       if (!MusicEnabled)
       {
           Mute();
       }
    }

    public void Mute()
    {
        foreach (var audioClipExtended in _clips.Values)
        {
            audioClipExtended.Mute();
        }
    }
    public void UnMute()
    {
        if (!MusicEnabled)
            return;
        
        foreach (var audioClipExtended in _clips.Values)
        {
            audioClipExtended.UnMute();
        }
    }

    public void StopSound()
    {
        _soundSource.Stop();
    }
    
    public void PlaySound(SoundType soundType)
    {
        if (!MusicEnabled)
            return;
        
        _soundSource.loop = false;
        _soundSource.clip = _clips[soundType.ToString()].Clip;
        _soundSource.Play();
    }

    public void PauseAmbient(AmbientType type)
    {
        foreach (var audioClipExtended in _clips.Values)
        {
            if (audioClipExtended.IsAmbient && audioClipExtended.Name == type.ToString())
            {
                audioClipExtended.Pause();
            }
        }
    }

    public void PlayAmbient(AmbientType ambientType, bool fromStart = false)
    {
        foreach (var audioClipExtended in _clips.Values.Where(c=>c.IsAmbient))
        {
            audioClipExtended.Pause();
        }
        var ambient = _clips.Values.FirstOrDefault(c => c.Name == ambientType.ToString());

        if (fromStart)
        {
            ambient.Stop();
        }

        ambient.Play();
    }
    
    public void UnPauseAmbient(AmbientType ambientType)
    {
        foreach (var audioClipExtended in _clips.Values.Where(c=>c.IsAmbient))
        {
            audioClipExtended.Pause();
        }
        var ambient = _clips.Values.FirstOrDefault(c => c.Name == ambientType.ToString());
        ambient.UnPause();
    }
}
