﻿using System;
using Core.CoroutineSystem;
using Game.Managers;
using Services;
using UI;
using UnityEngine;

namespace Game
{
    public class Initialization : MonoBehaviour, ITimeProvider
    {
        [SerializeField] private SaveSystem _saveSystem;
        [SerializeField] private RoomsManager _roomManager;
        [SerializeField] private OfflineEarnings _offlineEarnings;
        [SerializeField] private GameObject[] _servicesPrefabs;
        [SerializeField] private GameObject[] _moveToDontDestroy;
        
        public float deltaTime => Time.deltaTime;
        
        public event Action OnUpdate;
        
        private void Awake()
        {
            if (ApplicationManager.Instance != null && ApplicationManager.Instance.IsInitialized)
            {
                ApplicationManager.Instance.RoomsManagerRef = _roomManager;
                return;
            }

            ApplicationManager.Create(this, _saveSystem, _roomManager, _offlineEarnings);
            DontDestroyOnLoad(this);
            name = "TimeProvider";
            
            foreach (var go in _moveToDontDestroy)
            {
                go.SetActive(true);
                DontDestroyOnLoad(go);
            }

            foreach (var servicesPrefab in _servicesPrefabs)
            {
                var go = Instantiate(servicesPrefab);
                DontDestroyOnLoad(go);
            }

            InitAppsFlyer();
            
            var facebookEventsGo = new GameObject("FaceBookEventsHolder");
            facebookEventsGo.AddComponent<FacebookEventSubscribers>();
            DontDestroyOnLoad(facebookEventsGo);
        }

        private void InitAppsFlyer()
        {
            var wrapper = new AppsFlyerWrapper();
            wrapper.Initialize();
        }
        
        private void Update()
        {
            OnUpdate?.Invoke();
        }
    }
}
