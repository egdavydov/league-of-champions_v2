using Core.StatesMachine;
using Core.StatesMachine.States;
using Game.Managers;
using Services;
using Tournament.Managers;
using Tournament.States;
using Tutorial.Steps;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.States
{
    public sealed class TournamentState : State
    {
        public static string SceneName { get; } = "Tournament";
        public StatesMachine StatesMachine { get; private set; }
        public TournamentManager TournamentManager { get; set; }

        public TournamentState(int id) : base(id)
        {
        }
        
        public override void Enter()
        {
            Debug.Log("TournamentState entered");
            base.Enter();
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
            StatesMachine = new StatesMachine(ApplicationManager.Instance.CoroutineSystem);
            TournamentManager = TournamentManager.Instance;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.Log("OnSceneLoaded: " + scene.name);
            SceneManager.sceneLoaded -= OnSceneLoaded;
            TournamentManager.Init();
            StatesMachine.AddState(TournamentStates.PrepareState, TournamentManager);
            StatesMachine.AddState(TournamentStates.TournamentGameState, TournamentManager);
            StatesMachine.AddState(TournamentStates.WinState, TournamentManager);
            StatesMachine.AddState(TournamentStates.LoseState, TournamentManager);
            StatesMachine.SwitchTo((int) TournamentStates.PrepareState);

            TournamentManager.WinAction += OnWinHandler;
            TournamentManager.LoseAction += OnLoseHandler;
            TournamentManager.TournamentAgainAction += OnGameAgainHandler;
            
            if (ApplicationManager.Instance.TutorialSystem != null && ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is TutorialStep)
                ApplicationManager.Instance.TutorialSystem.TutorialWindowController.SetCamera();
        }

        public override void Update()
        {
        }

        public override void Exit()
        {
            ApplicationManager.Instance.SaveSystemRef.Save();
            TournamentManager.WinAction -= OnWinHandler;
            TournamentManager.LoseAction -= OnLoseHandler;
            TournamentManager.TournamentAgainAction -= OnGameAgainHandler;
            
            if (ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is TournamentStep)
                ApplicationManager.Instance.TutorialSystem.CompleteStep();
            
            base.Exit();
        }

        private void OnWinHandler()
        {
            StatesMachine.SwitchTo((int) TournamentStates.WinState);
        }
        
        private void OnLoseHandler()
        {
            StatesMachine.SwitchTo((int) TournamentStates.LoseState);
        }
        private void OnGameAgainHandler()
        {
            StatesMachine.SwitchTo((int) TournamentStates.TournamentGameState);
            FacebookEventSubscribers.LogTournamentTryAgainEvent(ApplicationManager.Instance.CurrentRoom.Number);
        }
    }
}