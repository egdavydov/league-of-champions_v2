using Core.StatesMachine.States;
using Game.Managers;
using Tutorial.Steps;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.States
{
    public class GameState : State
    {
        public static string SceneName { get; } = "GameScene";

        public GameState(int id) : base(id)
        {
        }

        public override void Update()
        {
            
        }

        public override void Enter()
        {
            base.Enter();
            Debug.Log("GameState entered");
            
            // load application with main scene
            var scene = SceneManager.GetActiveScene();
            if (scene.name == SceneName)
            {
                ApplicationManager.Instance.RoomsManagerRef.Generate();
                return;
            }

            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.Log("OnSceneLoaded: " + scene.name);
            SceneManager.sceneLoaded -= OnSceneLoaded;
            ApplicationManager.Instance.RoomsManagerRef.Generate();

            if (ApplicationManager.Instance.TutorialSystem == null) return;
            
            if (ApplicationManager.Instance.TutorialSystem.TutorialWindowController != null)
                ApplicationManager.Instance.TutorialSystem.TutorialWindowController.SetCamera();
        }
    }
}