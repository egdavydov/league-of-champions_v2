using System;
using System.Collections;
using Core.Common.Managers;
using DB;
using Rooms;
using UnityEngine;

namespace Game.Managers
{
    public class CollectionManager : Manager
    {
        public BaseDB Base { get; private set; }
        public PerksDB Perks { get; private set; }
        
        private RoomsDB[] _roomsDB;
        public RoomsDB[] GetRoomsDB => _roomsDB;
        
        public UltraBonusDB UltraBonus { get; private set; }

        public static Action DatabaseLoaded;

        public override void Init()
        {
            
        }

        public override IEnumerator InitAsync()
        {
            yield return null;
        }

        public override void Deinit()
        {
        }

        public void SetDatabases(BaseDB baseDb, PerksDB perksDb, RoomsDB[] roomsDbs, UltraBonusDB ultraBonusDb)
        {
            Base = baseDb;
            Perks = perksDb;
            _roomsDB = roomsDbs;
            UltraBonus = ultraBonusDb;

            for (var roomNumber = 0; roomNumber < _roomsDB.Length; roomNumber++)
            {
                _roomsDB[roomNumber].Number = roomNumber;
                var roomDb = _roomsDB[roomNumber];
                foreach (var gamer in roomDb.GetGamers)
                {
                    gamer.skills[gamer.skills.Length - 1].BySkillPoints = true;
                }
            }

            DatabaseLoaded.Invoke();
        }
        
        public RoomsDB GetRoomDB(int number)
        {
            return _roomsDB[number];
        }
    }
}