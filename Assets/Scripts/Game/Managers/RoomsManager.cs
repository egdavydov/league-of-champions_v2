using System;
using Core.Common.Managers;
using Game.States;
using Rooms;
using UnityEngine;

namespace Game.Managers
{
    public class RoomsManager : MonoBehaviour
    {
        [SerializeField] private ActivateRoom[] _rooms;
        private GamersPlacer[] _gamersPlacer;

        public ActivateRoom[] Rooms => _rooms;

        public GamerActivate GetGamer(int numberRoom, int numberGamer)
        {
            return _gamersPlacer[numberRoom].GetGamer(numberGamer);
        }

        public GamerActivate[] GetGamers(int numberRoom)
        {
            return _gamersPlacer[numberRoom].GetGamersActivate;
        }

        public void Generate()
        {
            _gamersPlacer = new GamersPlacer[_rooms.Length];

            for (var i = 0; i < _rooms.Length; i++)
            {
                _gamersPlacer[i] = _rooms[i].GetGamersPlacer;
            }

            foreach (var roomDB in ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB)
            {
                if (!roomDB.Activate) continue;
                
                foreach (var gamer in roomDB.GetGamers)
                {
                    if (!gamer.activate) continue;
                    
                    gamer.SetCoinsVictory(roomDB);
                    gamer.SetWinRate();
                    gamer.SetCoinsInSecond(roomDB);
                }
            }
        }
    }
}