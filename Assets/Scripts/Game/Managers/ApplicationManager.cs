using System;
using Core.CoroutineSystem;
using Core.StatesMachine;
using Core.UI;
using Core.UI.Interfaces;
using Game.States;
using Services;
using Tournament.Managers;
using Tutorial;
using UI;
using UnityEngine;

namespace Game.Managers
{
    public enum ViewType
    {
        BoostInfoView,
        DailyBonusView,
        RoomCustomizationView,
        TutorialView,
        UltraBonusView,
        UltraBonusRewardView,
        SpAdvRewardView,
        TournamentFillView
    }

    public class ApplicationManager
    {
        public static ApplicationManager Instance { get; private set; }
        public CoroutineSystem CoroutineSystem { get; }
        public StatesMachine StatesManager { get; }
        public AudioManager AudioManager { get; }
        public RoomsDB CurrentRoom { get; private set; }
        public SaveSystem SaveSystemRef { get; }
        public TournamentManager TournamentManagerRef { get; set; } 
        public CollectionManager CollectionManagerRef { get; } 
        public RoomsManager RoomsManagerRef { get; set; }
        public OfflineEarnings OfflineEarningsWindow { get; }
        public TutorialSystem TutorialSystem { get; private set; }
        public Tutorial.Tutorial TutorialRef { get; set; }

        public IViewSystem<ViewType> UISystem;
        public bool IsInitialized { get; }
        
        private ApplicationManager(ITimeProvider timeProvider, SaveSystem saveSystem, RoomsManager roomsManager, OfflineEarnings offlineEarnings)
        {
            CoroutineSystem = new CoroutineSystem { TimeProvider = timeProvider };
            SaveSystemRef = saveSystem;
            RoomsManagerRef = roomsManager;
            OfflineEarningsWindow = offlineEarnings;
            CollectionManagerRef = new CollectionManager();
            UISystem = new UISystem<ViewType>();
            TutorialSystem = new TutorialSystem(CoroutineSystem);
            AudioManager = new AudioManager();
            
            StatesManager = new StatesMachine(CoroutineSystem);
            StatesManager.AddState(GameStates.GameState);
            StatesManager.AddState(GameStates.TournamentState);
            
            EventManager.ChangeRoom.AddListener(RoomChanged);
            CollectionManager.DatabaseLoaded += DatabaseLoaded;
            IsInitialized = true;
        }

        private void RoomChanged(int number)
        {
            CurrentRoom = CollectionManagerRef.GetRoomDB(number);
        }
        
        private void DatabaseLoaded()
        {
            Debug.Log("Database Loaded");
            CollectionManager.DatabaseLoaded -= DatabaseLoaded;
            SaveSystemRef.SavedDataLoaded += OnSavedDataLoaded;
            SaveSystemRef.LoadData();
        }

        private void OnSavedDataLoaded()
        {
            Debug.Log("SavedData Loaded");
            SaveSystemRef.SavedDataLoaded -= OnSavedDataLoaded;
            StatesManager.SwitchTo((int) GameStates.GameState);
            OfflineEarningsWindow.OpenWindow();
        }
        
        public static void Create(ITimeProvider timeProvider, SaveSystem saveSystem, RoomsManager roomsManager, OfflineEarnings offlineEarnings)
        {
            Instance = new ApplicationManager(timeProvider, saveSystem, roomsManager, offlineEarnings);
        }
    }
}