﻿using System.Collections;
using Services;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Creation : MonoBehaviour
{
   private const string ResetedKey = "FirstResetKey";
   private void Start()
   {
      var isReseted = PlayerPrefs.GetInt(ResetedKey, 0) == 1;
      
      if(!isReseted)
      {
         var saveSystem = new SaveSystem();
         saveSystem.ResetDefault();
         PlayerPrefs.SetInt(ResetedKey,1);
         PlayerPrefs.SetInt("Music",1);
      }
      
      StartCoroutine(LoadScene());

#if UNITY_IOS
      UnityEngine.iOS.NotificationServices.RegisterForNotifications(UnityEngine.iOS.NotificationType.Alert| UnityEngine.iOS.NotificationType.Badge |  UnityEngine.iOS.NotificationType.Sound);
#endif
   }

   private IEnumerator LoadScene()
   {
      yield return new WaitForSeconds(2f);
      SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
   }
}
