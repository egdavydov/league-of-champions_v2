﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class Raycaster : MonoBehaviour
{
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    private void Start()
    {
        m_Raycaster = GetComponent<GraphicRaycaster>();
        m_EventSystem = GetComponent<EventSystem>();
    }

    public bool IsRaycast(Vector3 position, string name)
    {
        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = position;

        List<RaycastResult> results = new List<RaycastResult>();

        EventSystem.current.RaycastAll(m_PointerEventData, results);

        foreach (RaycastResult result in results)
        {
                
            if (name == result.gameObject.name)
            {
                return true;
            }
        }

        return false;
    }
}