﻿using System;
using Game.Managers;
using Rooms;
using UnityEngine;

public class ScrollCamera : MonoBehaviour
{
    [SerializeField] private Raycaster m_raycaster;
    [SerializeField] private float m_timeDown = 0.2f;
    [SerializeField] private RoomsManager _roomsManager;
    
    private float _curTimeDown = 0f;
    private Vector2 _curDownPoint;
    private Vector2 _firstDownPoint;
    private Vector2 _newCameraPosition;

    private Vector2 _firstTapPosition;
    private Vector2 _moveTapPosition;
    private Vector2 _targetVelocity;

    private float _deltaY;
    private float? _lerpT = null;
    private bool _isOneDown = true;

    private BoxCollider2D _minCollider2D;
    private BoxCollider2D _maxCollider2D;
    private Camera _camera;

    public float PositionMin { get; private set; }
    public float PositionMax { get; private set; }
    public bool IsSwipe { private get; set; }

    private void Awake()
    {
        _curTimeDown = 0f;
        _camera = Camera.main;
        IsSwipe = true;
        _minCollider2D = _roomsManager.Rooms[0].GetComponent<BoxCollider2D>();
        _maxCollider2D = _roomsManager.Rooms[_roomsManager.Rooms.Length - 1].GetComponent<BoxCollider2D>();

        ScaleCamera();
        BorderCamera();

        var userCameraY = PlayerPrefs.GetFloat("Camera");
        SetCameraPosition(userCameraY);
    }

    private void ScaleCamera()
    {
        var width = _minCollider2D.size.x;
        _camera.orthographicSize = width / (_camera.aspect * 2);
    }

    private void BorderCamera()
    {
        var minBound = _minCollider2D.bounds.min.y;
        var maxBound = _maxCollider2D.bounds.max.y;

        var orthographicSize = _camera.orthographicSize;
        PositionMin = minBound + orthographicSize;
        PositionMax = maxBound - orthographicSize;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var activateRoom = other.GetComponent<ActivateRoom>();
        if (activateRoom != null)
        {
            EventManager.ChangeRoom.Invoke(activateRoom.GetRoomDB.Number);
        }
    }

    private void Update()
    {
        if (!IsSwipe)
            return;
        
        if (Input.touchCount > 1)
        {
            _isOneDown = false;
            return;
        }

        if (Input.touchCount == 1 && !_isOneDown)
        {
            _isOneDown = true;
            _firstTapPosition = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
        }

        if (!_isOneDown)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            if (m_raycaster.IsRaycast(Input.mousePosition, "Panel"))
                return;

            _curTimeDown = 0f;
            _deltaY = 0f;
            _firstTapPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            return;
        }

        if (Input.GetMouseButton(0))
        {
            if (m_raycaster.IsRaycast(Input.mousePosition, "Panel"))
                return;

            _curTimeDown += Time.deltaTime;
            if (_curTimeDown <= m_timeDown)
            {
                _lerpT = null;
                return;
            }

            _lerpT = 0f;
            _moveTapPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            _deltaY = _firstTapPosition.y - _moveTapPosition.y;
            ShiftCamera(_deltaY);
            return;
        }

        if (_lerpT != null && _lerpT < 1f)
        {
            _lerpT += Time.deltaTime;
            _deltaY = Mathf.Lerp(_deltaY, 0, (float) _lerpT);
            ShiftCamera(_deltaY);
        }

        if (_lerpT >= 1f)
        {
            _lerpT = null;
        }
    }

    private void ShiftCamera(float delta)
    {
        SetCameraPosition(transform.position.y + delta);
    }

    public void SetCameraPosition(float positionY)
    {
        var position = transform.position;
        position.y = Mathf.Clamp(positionY, PositionMin, PositionMax);
        transform.position = position;
    }

    public void SetCameraObject(Vector3 targetPosition)
    {
        var newY = Mathf.Clamp(targetPosition.y, PositionMin, PositionMax);
        if (Math.Abs(transform.position.y - newY) > .5f)
            SetCameraPosition(newY);
    }
    
}