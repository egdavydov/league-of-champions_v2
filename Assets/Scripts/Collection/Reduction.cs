﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class Reduction : MonoBehaviour
{
    public static Reduction instance = null;

    private static string[] _letters = new string[]
        {"", "K", "M", "B", "T", "q", "Q", "s", "S", "O", "N", "d", "U", "D"};

    public static string ReductionNumber(double value, int thousandsCount, bool fraction)
    {
        string strValue;
        
        if (value < 1000)
        {
            value = (float) Math.Truncate(value * 100) / 100;
            strValue = value.ToString("#,##0");
            return strValue;
        }
        
        int multiple = (int) Math.Pow(1000, thousandsCount + 1);

        for (int i = 0; i < _letters.Length; i++)
        {
            if (value < multiple)
            {
                value = (float) Math.Truncate(value * 100) / 100;

                if (fraction)
                {
                    strValue = string.Format("{0:0.00}" + _letters[i], value);
                    strValue = strValue.Replace(",", ".");
                }
                else
                {
                    strValue = value.ToString("#,##0" + _letters[i]);
                    strValue = strValue.Replace(" ", ",");
                }

                return strValue;
            }

            value = value / 1000;
        }

        return null;
    }

    private void Awake()
    {
        //////////////////////// Singleton release //////////////////////////////

        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }
    }
}