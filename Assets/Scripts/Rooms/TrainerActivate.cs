using System;
using System.Collections;
using Game.Managers;
using TMPro;
using Tutorial.Steps;
using UI.Trainer;
using UnityEngine;
using UnityEngine.Rooms.Trainer;
using UnityEngine.UI;

namespace UnityEngine.Rooms
{
    public class TrainerActivate : MonoBehaviour
    {
        [SerializeField] private GameObject _lock;
        [SerializeField] private GameObject _activate;
        [SerializeField] private CountUpgradesWidget _countUpgradesWidget;
        [SerializeField] private string[] _comments;
        [SerializeField] private Animator _commentAnimator;
        [SerializeField] private TextMeshProUGUI _commentText;
        [SerializeField] private Button _trainerBtn;
        [SerializeField] private TrainerWindow _trainerWindow;

        private RoomsDB _roomDB;

        private void Start()
        {
            EventManager.TutorialStepBegin.AddListener(CheckTutorial);
        }

        public void Place(RoomsDB roomDB)
        {
            _roomDB = roomDB;
            _countUpgradesWidget.Trainer = _roomDB.GetTrainer;
            
            if (!_roomDB.Activate)
            {
                IsEnable(false);
                _countUpgradesWidget.gameObject.SetActive(false);
                return;
            }

            IsEnable(_roomDB.GetTrainer.Activate);
            _countUpgradesWidget.gameObject.SetActive(_roomDB.GetTrainer.Activate);
            _countUpgradesWidget.Refresh();
            _trainerBtn.onClick.AddListener(TrainerClick);
         
            if ( ApplicationManager.Instance.TutorialSystem?.GetActiveTutorialStep() is TrainerStep )
                CheckTutorial(ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep());
        }

        private void CheckTutorial(TutorialStep step)
        {
            if (_roomDB.Number > 0 || _roomDB.GetTrainer.Activate || !(step is TrainerStep)) return;
            EventManager.TutorialStepBegin.RemoveListener(CheckTutorial);
            ApplicationManager.Instance.TutorialSystem.TutorialWindowController.NextTutorialTarget();
        }
        
        private void IsEnable(bool enable)
        {
            _lock.SetActive(!enable);
            _activate.SetActive(enable);
        }

        public void IsHide(bool hide)
        {
            gameObject.SetActive(!hide);
        }

        public void Click()
        {
            EventManager.VibrateLight.Invoke();
            _trainerWindow.OpenWindow(_roomDB);
        }

        private void TrainerClick()
        {
            EventManager.VibrateLight.Invoke();
            
            if(!IsCommentAnimationStop())
                return;

            StartCoroutine(Comment());
        }

        private IEnumerator Comment()
        {
            _commentAnimator.SetBool("Comment", true);
            var commentId = Random.Range(0, _comments.Length - 1);
            _commentText.text = _comments[commentId];
            
            yield return new WaitForSeconds(5f);
            _commentAnimator.SetBool("Comment", false);
        }
        
        private bool IsCommentAnimationStop()
        {
            var animatorStateInfo = _commentAnimator.GetCurrentAnimatorStateInfo(0);
            return animatorStateInfo.IsName("Close") || animatorStateInfo.IsName("Idle");
        }

        private void OnDestroy()
        {
            _trainerBtn.onClick.RemoveListener(TrainerClick);
            EventManager.TutorialStepBegin.RemoveListener(CheckTutorial);
        }
    }
}