using Game.Managers;
using TMPro;
using UnityEngine;

namespace UnityEngine.Rooms.Trainer
{
    public class CountUpgradesWidget : MonoBehaviour
    {
        [SerializeField] private GameObject _activeWidget;
        [SerializeField] private GameObject _noActiveWidget;
        [SerializeField] private TextMeshProUGUI _countTxt;
        
        private bool _active;
        private RoomsDB.Trainer _trainer;

        public RoomsDB.Trainer Trainer
        {
            set => _trainer = value;
        }
        
        private void Start()
        {
            EventManager.ChangeSkillPoints.AddListener(Refresh);
        }
        
        public void Refresh()
        {
            if(!_trainer.Activate)
                return;
            
            var countImprovement = 0;
            var baseDb = ApplicationManager.Instance.CollectionManagerRef.Base;
            var curSkillPoints = baseDb.curSkillPoints;

            if (_trainer.GetTimeLimitLevel(baseDb) > 0 && _trainer.GetUpgradeTimeLimitPrice() <= curSkillPoints)
            {
                countImprovement++;
            }
            if (_trainer.GetRevenueLevel(baseDb) > 0 && _trainer.GetUpgradeRevenuePrice() <= curSkillPoints)
            {
                countImprovement++;
            }
            
            _active = countImprovement > 0;
            _activeWidget.SetActive(_active);
            _noActiveWidget.SetActive(!_active);

            if (_active)
            {
                _countTxt.text = countImprovement.ToString();
            }
        }
        
        private void OnDestroy()
        {
            EventManager.ChangeSkillPoints.RemoveListener(Refresh);
        }
    }
}