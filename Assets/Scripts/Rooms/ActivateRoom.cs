﻿using System;
using DG.Tweening;
using Fans;
using Game.Managers;
using TMPro;
using UI.Tournament;
using UnityEngine;
using UnityEngine.Rooms;
using UnityEngine.UI;

namespace Rooms
{
    public class ActivateRoom : MonoBehaviour
    {
        private bool _btnPressed;

        [SerializeField] private RoomsDB _roomDB;
        [SerializeField] private GameObject _activateRoom;
        [SerializeField] private GameObject _lockRoom;
        [SerializeField] private Image _iconRoom;
        [SerializeField] private TournamentWidget _tournamentWidget;
        [SerializeField] private TrainerActivate _trainerActivate;
        [SerializeField] private GamersPlacer _gamersPlacer;
        [SerializeField] private CustomLogo _customLogo;

        [Header("LockLevel")]
        [SerializeField] private GameObject _lockLevelAccess;
        [SerializeField] private GameObject _lockPriceAccess;

        [Header("Price")]
        [SerializeField] private TextMeshProUGUI _accessLevelTxt;
        [SerializeField] private TextMeshProUGUI _accessPriceTxt;
    
        [Header("Buy Btns")]
        [SerializeField] private GameObject _activateBuyBtn;
        [SerializeField] private GameObject _unactivateBuyBtn;
    
        private FansManager _fans;
        private Coroutine _refresh;

        public FansManager GetFans => _fans;
        public RoomsDB GetRoomDB => _roomDB;
        public GamersPlacer GetGamersPlacer => _gamersPlacer;
        public TrainerActivate GetTrainerActivate => _trainerActivate;
        public TournamentWidget GetTournamentWidget => _tournamentWidget;

        private void Awake()
        {
            _tournamentWidget.RoomsDb = _roomDB;
            EventManager.ChangeRoomCustomization.AddListener(UpdateCustomLogo);
        }

        private void Start()
        {
            _gamersPlacer.Place(_roomDB);
            _trainerActivate.Place(_roomDB);
            if (_roomDB.Number == 0)
            {
                _trainerActivate.IsHide(_roomDB.TournamentCount == 0);
            }

            Activate(_roomDB.Activate);
            UpdateCustomLogo();
        }

        private void Activate(bool activate)
        {
            _activateRoom.SetActive(activate);
            _lockRoom.SetActive(!activate);

            if (activate)
            {
                _fans = GetComponentInChildren<FansManager>();
                _fans.Enable(_roomDB);
                return;
            }
        
            _iconRoom.sprite = _roomDB.LogoRoom;

            var accessDenied = _roomDB.LevelAccess > ApplicationManager.Instance.CollectionManagerRef.Base.curLevel;
        
            _lockLevelAccess.SetActive(accessDenied);
            _lockPriceAccess.SetActive(!accessDenied);

            if (accessDenied)
            {
                _accessLevelTxt.text = "LEVEL " + _roomDB.LevelAccess;
                RefreshByLevel();
                EventManager.ChangeLevel.AddListener(RefreshByLevel);
                return;
            }

            _accessPriceTxt.text = Reduction.ReductionNumber(_roomDB.PriceAccess, 0, true);
            RefreshByPrice();
            EventManager.ChangeCoins.AddListener(RefreshByPrice);
        }

        private void RefreshByLevel()
        {
            if (!(_roomDB.LevelAccess > ApplicationManager.Instance.CollectionManagerRef.Base.curLevel))
            {
                _lockLevelAccess.SetActive(false);
                _lockPriceAccess.SetActive(true);
                _accessPriceTxt.text = Reduction.ReductionNumber(_roomDB.PriceAccess, 0, true);

                RefreshByPrice();
                EventManager.ChangeCoins.AddListener(RefreshByPrice);
                EventManager.ChangeLevel.RemoveListener(RefreshByLevel);
            }
        }

        private void RefreshByPrice()
        {
            var notEnough = _roomDB.PriceAccess > ApplicationManager.Instance.CollectionManagerRef.Base.curCoins;
            _unactivateBuyBtn.SetActive(notEnough);
            _activateBuyBtn.SetActive(!notEnough);
        }

        public void OnBuyRoomBtn(bool isChangeCoins = true)
        {
            EventManager.ChangeCoins.RemoveListener(RefreshByPrice);
        
            _roomDB.Activate = true;
            if (isChangeCoins)
            {
                ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(_roomDB.PriceAccess * -1);
            }

            _roomDB.GetGamerByNumber(0).activate = true;

            for (var i = 0; i < _gamersPlacer.GetGamersActivate.Length; i++)
            {
                var gamer = _gamersPlacer.GetGamer(i);
                gamer.GamerPlace(_roomDB, _roomDB.GetGamerByNumber(i));
            }

            Activate(true);
            
            var camera = Camera.main;
            var position = camera.transform.position;
            var roomCollider = ApplicationManager.Instance.RoomsManagerRef.Rooms[_roomDB.Number].GetComponent<BoxCollider2D>();
            var roomPositionY = roomCollider.transform.position.y - roomCollider.offset.y * .5f;

            var cameraY = position.y;
            var tweener = DOTween.To(() => cameraY, x => cameraY = x, roomPositionY, 1).OnUpdate(() =>
            {
                position.y = cameraY;
                camera.transform.position = position;
            });
            tweener.SetAutoKill(true);
            tweener.SetEase(Ease.OutExpo);
            tweener.Play();
            
            EventManager.ChangeRoom.Invoke(_roomDB.Number);
            EventManager.BuyRoom.Invoke(_roomDB.Number);
        }

        private void UpdateCustomLogo()
        {
            if (_roomDB.CustomLogo != null)
            {
                var customLogo = ApplicationManager.Instance.CollectionManagerRef.Base.GetCustomLogo((int) _roomDB.CustomLogo);
                _customLogo.SetLogoSprite(customLogo.Image);
                _customLogo.gameObject.SetActive(true);
            }
            else
            {
                _customLogo.gameObject.SetActive(false);   
            }
        }

        private void OnDestroy()
        {
            EventManager.ChangeRoomCustomization.RemoveListener(UpdateCustomLogo);
        }
    }
}