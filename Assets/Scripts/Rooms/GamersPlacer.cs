﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamersPlacer : MonoBehaviour
{
    [SerializeField] private GamerActivate[] _gamersActivate;
    private RoomsDB _roomDB;

    public GamerActivate[] GetGamersActivate  => _gamersActivate;
 
    public GamerActivate GetGamer(int numberGamer)
    {
        return _gamersActivate[numberGamer];
    }

    public void Place(RoomsDB roomDB)
    {
        _roomDB = roomDB;
        
        //EventManager.BuyGamer.AddListener(GetGamer);

        for (int i = 0; i < _gamersActivate.Length; i++)
        {
            RoomsDB.Gamer gamer = _roomDB.GetGamerByNumber(i);
            gamer.gamerNumber = i;
            gamer.roomNumber = _roomDB.Number;
            _gamersActivate[i].GamerPlace(_roomDB, gamer);
        }
    }

    private void OnDestroy()
    {
        //EventManager.BuyGamer.RemoveListener(GetGamer);
    }
}
