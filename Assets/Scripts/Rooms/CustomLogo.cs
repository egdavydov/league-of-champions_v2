using UnityEngine;

namespace UnityEngine.Rooms
{
    public class CustomLogo : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer[] _logoSprites;

        public void SetLogoSprite(Sprite sprite)
        {
            foreach (var logo in _logoSprites)
            {
                logo.sprite = sprite;
            }
        }
    }
}