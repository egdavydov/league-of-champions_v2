using UnityEngine;

public class SetCanvasBounds : MonoBehaviour
{
	private RectTransform _rectTransform;
	private float _initialHeight;
	
	private void ApplySafeArea()
	{
#if DEVELOPMENT
		MyDebug.Log($"Screen height {Screen.height} width {Screen.width}");
		MyDebug.Log($"SafeArea height {Screen.safeArea.height} width {Screen.safeArea.width}");
		MyDebug.Log($"SafeArea Y {Screen.safeArea.yMin} X {Screen.safeArea.xMin}");
#endif
		
		var deltaVector = new Vector2(0, _initialHeight + Screen.safeArea.yMin * 0.6f);
		_rectTransform.sizeDelta = deltaVector;
	}

	private void Start ()
	{
		_rectTransform = GetComponent<RectTransform>();
		_initialHeight = _rectTransform.rect.height;
		ApplySafeArea();
	}
}