﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using Game.Managers;
using Gamers.Widgets;
using Services;
using UnityEngine;
using TMPro;
using UnityEngine.Serialization;
using Random = System.Random;

public class Match : MonoBehaviour
{
    private UIOverride m_ui;
   
    private WaitForSeconds m_delay;
    private Animator m_gamerAnimator;
    private MatchLabelsWidget m_matchLabelsWidget;

    private GamerActivate m_gamerActivate;

    private bool m_boostEnergetic = false;
    private bool m_boostConcentration = false;

    private Coroutine _run;

    private float _matchCount; 
 
    private void Start()
    {
        var uiManager = GetComponent<UIManager>();
        uiManager.SetMatch = this;
        
        EventManager.BoostEnergetic.AddListener(BoostEnergetic);
        EventManager.BoostConcentration.AddListener(BoostConcentration);

        m_ui = GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIOverride>();

        m_gamerActivate = GetComponent<GamerActivate>();
        m_gamerAnimator = GetComponent<Animator>();

        m_matchLabelsWidget = m_gamerActivate.GetMatchLabels;

        GenerateDelayMatch();

        _run = StartCoroutine(RunMatch());
    }

    private void BoostEnergetic()
    {
        m_boostEnergetic = !m_boostEnergetic;
        if (m_boostEnergetic)
        {
            m_delay = new WaitForSeconds(0.2f);
            m_gamerAnimator.SetBool("BoostEnergetic", true);
            StopCoroutine(_run);
            _run = StartCoroutine(RunMatch());
        }
        else
        {
            GenerateDelayMatch();
            m_gamerAnimator.SetBool("BoostEnergetic", false);
        }
    }

    private void BoostConcentration()
    {
        m_boostConcentration = !m_boostConcentration;
    }

    public void GenerateDelayMatch()
    {
        m_delay = new WaitForSeconds(ApplicationManager.Instance.CollectionManagerRef.Base.baseDelayBetweenMatches / m_gamerActivate.GetRoomDB.curLevelSpeed);
        
    }

    //////////////////////// automatic match //////////////////////////////

    private IEnumerator RunMatch()
    {
        yield return m_delay;
        ResultMatch(false);
    }
    
    //////////////////////// manual match //////////////////////////////

    public void Click()
    {
        EventManager.VibrateLight.Invoke();
        m_gamerAnimator.SetTrigger("ClickMatch");
        ResultMatch(true);
    }
    
    private void ResultMatch(bool byClick)
    {
        var chance = m_boostConcentration ? 0 : UnityEngine.Random.Range(0, 101);

        if (chance <= m_gamerActivate.GetGamerDB.curWinRate)
        {
            var perkMultiplier = 1f;
            if (m_gamerActivate.GetGamerDB.Perks > 0)
            {
                var perkEveryWinBonus = ApplicationManager.Instance.CollectionManagerRef.Perks.perks.First(perk => perk.bonusEveryVictory > 0);
                if (perkEveryWinBonus != null)
                {
                    var gamerHasPerk = m_gamerActivate.GetGamerDB.Perks >= Array.IndexOf(ApplicationManager.Instance.CollectionManagerRef.Perks.perks, perkEveryWinBonus) + 1;
                    var isEveryMatch = Math.Abs(_matchCount % perkEveryWinBonus.bonusEveryVictory) <= 0;
                    
                    perkMultiplier = gamerHasPerk && isEveryMatch 
                                        ? perkEveryWinBonus.bonusEveryVictoryMultiplier 
                                        : perkMultiplier;
                }
            }

            var coins = m_gamerActivate.GetGamerDB.coinsVictory * perkMultiplier;
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(coins);
            ApplicationManager.Instance.CollectionManagerRef.Base.AddExperience();
            _matchCount++;
            m_matchLabelsWidget.PlayAnim(coins);
            if(byClick && ApplicationManager.Instance.CollectionManagerRef.Base.curLevel >= 7)
                m_gamerActivate.GetRoomDB.TournamentReadyTimer += m_gamerActivate.GetRoomDB.tournamentGamerClickTime;
        }
        else
        {
            m_matchLabelsWidget.PlayAnim(0f);
        }
        
        StopCoroutine(_run);
        _run = StartCoroutine(RunMatch());
    }

    private void OnDestroy()
    {
        EventManager.BoostEnergetic.RemoveListener(BoostEnergetic);
        EventManager.BoostConcentration.RemoveListener(BoostConcentration);
    }
}
