﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Gamers.Widgets;

public class GamerActivate : MonoBehaviour
{
    private Animator m_anim;

    [SerializeField] private GameObject m_lock;
    [SerializeField] private GameObject m_activate;
    [SerializeField] private AnimationParticles m_upgradeSpeedParticle;
    [SerializeField] private CountBuySkillsWidget m_countBuySkillsWidget;
    [SerializeField] private CheckBuyGamerWidget m_checkBuyGamerWidget;
    [SerializeField] private MatchLabelsWidget m_matchLabelsWidget;
    public MatchLabelsWidget GetMatchLabels => m_matchLabelsWidget;
    public AnimationParticles GetParticles => m_upgradeSpeedParticle;

    private RoomsDB.Gamer _gamerDB;
    public RoomsDB.Gamer GetGamerDB => _gamerDB;

    private RoomsDB _roomDB;
    public RoomsDB GetRoomDB => _roomDB;

    private bool _isActive;
    public bool IsActive => _isActive;

    private void Awake()
    {
        m_countBuySkillsWidget.enabled = false;
        m_checkBuyGamerWidget.enabled = false;
    }

    public void GamerPlace(RoomsDB roomDB, RoomsDB.Gamer gamerDB)
    {
        ////generate start specifications////
        _roomDB = roomDB;
        _gamerDB = gamerDB;

        if (!_roomDB.Activate)
        {
            IsEnable(false);
            m_checkBuyGamerWidget.gameObject.SetActive(false);
            return;
        }

        m_checkBuyGamerWidget.gameObject.SetActive(true);
        _gamerDB.SetWinRate();

        foreach (var skill in _gamerDB.skills)
        {
            skill.curPrice = skill.basePrice;
        }

        ////enable or disable gamer////

        var activate = _gamerDB.activate;
        IsEnable(activate);

        if (activate)
        {
            _isActive = true;
            m_anim = GetComponent<Animator>();
            m_anim.enabled = true;

            EventManager.ChangeMultiplier.AddListener(GenerateCoinsVictory);
            EventManager.ChangePerks.AddListener(GenerateParameters);

            GenerateParameters();

            gameObject.AddComponent<GamerSkills>();
            gameObject.AddComponent<Match>();

            m_countBuySkillsWidget.enabled = true;
            m_countBuySkillsWidget.SetGamer = _gamerDB;
        }
        else
        {
            m_checkBuyGamerWidget.enabled = true;
            m_checkBuyGamerWidget.SetGamer = _gamerDB;
        }
    }

    private void IsEnable(bool enable)
    {
        m_lock.SetActive(!enable);
        m_activate.SetActive(enable);
    }

    public void GenerateParameters()
    {
        _gamerDB.SetCoinsVictory(_roomDB);
        _gamerDB.SetWinRate();
        _gamerDB.SetCoinsInSecond(_roomDB);
        //m_matchLabelsWidget.SetCoins(_gamerDB.coinsVictory);
    }

    private void GenerateCoinsVictory()
    {
        _gamerDB.SetCoinsVictory(_roomDB);
        //m_matchLabelsWidget.SetCoins(_gamerDB.coinsVictory);
    }

    private void OnDestroy()
    {
        if (!_gamerDB.activate) return;
        EventManager.ChangeMultiplier.RemoveListener(GenerateCoinsVictory);
        EventManager.ChangePerks.RemoveListener(GenerateParameters);
    }
}