﻿using System.Collections;
using System.Collections.Generic;
using Rooms;
using UnityEngine;
using UnityEngine.Events;


public class UIManager : MonoBehaviour
{
    private Match _match;
    private RoomsDB _roomDB;

    public Match SetMatch
    {
        set { _match = value; }
    }
    
    public void Click()
    {
        _match.Click();
    }

    private GamerActivate m_gamerActivate;
    private UIOverride m_ui;

    private void Start()
    {
        m_gamerActivate = GetComponent<GamerActivate>();
        _roomDB = GetComponentInParent<ActivateRoom>().GetRoomDB;
    }

    public void OnClickOpenGamerWindow()
    {
        EventManager.VibrateLight.Invoke();
        EventManager.OpenGamerWindow.Invoke(_roomDB, m_gamerActivate.GetGamerDB);
    }
}