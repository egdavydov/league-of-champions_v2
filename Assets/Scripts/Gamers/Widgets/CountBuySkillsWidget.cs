﻿using System.Linq;
using TMPro;
using UnityEngine;

namespace Gamers.Widgets
{
    public class CountBuySkillsWidget : MonoBehaviour
    {
        [SerializeField] private GameObject m_activeWidget;
        [SerializeField] private GameObject m_noActiveWidget;
        [SerializeField] private TextMeshProUGUI m_countTxt;

        private int _mCountImprovement;
        private bool m_active;

        private RoomsDB.Gamer m_gamer;
        public RoomsDB.Gamer SetGamer
        {
            set { m_gamer = value; }
        }

        private void Start()
        {
            EventManager.ChangeCoins.AddListener(Refresh);
            EventManager.ChangeSkillPoints.AddListener(Refresh);
            Refresh();
        }

        private void Refresh()
        {
            _mCountImprovement = 0;
            var allSkillFulled = m_gamer.skills.All(skill => skill.curLevel == 4);

            if(allSkillFulled && m_gamer.Perks < 3)
            {
                _mCountImprovement = 1;
            }
            else
            {
                foreach (var skill in m_gamer.skills)
                {
                    if (skill.curLevel < 4 && skill.IsAvailableToBuy())
                    {
                        _mCountImprovement++;
                    }
                }
            }

            m_active = _mCountImprovement > 0;
            m_activeWidget.SetActive(m_active);
            m_noActiveWidget.SetActive(!m_active);

            if (m_active) { m_countTxt.text = "" + _mCountImprovement; }
        }

        private void OnDestroy()
        {
            EventManager.ChangeCoins.RemoveListener(Refresh);
            EventManager.ChangeSkillPoints.RemoveListener(Refresh);
        }
    }
}
