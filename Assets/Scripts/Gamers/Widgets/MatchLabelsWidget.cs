﻿using TMPro;
using UnityEngine;

namespace Gamers.Widgets
{
    public class MatchLabelsWidget : MonoBehaviour
    {
        [SerializeField] private AnimationLabels m_winParticles;
        [SerializeField] private AnimationLabels m_lossParticles;
        [SerializeField] private TextMeshProUGUI[] m_coinsTxt;

        public void SetCoins(float coins)
        {
            foreach (var coinTxt in m_coinsTxt)
            {
                coinTxt.text = Reduction.ReductionNumber(coins, 0, true);
            }
        }

        public void PlayAnim(float coins)
        {
            if (coins > 0f)
            {
                m_winParticles.Play(coins);
            }
            else
            {
                m_lossParticles.Play(0f);
            }
        }
    }
}
