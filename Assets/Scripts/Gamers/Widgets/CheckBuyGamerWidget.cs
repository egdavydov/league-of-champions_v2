﻿using Game.Managers;
using Services;
using UnityEngine;

namespace Gamers.Widgets
{
    public class CheckBuyGamerWidget : MonoBehaviour
    {
        [SerializeField] private GameObject m_activeWidget;
        [SerializeField] private GameObject m_noActiveWidget;

        private bool m_activate;
        private RoomsDB.Gamer m_gamer;
        public RoomsDB.Gamer SetGamer
        {
            set { m_gamer = value; }
        }

        private void Start()
        {
            EventManager.ChangeCoins.AddListener(Refresh);
            EventManager.ChangeLevel.AddListener(Refresh);
            Refresh();
        }

        public void LockAllBtn()
        {
            m_activeWidget.SetActive(false);
            m_noActiveWidget.SetActive(false);
        }

        private void Refresh()
        {
            if (!ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(m_gamer.roomNumber).Activate)
            {
                m_activeWidget.SetActive(false);
                m_noActiveWidget.SetActive(true);
                return;
            }
        
            m_activate = m_gamer.accessLevel <= ApplicationManager.Instance.CollectionManagerRef.Base.curLevel;

            m_activeWidget.SetActive(m_activate);
            m_noActiveWidget.SetActive(!m_activate);
        }

        private void OnDestroy()
        {
            EventManager.ChangeCoins.RemoveListener(Refresh);
            EventManager.ChangeLevel.RemoveListener(Refresh);
        }
    }
}
