﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamerSkills : MonoBehaviour
{
    private GamerActivate m_gamerActivate;

    private void Start()
    {
        m_gamerActivate = GetComponent<GamerActivate>();

        for (int i = 0; i < m_gamerActivate.GetGamerDB.skills.Length; i++)
        {
            m_gamerActivate.GetGamerDB.skills[i].SetCurPrice(m_gamerActivate.GetRoomDB, m_gamerActivate.GetGamerDB);
        }
    }
}