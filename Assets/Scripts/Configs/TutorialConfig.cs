﻿using System;
using System.Collections.Generic;

namespace Configs
{
    [Serializable]
    public class TutorialConfig
    {
        public string Id;
        public List<TutorialStepConfig> Steps;
    }
}