﻿using System;
using System.Collections.Generic;
using Tutorial.Steps;
using UI.TutorialWindow;

namespace Configs
{
    [Serializable]
    public class TutorialStepConfig
    {
        public TutorialSteps Type;
        public int Id;
        public List<TutorialTargetConfig> Targets;
    }
}