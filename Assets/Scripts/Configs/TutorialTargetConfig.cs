﻿using System;
using Tutorial;
using Tutorial.Steps;
using UI.TutorialWindow;

namespace Configs
{
    [Serializable]
    public class TutorialTargetConfig : IEquatable<TutorialTag>
    {
        public int Id;
        public TutorialTargetTypes TargetType;
        public string Tag;
        public string Text;
        public TutorialView.Types ViewType;
        public BottomTutorial.KittyStates KittyType;
        public float Position;
        public bool NoScrollCamera;

        public override bool Equals(object obj)
        {
            return Equals(obj as TutorialTag);
        }

        public bool Equals(TutorialTag tutorialTag)
        {
            return tutorialTag != null && tutorialTag.Type == TargetType && tutorialTag.Tag == Tag;
        }
    }
}