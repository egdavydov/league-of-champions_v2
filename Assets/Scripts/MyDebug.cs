﻿using UnityEngine.UI;

namespace UnityEngine
{

    public class MyDebug : MonoBehaviour
    {
        [SerializeField] private GameObject _logBtn;
        private static Text _debugTxt;
        private static string _beforeLoadDebug;

        private void Start()
        {
#if PRODUCTION
            _logBtn.SetActive(false);
#else
            _logBtn.SetActive(true);
#endif
            
            _debugTxt = GameObject.Find("DebugTxt").GetComponent<Text>();
            _debugTxt.text = "Run\n" + _beforeLoadDebug;
            gameObject.SetActive(false);
        }
        
        public static void Log(object message)
        {
            Debug.Log(message);
            if (_debugTxt == null)
            {
                _beforeLoadDebug += message + "\n";
                return;
            }
            
            _debugTxt.text = _debugTxt.text + "\n" + (string) message;
        }

        public void OnBtn()
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }
    }
}
