﻿using UnityEngine;

namespace Tutorial
{
    public class Click : MonoBehaviour
    {
        private Tutorial _tutorial;
        [SerializeField] private GameObject _hand;
        public GameObject GetHand => _hand;

        private void OnEnable()
        {
            _tutorial = GetComponentInParent<Tutorial>();
        }

        private void OnMouseDown()
        {
            _tutorial.CheckTutor();
        }

        private void OnDestroy()
        {
            Destroy(_hand);
        }
    }
}
