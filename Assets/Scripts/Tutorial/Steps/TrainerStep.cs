using Configs;
using Game.Managers;
using UnityEngine.EventSystems;

namespace Tutorial.Steps
{
    public class TrainerStep : TutorialStep
    {
        private float _buyTrainerCoins;
        
        public TrainerStep(int id, TutorialStepConfig config, TutorialSystem tutorialSystem) : base(id, config, tutorialSystem)
        {
        }
        
        protected override void SetTargetData()
        {
            _buyTrainerCoins = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetTrainer.Price;
        }
        
        public override bool CheckShowTarget(TutorialTargetConfig target)
        {
            switch (target.Id)
            {
                case 1:
                    return true;
                case 2:
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].GetTrainer.Activate)
                    {
                        _tutorialSystem.CompleteStep();
                        break;
                    }
                    
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].TournamentCount > 0 && ApplicationManager.Instance.CollectionManagerRef.Base.curCoins >= _buyTrainerCoins)
                        return true;
                    
                    EventManager.ChangeCoins.AddListener(CheckBuyTrainer);
                    break;
                case 3:
                    return false;
                case 4:
                    return true;
                default:
                    return false;
            }

            return false;
        }

        public override bool CheckTargetComplete(TutorialTargetConfig target)
        {
            return target.Id != 1;
        }

        private void CheckBuyTrainer()
        {
            if (ApplicationManager.Instance.CollectionManagerRef.Base.curCoins < _buyTrainerCoins)
                return;            
            
            EventManager.ChangeCoins.RemoveListener(CheckBuyTrainer);
            _tutorialSystem.TutorialWindowController.ShowTarget();
        }
    }
}