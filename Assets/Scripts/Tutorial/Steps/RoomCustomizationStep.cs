using Configs;
using Game.Managers;

namespace Tutorial.Steps
{
    public class RoomCustomizationStep : TutorialStep
    {
        public RoomCustomizationStep(int id, TutorialStepConfig config, TutorialSystem tutorialSystem) : base(id, config, tutorialSystem)
        {
        }
        
        public override bool CheckShowTarget(TutorialTargetConfig target)
        {
            switch (target.Id)
            {
                case 1:
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].CustomLogo != null)
                    {
                        _tutorialSystem.CompleteStep();
                        break;
                    }

                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].GetGamers[1].activate && ApplicationManager.Instance.CollectionManagerRef.Base.curLevel >= 6)
                        return true;
                    
                    EventManager.ChangeLevel.AddListener(CheckLevelAndGamer);
                    EventManager.BuyGamer.AddListener(CheckLevelAndGamer);
                    break;
                case 2:
                    return true;
                case 3:
                    return false;
                case 4:
                    return false;
                default:
                    return false;
            }

            return false;
        }

        private void CheckLevelAndGamer(int room, RoomsDB.Gamer gamer)
        {
            CheckLevelAndGamer();
        }
        
        private void CheckLevelAndGamer()
        {
            if (!(ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].GetGamers[1].activate && ApplicationManager.Instance.CollectionManagerRef.Base.curLevel >= 6))
                return;

            EventManager.ChangeLevel.RemoveListener(CheckLevelAndGamer);
            EventManager.BuyGamer.RemoveListener(CheckLevelAndGamer);
            _tutorialSystem.TutorialWindowController.ShowTarget();
        }
    }
}