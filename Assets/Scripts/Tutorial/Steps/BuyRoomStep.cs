using Configs;
using Game.Managers;

namespace Tutorial.Steps
{
    public class BuyRoomStep : TutorialStep
    {
        private float _buyRoomCoins;
        private int _buyRoomLevel;
        public BuyRoomStep(int id, TutorialStepConfig config, TutorialSystem tutorialSystem) : base(id, config, tutorialSystem)
        {
        }
        
        protected override void SetTargetData()
        {
            _buyRoomCoins = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(1).PriceAccess;
            _buyRoomLevel = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(1).LevelAccess;
        }
        
        public override bool CheckShowTarget(TutorialTargetConfig target)
        {
            switch (target.Id)
            {
                case 1:
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[1].Activate)
                    {
                        _tutorialSystem.CompleteStep();
                        break;
                    }

                    if (ApplicationManager.Instance.CollectionManagerRef.Base.curCoins >= _buyRoomCoins && ApplicationManager.Instance.CollectionManagerRef.Base.curLevel >= _buyRoomLevel)
                        return true;
                    
                    EventManager.ChangeRoom.AddListener(CheckBuyRoom);
                    EventManager.ChangeCoins.AddListener(CheckBuyRoom);
                    break;
                default:
                    return false;
            }

            return false;
        }

        private void CheckBuyRoom()
        {
            CheckBuyRoom(1);
        }

        private void CheckBuyRoom(int number)
        {
            if (number != 1)
                return;
            
            if (!(ApplicationManager.Instance.CollectionManagerRef.Base.curCoins >= _buyRoomCoins && ApplicationManager.Instance.CollectionManagerRef.Base.curLevel >= _buyRoomLevel))
                return;
            
            EventManager.ChangeRoom.RemoveListener(CheckBuyRoom);
            _tutorialSystem.TutorialWindowController.ShowTarget();
        }
    }
}