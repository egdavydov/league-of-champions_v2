using Configs;
using Game.Managers;

namespace Tutorial.Steps
{
    public class UpgradeStep : TutorialStep
    {
        private float _upgradeRoomCoins;
        private float _nextLevelExp;
        
        public UpgradeStep(int id, TutorialStepConfig config, TutorialSystem tutorialSystem) : base(id, config, tutorialSystem)
        {
        }
        
        protected override void SetTargetData()
        {
            _upgradeRoomCoins = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).CurPriceSponsors;
            _nextLevelExp = ApplicationManager.Instance.CollectionManagerRef.Base.PriceNextLevel;
        }
        
        public override bool CheckTargetComplete(TutorialTargetConfig target)
        {
            switch (target.Id)
            {
                case 1:
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).curLevelSponsors > 1f)
                        return true;
                    
                    break;
                case 2:
                    return true;
                default:
                    return false;
            }
            
            return false;
        }

        public override bool CheckShowTarget(TutorialTargetConfig target)
        {
            switch (target.Id)
            {
                case 1:
                    if (ApplicationManager.Instance.CollectionManagerRef.Base.curCoins >= _upgradeRoomCoins)
                        return true;
                    
                    EventManager.ChangeCoins.AddListener(CheckUpgradeRoom);
                    break;
                case 2:
                    if (ApplicationManager.Instance.CollectionManagerRef.Base.curLevel > 1)
                    {
                        _tutorialSystem.CompleteStep();
                        break;
                    }
                    
                    if (ApplicationManager.Instance.CollectionManagerRef.Base.curExperience >= _nextLevelExp)
                        return true;
                    
                    EventManager.ChangeExperience.AddListener(CheckLevelUp);
                    break;
                case 3:
                    return false;
                default:
                    return false;
            }

            return false;
        }

        private void CheckUpgradeRoom()
        {
            if (ApplicationManager.Instance.CollectionManagerRef.Base.curCoins < _upgradeRoomCoins)
                return;

            EventManager.ChangeCoins.RemoveListener(CheckUpgradeRoom);
            _tutorialSystem.TutorialWindowController.ShowTarget();
        }
        
        private void CheckLevelUp()
        {
            if (ApplicationManager.Instance.CollectionManagerRef.Base.curExperience < _nextLevelExp ) return;
            
            EventManager.ChangeExperience.RemoveListener(CheckLevelUp);
            _tutorialSystem.TutorialWindowController.ShowTarget();
        }
    }
}