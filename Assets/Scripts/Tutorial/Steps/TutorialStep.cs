using System.Collections.Generic;
using Configs;
using Core.StatesMachine.States;
using Game.Managers;
using UI.TutorialWindow;
using UnityEngine;

namespace Tutorial.Steps
{
    public abstract class TutorialStep : State
    {
        private readonly TutorialStepConfig _config;
        protected readonly TutorialSystem _tutorialSystem;

        protected TutorialStep(int id, TutorialStepConfig config, TutorialSystem tutorialSystem) : base(id)
        {
            _config = config;
            _tutorialSystem = tutorialSystem;
        }
        
        public override void Enter()
        {
            base.Enter();
            Debug.LogWarning($"Step into {_config.Type}");
            SetTargetData();
            ApplicationManager.Instance.UISystem.Show(ViewType.TutorialView, 
                new TutorialWindowParams(_config, this)
            );
        }

        public override void Update()
        {
            
        }

        protected virtual void SetTargetData()
        {
            
        }

        public virtual bool CheckTargetComplete(TutorialTargetConfig target)
        {
            return true;
        }

        public virtual bool CheckShowTarget(TutorialTargetConfig target)
        {
            return true;
        }
    }
}