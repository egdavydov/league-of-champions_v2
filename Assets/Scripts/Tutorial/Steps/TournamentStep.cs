using Configs;
using Game.Managers;

namespace Tutorial.Steps
{
    public class TournamentStep : TutorialStep
    {
        public TournamentStep(int id, TutorialStepConfig config, TutorialSystem tutorialSystem) : base(id, config, tutorialSystem)
        {
        }
        
        public override bool CheckShowTarget(TutorialTargetConfig target)
        {
            switch (target.Id)
            {
                case 1:
                    EventManager.TournamentRoomReady.RemoveListener(CheckTournamentReady);
                    if (ApplicationManager.Instance.RoomsManagerRef.Rooms[0].GetTournamentWidget.TournamentReady)
                        return true;
                    
                    EventManager.TournamentRoomReady.AddListener(CheckTournamentReady);
                    break;
                case 2:
                    return false;
                default:
                    return false;
            }

            return false;
        }

        private void CheckTournamentReady(int roomNumber)
        {
            EventManager.TournamentRoomReady.RemoveListener(CheckTournamentReady);
            _tutorialSystem.TutorialWindowController.ShowTarget();
        }
    }
}