﻿namespace Tutorial.Steps
{
    public enum TutorialSteps
    {
        IntroductionStep,
        UpgradeStep,
        GamerBuyStep,
        RoomCustomizationStep,
        TournamentStep,
        TrainerStep,
        BuyRoomStep
    }
}
