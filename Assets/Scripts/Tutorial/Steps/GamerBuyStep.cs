using Configs;
using Game.Managers;

namespace Tutorial.Steps
{
    public class GamerBuyStep : TutorialStep
    {
        private float _buyGamerCoins;
        private int _buyGamerLevel;
        
        public GamerBuyStep(int id, TutorialStepConfig config, TutorialSystem tutorialSystem) : base(id, config, tutorialSystem)
        {
        }
        
        protected override void SetTargetData()
        {
            _buyGamerCoins = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetGamers[1].accesPrice;
            _buyGamerLevel = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetGamers[1].accessLevel;
        }
        
        public override bool CheckShowTarget(TutorialTargetConfig target)
        {
            switch (target.Id)
            {
                case 1:
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].GetGamers[1].activate)
                    {
                        _tutorialSystem.CompleteStep();
                        break;
                    }
                    
                    if (ApplicationManager.Instance.CollectionManagerRef.Base.curLevel >= _buyGamerLevel)
                        return true;
                    
                    EventManager.ChangeCoins.AddListener(CheckBuyGamer);
                    EventManager.ChangeLevel.AddListener(CheckBuyGamer);
                    break;
                case 2:
                    return true;
                default:
                    return false;
            }

            return false;
        }

        private void CheckBuyGamer()
        {
            if (ApplicationManager.Instance.CollectionManagerRef.Base.curLevel < _buyGamerLevel)
                return;

            EventManager.ChangeCoins.RemoveListener(CheckBuyGamer);
            EventManager.ChangeLevel.RemoveListener(CheckBuyGamer);
            _tutorialSystem.TutorialWindowController.ShowTarget();
        }
    }
}