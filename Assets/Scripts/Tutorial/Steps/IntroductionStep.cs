﻿using Configs;
using Game.Managers;

namespace Tutorial.Steps
{
    public class IntroductionStep : TutorialStep
    {
        private float _needCoins;

        public IntroductionStep(int id, TutorialStepConfig config, TutorialSystem tutorialSystem) : base(id, config, tutorialSystem)
        {
        }

        protected override void SetTargetData()
        {
            _needCoins = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetGamerByNumber(0).skills[0].basePrice;
        }

        public override bool CheckShowTarget(TutorialTargetConfig target)
        {
            return !(target.Id == 3 || target.Id == 4);
        }

        public override bool CheckTargetComplete(TutorialTargetConfig target)
        {
            switch (target.Id)
            {
                case 1:
                    if (ApplicationManager.Instance.CollectionManagerRef.Base.curCoins >= _needCoins)
                        return true;
                    
                    break;
                case 2:
                    return true;
                case 3:
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetGamerByNumber(0).skills[0].curLevel > 0)
                        return true;
                    break;
                default:
                    return false;
            }

            return false;
        }
    }
}
