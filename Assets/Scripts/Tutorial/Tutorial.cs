﻿using Game.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Tutorial
{
    public class Tutorial : MonoBehaviour
    {
        [SerializeField] private Click[] _tutorials;
        private int _curTutor;
        private float _needСoins;
        private ScrollCamera _swipe;
        public int CurTutor => _curTutor;

        private void Start()
        {
            if (!ApplicationManager.Instance.SaveSystemRef.ShowTutorial)
            {
                Destroy(gameObject);
                return;
            }
        
            _curTutor = PlayerPrefs.GetInt("Tutor");
            if (_curTutor >= _tutorials.Length)
            {
                Destroy(gameObject);
                return;
            }

            ApplicationManager.Instance.TutorialRef = this;
            _swipe = Camera.main.GetComponent<ScrollCamera>();
            _swipe.IsSwipe = false;

            var transform = _swipe.GetComponent<Transform>();
            var position = transform.position;
            position.y = -3f;
            transform.position = position;
        
           
                
            GetTutor();
        }

        private void GetTutor()
        {
            PlayerPrefs.SetInt("Tutor", _curTutor);
            PlayerPrefs.Save();
        
            for (var i = 0; i < _tutorials.Length; i++)
            {
                _tutorials[i].GetHand.SetActive(i == _curTutor);
                _tutorials[i].gameObject.SetActive(i == _curTutor);
            }

            switch (_curTutor)
            {
                case 0:
                    _needСoins = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetGamerByNumber(0).skills[0].basePrice;
                    break;
                case 1:
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetGamerByNumber(0).skills[0].curLevel > 0)
                    {
                        NextStep();
                    }

                    break;
                case 2:
                    if (ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetGamerByNumber(0).skills[0].curLevel > 0)
                    {
                        NextStep();
                        break;
                    }
                    
                    EventManager.ChangeSkill.AddListener(CheckSkillUpgrade);
                    break;
                default:
                    _swipe.IsSwipe = true;
                    PlayerPrefs.Save();
                    Destroy(gameObject);
                    break;
            }
        }
        
        private void CheckSkillUpgrade()
        {
            if (ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(0).GetGamerByNumber(0).skills[0].curLevel == 0) return;
            
            EventManager.ChangeSkill.RemoveListener(CheckSkillUpgrade);
            NextStep();
        }

        private void NextStep()
        {
            _curTutor++;
            GetTutor();
        }

        public void CheckTutor()
        {
            switch (_curTutor)
            {
                case 0:
                    if (ApplicationManager.Instance.CollectionManagerRef.Base.curCoins < _needСoins)
                        return;
                    break;
                case 1:
                    break;
                case 2:
                    break;
                default:
                    return;
            }

            NextStep();
        }

        private void OnDestroy()
        {
            ApplicationManager.Instance.TutorialRef = null;
        }
    }
}