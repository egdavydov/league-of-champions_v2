﻿using System;
using Game.Managers;
using Tutorial.Steps;
using UnityEngine;

namespace Tutorial
{
    public class TutorialTag : MonoBehaviour
    {
        public TutorialTargetTypes Type;
        public string Tag;

        public GameObject Target => gameObject;

        private void Awake()
        {
            ApplicationManager.Instance.TutorialSystem.RegisterTag(this);
        }

        public Vector2 GetCanvasPosition(Vector2 canvasSize)
        {
            var viewportPosition = Camera.main.WorldToViewportPoint(transform.position);
            return new Vector2(
                Mathf.Clamp((viewportPosition.x - 0.5f) * canvasSize.x, canvasSize.x * -.5f, canvasSize.x * .5f - 80f),
                Mathf.Clamp((viewportPosition.y - 0.5f) * canvasSize.y, canvasSize.y * -.5f + 130f, canvasSize.y * .5f)
                );  
        }
    }
}
