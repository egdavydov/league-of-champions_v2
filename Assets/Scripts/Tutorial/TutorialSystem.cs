﻿using System;
using System.Collections;
using System.Collections.Generic;
using Configs;
using Core.CoroutineSystem;
using Core.StatesMachine;
using Game.Managers;
using Managers;
using Tutorial.Steps;
using UI.TutorialWindow;
using UnityEngine;

namespace Tutorial
{
    public class TutorialSystem
    {
        private readonly StatesMachine _statesMachine;
        private TutorialConfig _config;
        private int _currentStepIndex;
        private TutorialSteps _currentStep;
        private bool _isInit;
        
        private const string playerPrefsStepKey = "TutorialStep";
        
        private List<TutorialTag> _registeredTags = new List<TutorialTag>();

        public List<TutorialTag> RegisteredTags => _registeredTags;
        public TutorialWindowController TutorialWindowController { get; set; }

        public TutorialSystem(ICoroutineManager coroutineManager)
        {
            _statesMachine = new StatesMachine(coroutineManager);
            CreateSteps();
        }

        private async void CreateSteps()
        {
            _config = await EmbeddedParamsManager.Instance.Get<TutorialConfig>();
            _config.Steps.ForEach(configStep => _statesMachine.AddState(configStep.Type, configStep, this));
            _currentStepIndex = PlayerPrefs.GetInt(playerPrefsStepKey);
            _isInit = true;
        }

        public void RegisterTag(TutorialTag tag)
        {
            _registeredTags.Add(tag);
        }

        public void BeginStep()
        {
            if (!_isInit)
            {
                ApplicationManager.Instance.CoroutineSystem.StartCoroutine(this, WaitInit());
                return;
            }
            
            if(_currentStepIndex >= _config.Steps.Count)
                return;
            
            _currentStep = _config.Steps[_currentStepIndex].Type;
            Debug.Log($"Begin tutorial step {_currentStep}");
            _statesMachine.SwitchTo((int)_currentStep);
        }

        public void CompleteStep()
        {
            TutorialWindowController.CompleteTutorialStep();
            _currentStepIndex++;
            PlayerPrefs.SetInt(playerPrefsStepKey, _currentStepIndex);
            PlayerPrefs.Save();

            if (_currentStepIndex < _config.Steps.Count)
            {
                BeginStep();
                return;
            }

            Debug.Log("Tutorial complete!");
        }

        public TutorialStep GetActiveTutorialStep()
        {
            return _statesMachine.ActiveState as TutorialStep;
        }

        private IEnumerator WaitInit()
        {
            while (!_isInit)
                yield return null;
            
            BeginStep();
        }
    }
}
