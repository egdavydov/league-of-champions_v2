﻿using System;
using System.Collections;
using Advertising;
using Game.Managers;
using Rooms;
using Services;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UI.RoomsUpgrades
{
    public abstract class AUpgradeRoom : MonoBehaviour
    {
        public TextMeshProUGUI CurLevelTxt;
        public TextMeshProUGUI PriceNextLevelTxt;
    
        [Header("Bought")]
        public GameObject AllBought;
        public GameObject NoAllBought;
        public GameObject NoActive;
    
        [Header("Activate")]
        public GameObject ActivateBtn;
        public GameObject NoActivateBtn;
        public GameObject AdvActivateBtn;
    
        protected bool _active;
        protected float PriceNextLevel;
        protected RoomsDB RoomDB;
        protected ActivateRoom Room;
        private int _number;
        private int _showAdvBtnTimeoutMin;
        private int _showAdvBtnTimeoutMax;
        private float _showAdvBtnTimeout;
        private float _showAdvBtnTimer;
        private float _hideAdvBtnTimeout;
        private int _limitAdvPrice;
        private float _startPricePercent;
        private float _endPricePercent;
        private bool _isFulled;
        private Coroutine _hideAdvBtnHideCoroutine;

        private void Start()
        {
            EventManager.ChangeCoins.AddListener(Refresh);

            _showAdvBtnTimeoutMin = ApplicationManager.Instance.CollectionManagerRef.Base.UpgradeRoomAdvRepeatMin;
            _showAdvBtnTimeoutMax = ApplicationManager.Instance.CollectionManagerRef.Base.UpgradeRoomAdvRepeatMax;
            _hideAdvBtnTimeout = ApplicationManager.Instance.CollectionManagerRef.Base.UpgradeRoomAdvShowed;
            _limitAdvPrice = ApplicationManager.Instance.CollectionManagerRef.Base.UpgradeRoomAdvLimitPrice;
            _startPricePercent = ApplicationManager.Instance.CollectionManagerRef.Base.UpgradeRoomAdvStartPercent;
            _endPricePercent = ApplicationManager.Instance.CollectionManagerRef.Base.UpgradeRoomAdvEndPercent;
            HideAdvBtn();
        }

        public void Open(int numberRoom)
        {
            _number = numberRoom;
            RoomDB = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(_number);
            Room = ApplicationManager.Instance.RoomsManagerRef.Rooms[_number];
            Reference();
            SetPriceNextLevel();
            HideAdvBtn();
            Refresh();
        }

        protected abstract void SetPriceNextLevel();
    
        protected abstract void Refresh();
    
        protected abstract void Reference();

        public void OnBuyBtn()
        {
            if(!_active)
                return;
        
            EventManager.VibrateMedium.Invoke();
            UpgradeRoom();
            
            var price = PriceNextLevel * -1;
            SetPriceNextLevel();
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(price);
        }

        public void OnAdvBtn()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
        
            MaxAppLovin.Instance.HiddenRevard.AddListener(AfterAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
            if(_hideAdvBtnHideCoroutine != null)
                StopCoroutine(_hideAdvBtnHideCoroutine);
            HideAdvBtn();
        }

        private void AfterAdv()
        {
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(AfterAdv);
            UpgradeRoom();
            SetPriceNextLevel();
            Refresh();
            FacebookEventSubscribers.LogRoomUpgradeAdvEvent();
        }

        protected abstract void UpgradeRoom();

        private void ShowAdvBtn()
        {
            AdvActivateBtn.SetActive(true);
            _hideAdvBtnHideCoroutine = StartCoroutine(HideAdvBtnEnumerator());
        }

        private void HideAdvBtn()
        {
            AdvActivateBtn.SetActive(false);
            _showAdvBtnTimeout = -1f;
        }

        private IEnumerator HideAdvBtnEnumerator()
        {
            yield return new WaitForSeconds(_hideAdvBtnTimeout);
            HideAdvBtn();
        }
    
        private void Update()
        {
            if(RoomDB == null)
                return;
            
            if(!RoomDB.Activate)
                return;
            
            var curCoins = ApplicationManager.Instance.CollectionManagerRef.Base.curCoins;
            if(_isFulled || curCoins >= PriceNextLevel || AdvActivateBtn.activeSelf)
                return;
            
            var notEnoughCoins = PriceNextLevel - curCoins;
            if(notEnoughCoins < _limitAdvPrice)
                return;

            var notEnoughPercent = 1 - curCoins / PriceNextLevel;
            if (!(notEnoughPercent >= _startPricePercent) || !(notEnoughPercent <= _endPricePercent))
            {
                _showAdvBtnTimeout = -1f;
                return;
            }

            if (_showAdvBtnTimeout < 0)
            {
                _showAdvBtnTimeout = Random.Range(_showAdvBtnTimeoutMin, _showAdvBtnTimeoutMax);
                _showAdvBtnTimer = 0f;
            }

            if( _showAdvBtnTimer >= _showAdvBtnTimeout )
                ShowAdvBtn();

            _showAdvBtnTimer += Time.deltaTime;
        }

        protected void Show(string to, float curLevel, float maxLevel, string after)
        {
            if (!RoomDB.Activate)
            {
                AllBought.SetActive(false);
                NoAllBought.SetActive(false);
                NoActive.SetActive(true);
                CurLevelTxt.text = "---";
                return;
            }
        
            NoActive.SetActive(false);
            CurLevelTxt.text = to + curLevel + after;
            _isFulled = curLevel >= maxLevel;
            AllBought.SetActive(_isFulled);
            NoAllBought.SetActive(!_isFulled);

            if (_isFulled)
                return;

            PriceNextLevelTxt.text = Reduction.ReductionNumber(PriceNextLevel, 0, true);
            _active = PriceNextLevel <= ApplicationManager.Instance.CollectionManagerRef.Base.curCoins;
            ActivateBtn.SetActive(_active);
            NoActivateBtn.SetActive(!_active);
        }
    
        private void OnDestroy()
        {
            EventManager.ChangeCoins.RemoveListener(Refresh);
        }
    }
}
