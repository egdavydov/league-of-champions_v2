﻿using System;
using Game.Managers;
using Services;
using UnityEngine;

namespace UI.RoomsUpgrades
{
    public class UpgradeSponsors : AUpgradeRoom
    {
        [SerializeField] private AnimationParticles _coinsAnimation;
    
        protected override void SetPriceNextLevel()
        {
            RoomDB.SetUpgradeSponsorsPrice();
            PriceNextLevel = RoomDB.CurPriceSponsors;
        }

        protected override void Refresh()
        {
            if (RoomDB == null)
            {
                return;
            }
        
            Show("x", RoomDB.curLevelSponsors, RoomDB.maxLevelSponsors, "");
        }

        protected override void Reference()
        {
        
        }

        protected override void UpgradeRoom()
        {
            _coinsAnimation.Play();
            RoomDB.AddLevelSponsors();
        
            foreach (var gamer in Room.GetGamersPlacer.GetGamersActivate)
            {
                if (gamer.IsActive)
                {
                    gamer.GenerateParameters();
                }
            }
            
            FacebookEventSubscribers.LogSponsor_Room_NEvent(RoomDB.Number);
            if (Mathf.Approximately(RoomDB.maxLevelLeague, RoomDB.curLevelLeague) &&
                Mathf.Approximately(RoomDB.maxLevelSpeed, RoomDB.curLevelSpeed) &&
                Mathf.Approximately(RoomDB.maxLevelSponsors, RoomDB.curLevelSponsors))
                FacebookEventSubscribers.LogUpgrade_Room_N_MaxEvent(RoomDB.Number);
        }
    }
}