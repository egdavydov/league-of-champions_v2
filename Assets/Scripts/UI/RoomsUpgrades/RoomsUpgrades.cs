﻿using System.Collections;
using System.Collections.Generic;
using UI.RoomsUpgrades;
using UnityEngine;

public class RoomsUpgrades : MonoBehaviour
{
    private int _numberRoom;
    private Animator _anim;
    private WaitForSeconds _delay;
    private AUpgradeRoom[] _upgrades;
    
    private void Start()
    {
        _anim = GetComponent<Animator>();
        _delay = new WaitForSeconds(0.02f);
        _upgrades = GetComponentsInChildren<AUpgradeRoom>();
        
        EventManager.ChangeRoom.AddListener((i => StartCoroutine(Open(i))));
    }

    public int NumberRoom
    {
        get => _numberRoom;
        set => _numberRoom = value;
    }
    
    private bool IsAnimationPlaying()
    {        
        var animatorStateInfo = _anim.GetCurrentAnimatorStateInfo(0);
        if (animatorStateInfo.IsName("Close") || animatorStateInfo.IsName("Idle"))             
            return true;

        return false;
    }

    private IEnumerator Open(int i)
    {
        if (_anim.GetBool("Open"))
        {
            _anim.SetBool("Open", false);
            
            while (!IsAnimationPlaying())
            {
                yield return _delay;
            }
        }

        foreach (var upgrade in _upgrades)
        {
            upgrade.Open(i);
        }
        
        _anim.SetBool("Open", true);
    }

    private void OnDestroy()
    {
        EventManager.ChangeRoom.AddListener((i => StartCoroutine(Open(i))));
    }
}
