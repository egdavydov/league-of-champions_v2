﻿using System;
using Game.Managers;
using Services;
using UnityEngine;

namespace UI.RoomsUpgrades
{
    public class UpgradeSpeed : AUpgradeRoom
    {
        protected override void SetPriceNextLevel()
        {
            RoomDB.SetUpgradeSpeedPrice();
            PriceNextLevel = RoomDB.CurPriceSpeed;
        }

        protected override void Refresh()
        {
            if (RoomDB == null)
            {
                return;
            }
        
            Show("x", RoomDB.curLevelSpeed, RoomDB.maxLevelSpeed, "");
        }

        protected override void Reference()
        {
        
        }

        protected override void UpgradeRoom()
        {
            RoomDB.AddLevelSpeed();
            
            foreach (var gamer in Room.GetGamersPlacer.GetGamersActivate)
            {
                if (!gamer.IsActive) continue;
            
                var match = gamer.GetComponent<Match>();
                if (match != null)
                {
                    match.GenerateDelayMatch();
                }

                gamer.GetParticles.Play();
                gamer.GenerateParameters();
            }

            FacebookEventSubscribers.LogSpeed_Room_NEvent(RoomDB.Number);
            if (Mathf.Approximately(RoomDB.maxLevelLeague, RoomDB.curLevelLeague) &&
                Mathf.Approximately(RoomDB.maxLevelSpeed, RoomDB.curLevelSpeed) &&
                Mathf.Approximately(RoomDB.maxLevelSponsors, RoomDB.curLevelSponsors))
                FacebookEventSubscribers.LogUpgrade_Room_N_MaxEvent(RoomDB.Number);
        }
    }
}