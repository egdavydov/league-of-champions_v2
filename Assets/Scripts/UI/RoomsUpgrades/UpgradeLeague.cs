﻿using System;
using Fans;
using Game.Managers;
using Services;
using UnityEngine;

namespace UI.RoomsUpgrades
{
    public class UpgradeLeague : AUpgradeRoom
    {
        [SerializeField] private LeaguesWidget _leaguesWidget;
        private FansManager _fans;
    
        protected override void SetPriceNextLevel()
        {
            RoomDB.SetUpgradeLeaguePrice();
            PriceNextLevel = RoomDB.CurPriceLeague;
        }

        protected override void Refresh()
        {
            if (RoomDB == null)
            {
                return;
            }
        
            Show("", RoomDB.curLevelLeague, RoomDB.maxLevelLeague, "/25");
        }

        protected override void Reference()
        {
            _fans = Room.GetFans;
        }

        protected override void UpgradeRoom()
        {
            _fans.LimitFan();
            foreach (var fan in _fans.GetPlayFans)
            {
                fan.NextLeague();
            }

            RoomDB.AddLevelLeague();
            RoomDB.SetCurBaseBetLeague();
        
            foreach (var gamer in Room.GetGamersPlacer.GetGamersActivate)
            {
                if (gamer.IsActive)
                {
                    gamer.GenerateParameters();
                }
            }
        
            _leaguesWidget.Refresh();
            EventManager.AddLeagueUpgrade.Invoke();
            
            FacebookEventSubscribers.LogLeague_Room_NEvent(RoomDB.Number);
            if (Mathf.Approximately(RoomDB.maxLevelLeague, RoomDB.curLevelLeague) &&
                Mathf.Approximately(RoomDB.maxLevelSpeed, RoomDB.curLevelSpeed) &&
                Mathf.Approximately(RoomDB.maxLevelSponsors, RoomDB.curLevelSponsors))
                FacebookEventSubscribers.LogUpgrade_Room_N_MaxEvent(RoomDB.Number);
        }
    }
}