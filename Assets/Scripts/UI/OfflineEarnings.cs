﻿using System;
using Advertising;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class OfflineEarnings : BasicWindow
    {
        [SerializeField] private TextMeshProUGUI _coinsTxt;
        [SerializeField] private GameObject _advAcivateBtn;
        [SerializeField] private GameObject _advUnactivateBtn;
        private float _secondsPassed = 0;
        private float _secondsMultiplier = 0;
        private float _allCoinsAdd = 0;
        private int _allExperienceAdd;
        private DateTime _timePlay;
        private DateTime _timeStop;
        private string _stringTimeStop;
        private DateTime? _pauseTime = null;

        private void Start()
        {
            RefreshAdvButton();
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
        }

        public override void OpenWindow()
        {
            _stringTimeStop = ApplicationManager.Instance.CollectionManagerRef.Base.TimeStop;
            if (string.IsNullOrEmpty(_stringTimeStop))
            {
                ApplicationManager.Instance.TutorialSystem.BeginStep();
                return;
            }

            CheckTime();
            GetCoins();
            base.OpenWindow();
        }

        private void CheckTime()
        {
            _timePlay = DateTime.Now;
            _timeStop = DateTime.Parse(_stringTimeStop);

            if (_timeStop > _timePlay)
            {
                Debug.Log("cheater");
                ApplicationManager.Instance.SaveSystemRef.ResetDefault();
                SceneManager.LoadScene(0);
            }
        
            _secondsPassed = (int) (_timePlay - _timeStop).TotalSeconds;
            _secondsMultiplier = ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier;

            Debug.Log($"second {_secondsPassed}");
        }
    
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                _pauseTime = DateTime.Now;
            }
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus) return;
            
            if (_pauseTime == null)
                return;
                
            _stringTimeStop = _pauseTime.ToString();
                
            CheckTime();
            GetCoins();

            if (_secondsPassed <= 120)
            {
                ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(_allCoinsAdd);
                ApplicationManager.Instance.CollectionManagerRef.Base.AddExperience(_allExperienceAdd);
            }
            else
            {
                RefreshAdvButton();
                base.OpenWindow();
            }

            _pauseTime = null;
        }
#endif

        private void GetCoins()
        {
            _allExperienceAdd = 0;
            _allCoinsAdd = 0f;

            foreach (var roomDB in ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB)
            {
                if (!roomDB.Activate) continue;
                
                var timeLimitSeconds = roomDB.GetTrainer.GetOfflineEarningTimeLimit(ApplicationManager.Instance.CollectionManagerRef.Base) * 60;
                var roomSecondsPassed = Mathf.Clamp(_secondsPassed, 0, timeLimitSeconds);
                
                var secondsMultiplierSeconds = Mathf.Clamp(_secondsMultiplier, 0, roomSecondsPassed);
                var secondsPassedEarnings = roomSecondsPassed - secondsMultiplierSeconds;

                var roomCoins = 0f;
                foreach (var gamer in roomDB.GetGamers)
                {
                    if (!gamer.activate) continue;
                    
                    roomCoins += gamer.coinsInSecond * secondsPassedEarnings;
                    roomCoins += (gamer.coinsInSecond * 2) * secondsMultiplierSeconds;
                    roomCoins = Mathf.Clamp(
                        value: roomCoins,
                        min: roomDB.curBaseBetLeague * roomDB.curLevelSponsors,
                        max: float.MaxValue);
                    
                    _allExperienceAdd += (int) (gamer.winMatchInSecond * _secondsPassed);
                }

                roomCoins *= roomDB.GetTrainer.GetOfflineEarningRevenue(ApplicationManager.Instance.CollectionManagerRef.Base);

                _allCoinsAdd += roomCoins;
            }

            _allCoinsAdd = Mathf.Round(_allCoinsAdd);
            _coinsTxt.text = Reduction.ReductionNumber(_allCoinsAdd, 0, true);
        }

        public void Reward()
        {
            EventManager.VibrateMedium.Invoke();
        
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(_allCoinsAdd);
            ApplicationManager.Instance.CollectionManagerRef.Base.AddExperience(_allExperienceAdd);
            base.CloseWindow();
        }

        protected override void WindowClosedHandler()
        {
            ApplicationManager.Instance.TutorialSystem.BeginStep();
        }

        private void RefreshAdv(string s)
        {
            RefreshAdvButton();
        }

        private void RefreshAdvButton()
        {
            var advActivate = MaxAppLovin.Instance.IsRewardedAdReady;
            _advAcivateBtn.SetActive(advActivate);
            _advUnactivateBtn.SetActive(!advActivate);
        }

        public void OnBtnAdv()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
        
            MaxAppLovin.Instance.HiddenRevard.AddListener(RewardAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }
    
        private void RewardAdv()
        {
            FacebookEventSubscribers.LogOfflineBoostEvent();
            _allCoinsAdd *= 3;
            Reward();
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(RewardAdv);
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshAdv;
        }

    }
}