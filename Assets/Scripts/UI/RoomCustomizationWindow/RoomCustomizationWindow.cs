using System;
using Core.UI.Base;
using Game.Managers;
using TMPro;
using Tutorial;
using Tutorial.Steps;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.RoomCustomizationWindow
{
    public class RoomCustomizationWindow : View<RoomCustomizationWindowController, RoomCustomizationWindowParams>
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private Button _exitBtn;
        [SerializeField] private Button _editLogoBtn;
        [SerializeField] private Button _editNameBtn;
        [SerializeField] private GameObject _logosContainer;
        [SerializeField] private LogoItem _logoItem;
        [SerializeField] private Image _logoImage;
        [SerializeField] private TextMeshProUGUI _nameText;
        [SerializeField] private TMP_InputField _nameInput;
        [SerializeField] private TextMeshProUGUI _nameMaxLengthText;
        
        [SerializeField] private GameObject _mainState;
        [SerializeField] private GameObject _logoEditState;
        [SerializeField] private GameObject _nameEditState;
        [SerializeField] private StateButton _saveBtn;
        [SerializeField] private StateButton _nextBtn;
        
        public GameObject MainState => _mainState;
        public GameObject LogoEditState => _logoEditState;
        public GameObject NameEditState => _nameEditState;
        public StateButton SaveBtn => _saveBtn;
        public StateButton NextBtn => _nextBtn;
        public Image LogoImage => _logoImage;

        public TextMeshProUGUI NameText => _nameText;
        public TMP_InputField NameInput => _nameInput;
        public TextMeshProUGUI NameMaxLengthText => _nameMaxLengthText;

        private Animator _viewAnimator;
        private bool _isInitialized;
        private Canvas _canvas;
        private bool _isOpen;
        
        private void Awake()
        {
            _canvas = gameObject.GetComponent<Canvas>();
            if (_canvas.worldCamera == null)
                _canvas.worldCamera = Camera.main;
            _viewAnimator = GetComponent<Animator>();
            _exitBtn.onClick.AddListener(CloseClick);
            _editLogoBtn.onClick.AddListener(EditLogoClick);
            _editNameBtn.onClick.AddListener(EditNameClick);
            _saveBtn.AddClickListener(Controller.SaveClickHandler);
            _nextBtn.AddClickListener(Controller.NextClickHandler);
            _nameInput.onValueChanged.AddListener(Controller.InputChangedHandler);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public override void Show()
        {
            Controller.Init();
            _exitBtn.gameObject.SetActive(!Controller.FirstRun);

            if (!_isInitialized)
            {
                foreach (var logo in Params.CustomLogos)
                {
                    var item = Instantiate(_logoItem, _logosContainer.transform);
                    item.Init(logo);
                    if (Controller.CustomLogos.Count == 0)
                    {
                        var tutorialTag = item.gameObject.AddComponent<TutorialTag>();
                        tutorialTag.Type = TutorialTargetTypes.Button;
                        tutorialTag.Tag = "RoomLogo";
                    }
                    Controller.CustomLogos.Add(item);
                }

                _isInitialized = true;
            }

            base.Show();
            DontDestroyOnLoad(this);
            Controller.SetState(Controller.FirstRun
                ? RoomCustomizationWindowController.WindowState.LogoEditState
                : RoomCustomizationWindowController.WindowState.MainState);

            _viewAnimator.SetTrigger("Open");
        }
        
        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            _canvas.worldCamera = Camera.main;
        }
        
        public override void Close()
        {
            _viewAnimator.SetTrigger("Close");
        }
        
        public void WindowOpened()
        {
            if (!_isOpen)
            {
                if (ApplicationManager.Instance.TutorialSystem != null && ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is RoomCustomizationStep)
                    ApplicationManager.Instance.TutorialSystem.TutorialWindowController.ShowTarget();
            }

            _isOpen = true;
        }
        
        public void WindowClosed()
        {
            if (_isOpen)
            {
                if (ApplicationManager.Instance.TutorialSystem != null && ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is RoomCustomizationStep)
                    ApplicationManager.Instance.TutorialSystem.CompleteStep();
            }

            _isOpen = false;
        }

        private void CloseClick()
        {
            EventManager.ChangeRoomCustomization.Invoke();
            Close();
        }

        private void EditLogoClick()
        {
            Controller.SetState(RoomCustomizationWindowController.WindowState.LogoEditState);
        }
        
        private void EditNameClick()
        {
            Controller.SetState(RoomCustomizationWindowController.WindowState.NameEditState);
        }

        public void SetTitle(string title)
        {
            _title.text = title;
        }

        private void OnDestroy()
        {
            _exitBtn.onClick.RemoveListener(Close);
            _editLogoBtn.onClick.RemoveListener(EditLogoClick);
            _editNameBtn.onClick.RemoveListener(EditNameClick);
            _saveBtn.RemoveClickListener(Controller.SaveClickHandler);
            _nextBtn.RemoveClickListener(Controller.NextClickHandler);
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
}