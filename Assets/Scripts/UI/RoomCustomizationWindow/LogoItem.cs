using System;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.RoomCustomizationWindow
{
    public class LogoItem : MonoBehaviour
    {
        [SerializeField] private Image _logoImage;
        [SerializeField] private GameObject _selectedBackground;
        [SerializeField] private GameObject _selectedCheck;
        [SerializeField] private GameObject _buyState;
        [SerializeField] private Button _buyBtn;
        [SerializeField] private GameObject _lockBuyBtn;
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private TextMeshProUGUI _priceLockText;
        [SerializeField] private GameObject _lockState;
        [SerializeField] private TextMeshProUGUI _lockLevelText;

        private Button _button;
        private float _logoInactiveTransparent;
        private Color _logoColor;
        private CustomRoomLogo _customLogo;
        private bool _locked;
        private bool _purchased;

        public bool Selected { get; private set; }
        public int Id { get; private set; }

        public static Action<int> OnSelect;
        
        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(Select);
            OnSelect += Unselect;
            
            _buyBtn.onClick.AddListener(Buy);

            _logoColor = _logoImage.color;
            _logoInactiveTransparent = _logoColor.a;
        }

        public void Init(CustomRoomLogo customLogo)
        {
            Id = customLogo.Id;
            _customLogo = customLogo;
            _logoImage.sprite = _customLogo.Image;
            
            _buyState.SetActive(false);
            _lockState.SetActive(false);
            _purchased = ApplicationManager.Instance.CollectionManagerRef.Base.PurchasedCustomLogos.Contains(Id);
            
            if(_customLogo.Price > 0 && !_purchased)
                SetPrice();
            
            if(_customLogo.Level > 0)
                SetLevel();
        }
        
        public void Select()
        {
            if(_customLogo.Level > 0 && ApplicationManager.Instance.CollectionManagerRef.Base.curLevel < _customLogo.Level)
                return;
            
            if(_customLogo.Price > 0 && !_purchased)
                return;

            Selected = true;
            
            _logoColor.a = 1;
            _logoImage.color = _logoColor;
            
            _selectedBackground.SetActive(true);
            _selectedCheck.SetActive(true);
            OnSelect.Invoke(Id);
        }
        
        private void Unselect(int id)
        {
            if(id == Id)
                return;

            _logoColor.a = _logoInactiveTransparent;
            _logoImage.color = _logoColor;
            
            Selected = false;
            _selectedBackground.SetActive(false);
            _selectedCheck.SetActive(false);
        }

        private void SetPrice()
        {
            _priceText.text = $"{_customLogo.Price}";
            _priceLockText.text = $"{_customLogo.Price}";
            _buyState.SetActive(true);
            CheckPrice();
            EventManager.ChangeSkillPoints.AddListener(CheckPrice);
        }

        private void CheckPrice()
        {
            if(_customLogo.Price <= 0)
                return;

            var active = ApplicationManager.Instance.CollectionManagerRef.Base.curSkillPoints >= _customLogo.Price;
            _buyBtn.gameObject.SetActive(active);
            _lockBuyBtn.SetActive(!active);
        }
        
        private void SetLevel()
        {
            _lockLevelText.text = $"UNLOCK {_customLogo.Level}LVL";
            CheckLevel();
            EventManager.ChangeLevel.AddListener(CheckLevel);
        }

        private void CheckLevel()
        {
            var locked = ApplicationManager.Instance.CollectionManagerRef.Base.curLevel < _customLogo.Level;
            _lockState.SetActive(locked);
            if(!locked)
                EventManager.ChangeLevel.RemoveListener(CheckLevel);
        }

        private void Buy()
        {
            EventManager.VibrateLight.Invoke();
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(_customLogo.Price * -1);
            _buyState.SetActive(false);
            ApplicationManager.Instance.CollectionManagerRef.Base.PurchasedCustomLogos.Add(Id);
            _purchased = true;
            Select();
            FacebookEventSubscribers.LogRoomCustomizationBuyLogoEvent(Id);
        }

        private void OnDestroy()
        {
            EventManager.ChangeSkillPoints.RemoveListener(CheckPrice);
            EventManager.ChangeLevel.RemoveListener(CheckLevel);
            _button.onClick.RemoveListener(Select);
        }
    }
}