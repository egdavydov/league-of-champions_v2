using System;
using System.Collections.Generic;
using Core.UI.Base;
using Game.Managers;
using Services;
using Tutorial.Steps;

namespace UI.RoomCustomizationWindow
{
    public class RoomCustomizationWindowController : ViewController<RoomCustomizationWindow>
    {
        public enum WindowState
        {
            MainState,
            LogoEditState,
            NameEditState
        }

        public List<LogoItem> CustomLogos = new List<LogoItem>();
        
        private bool _firstRun;
        private WindowState _state;
        private int? _selectedLogo;
        private string _inputName;

        public bool FirstRun => _firstRun;

        public void Init()
        {
            _selectedLogo = ApplicationManager.Instance.CurrentRoom.CustomLogo;
            _inputName = ApplicationManager.Instance.CurrentRoom.CustomTeamName;
            _firstRun = _selectedLogo == null;
            
            View.SaveBtn.SetText("SAVE");
            View.NextBtn.SetText(_firstRun ? "NEXT" : "OK");
            
            View.NameMaxLengthText.SetText($"MAX LENGTH - {View.Params.NameMaxLength} CHARACTERS");
            View.NameInput.characterLimit = View.Params.NameMaxLength;

            if (_firstRun)
            {
                LogoItem.OnSelect += LogoChangedHandler;
            }
        }

        public void SetState(WindowState state)
        {
            EventManager.VibrateLight.Invoke();
            _state = state;
            View.MainState.SetActive(_state == WindowState.MainState);
            View.LogoEditState.SetActive(_state == WindowState.LogoEditState);
            View.NameEditState.SetActive(_state == WindowState.NameEditState);

            View.SaveBtn.gameObject.SetActive(_state == WindowState.MainState);
            View.NextBtn.gameObject.SetActive(_state != WindowState.MainState);
            View.NextBtn.SetActiveState(true);
            
            switch (_state)
            {
                case WindowState.MainState:
                {
                    var roomName = View.Params.RoomDb.Name;
                    View.SetTitle($"YOU {roomName} TEAM");
                    if (_selectedLogo != null)
                        View.LogoImage.sprite = View.Params.CustomLogos[(int) _selectedLogo - 1].Image;
                    if (_inputName != null)
                        View.NameText.text = _inputName;
                    
                    break;
                }

                case WindowState.LogoEditState:
                {
                    View.SetTitle("CHOOSE TEAM LOGO");
                    if (_selectedLogo != null)
                        CustomLogos.Find(logo => logo.Id == _selectedLogo).Select();
                    if(_firstRun)
                        View.NextBtn.SetActiveState(_selectedLogo != null);
                    
                    break;
                }

                case WindowState.NameEditState:
                {
                    View.SetTitle("CHOOSE TEAM NAME");
                    if (_inputName != null)
                        View.NameInput.text = _inputName;
                    if (_firstRun)
                    {
                        View.NextBtn.SetActiveState(!string.IsNullOrEmpty(_inputName));
                        if (ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is RoomCustomizationStep)
                            ApplicationManager.Instance.TutorialSystem.TutorialWindowController.ShowTarget();
                    }

                    break;
                }
            }
        }

        public void SaveClickHandler()
        {
            EventManager.VibrateMedium.Invoke();

            ApplicationManager.Instance.CurrentRoom.CustomLogo = _selectedLogo;
            ApplicationManager.Instance.CurrentRoom.CustomTeamName = _inputName;
            EventManager.ChangeRoomCustomization.Invoke();
            FacebookEventSubscribers.LogRoomCustomizationSaveEvent();
            View.Close();
        }
        
        public void NextClickHandler()
        {
            switch (_state)
            {
                case WindowState.LogoEditState:
                {
                    var selected = CustomLogos.Find(logo => logo.Selected);
                    _selectedLogo = selected.Id;
                    SetState(_firstRun ? WindowState.NameEditState : WindowState.MainState);

                    break;
                }

                case WindowState.NameEditState:
                {
                    _inputName = View.NameInput.text;
                    SetState(WindowState.MainState);
                    break;
                }
            }
        }

        public void InputChangedHandler(string text)
        {
            View.NextBtn.SetActiveState(!string.IsNullOrWhiteSpace(text));
        }

        private void LogoChangedHandler(int id)
        {
            View.NextBtn.SetActiveState(true);
        }
    }
}