using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.RoomCustomizationWindow
{
    public class StateButton : MonoBehaviour
    {
        [SerializeField] private Button _active;
        [SerializeField] private GameObject _unactive;
        [SerializeField] private GameObject _shadow;
        [SerializeField] private TextMeshProUGUI _activeText;
        [SerializeField] private TextMeshProUGUI _unactiveText;

        private Button _button;
        
        public void SetActiveState(bool active)
        {
            _unactive.SetActive(!active);
            _active.gameObject.SetActive(active);
            _shadow.SetActive(active);
        }
        
        public void SetText(string text)
        {
            _activeText.text = text;
            _unactiveText.text = text;
        }

        public void AddClickListener(UnityAction action)
        {
            _active.onClick.AddListener(action);
        }
        
        public void RemoveClickListener(UnityAction action)
        {
            _active.onClick.RemoveListener(action);
        }
    }
}