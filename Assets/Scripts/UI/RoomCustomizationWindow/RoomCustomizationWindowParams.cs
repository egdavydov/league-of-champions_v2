using Core.UI.Base;
using UnityEngine;

namespace UI.RoomCustomizationWindow
{
    public class RoomCustomizationWindowParams : ViewParams
    {
        public readonly RoomsDB RoomDb;
        public readonly CustomRoomLogo[] CustomLogos;
        public readonly int NameMaxLength;

        public RoomCustomizationWindowParams(RoomsDB roomDb, CustomRoomLogo[] logos, int nameMaxLength)
        {
            RoomDb = roomDb;
            CustomLogos = logos;
            NameMaxLength = nameMaxLength;
        }
    }
}