﻿using System;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;

namespace UI.Widgets
{
    public class CoinsWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _moneyMaxTxt;
        
        private void Awake()
        {
            EventManager.ChangeCoins.AddListener(Print);
        }

        private void Start()
        {
            Print();
        }

        private void OnDestroy()
        {
            EventManager.ChangeCoins.RemoveListener(Print);
        }

        private void Print()
        {
            var value = (float) Math.Floor(ApplicationManager.Instance.CollectionManagerRef.Base.curCoins);
            _moneyMaxTxt.text = value > 9999 ? Reduction.ReductionNumber(value, 0, true) : value.ToString();
        }
    }
}