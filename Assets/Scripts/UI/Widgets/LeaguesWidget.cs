﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Game.Managers;
using Services;
using TMPro;

enum Name
{
    Bronze = 1,
    Silver,
    Gold,
    Platinum,
    Diamond
}


public class LeaguesWidget : MonoBehaviour
{
    [SerializeField] private Animator[] _anims;
    [SerializeField] private Material m_material;
    [SerializeField] private Color[] m_color;
    [SerializeField] private TextMeshProUGUI m_title;
    [SerializeField] private TextMeshProUGUI m_coins;

    private int m_levelLeague;
    private int m_levelInLeague;
    private Animator _generalAnim;

    private WaitForSeconds _delay;

    private RoomsDB _roomDB;

    private string[] m_names = new[]
    {
        "Bronze",
        "Silver",
        "Gold",
        "Platinum",
        "Diamond"
    };

    private void Start()
    {
        _delay = new WaitForSeconds(0.1f);
        _generalAnim = GetComponent<Animator>();
 
        //EventManager.AddLeagueUpgrade.AddListener(Refresh);
        EventManager.ChangeRoom.AddListener((i => Open(i)));
        //Generate();
    }
    
    private bool IsAnimationPlaying(string stateName, int layerIndex = 0)
    {        
        var animatorStateInfo = _generalAnim.GetCurrentAnimatorStateInfo(layerIndex);
        if (animatorStateInfo.IsName(stateName))             
            return true;

        return false;
    }
   
    private IEnumerator ChangeState()
    {
        if (_generalAnim.GetBool("Open"))
        {
            _generalAnim.SetBool("Open", false);
        }

        while (IsAnimationPlaying("Open"))
        {
            yield return _delay;
        }
        
        Generate();
        
        _generalAnim.SetBool("Open", true);
    }

    public void Open(int number)
    {
        _roomDB = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(number);
        StartCoroutine(ChangeState());
    }

    private void Generate()
    {
        m_levelLeague = Mathf.CeilToInt(_roomDB.curLevelLeague / 5);
        m_material.color = m_color[m_levelLeague - 1];
        m_title.color = m_color[m_levelLeague - 1];

        m_levelInLeague = ((int)_roomDB.curLevelLeague - (m_levelLeague * 5)) + 5;
        
        for (int i = 0; i < _anims.Length; i++)
        {
            _anims[i].SetBool("BoolOpen", i < m_levelInLeague);
        }

        _roomDB.SetCurBaseBetLeague();
        
        if (!_roomDB.Activate)
        {
            m_title.text = "---";
            m_coins.text = "---";
            return;
        }
        
        m_title.text = m_names[m_levelLeague - 1];
        m_coins.text = Reduction.ReductionNumber(_roomDB.curBaseBetLeague, 0, true);
    }

    public void Refresh()
    {
        m_levelLeague = Mathf.CeilToInt(_roomDB.curLevelLeague / 5);
        m_levelInLeague = ((int)_roomDB.curLevelLeague - (m_levelLeague * 5)) + 5;
        m_coins.text = Reduction.ReductionNumber(Math.Round(_roomDB.curBaseBetLeague), 0, true);

        if (m_levelInLeague == 1)
        {
            _generalAnim.SetTrigger("Play");
            
            for (int i = _anims.Length - 1; i >= 0; i--)
            {
                _anims[i].SetBool("BoolOpen", false);
            }
            
            _generalAnim.SetTrigger("NameChange");
        
            m_material.color = m_color[m_levelLeague - 1];
            _anims[0].SetBool("BoolOpen", true);
            return;
        }

        _anims[m_levelInLeague - 1].SetBool("BoolOpen", true);
    }

    public void ChangeLeagueName()
    {
        m_title.text = m_names[m_levelLeague - 1];
        m_title.color = m_color[m_levelLeague - 1];
    }

    private void OnDestroy()
    {
        EventManager.ChangeRoom.AddListener((i => Open(i)));
        //EventManager.AddLeagueUpgrade.RemoveListener(Generate);
    }
}