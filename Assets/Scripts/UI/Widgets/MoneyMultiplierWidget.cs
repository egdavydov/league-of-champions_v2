﻿using System;
using System.Collections;
using System.Linq;
using Advertising;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;

namespace UI.Widgets
{
    public class MoneyMultiplierWidget : MonoBehaviour
    {
        [SerializeField] private int[] _times;
        [SerializeField] private TextMeshProUGUI _timerText;
        [SerializeField] private MoneyMultiplierWindow _window;
        [SerializeField] private GameObject _timer;
        [SerializeField] private GameObject _moneyText;

        private int _multiplierTime;
        private int _multiplierTimeSum;
        private int _multiplierTimeMax;
        private int _minutes;
        private int _seconds;
        private int _multiplierLevel;
        private bool _advAllowed;

        private const int showAdvTimeout = 120;
        
        private void Start()
        {
            _multiplierLevel = ApplicationManager.Instance.CollectionManagerRef.Base.LastMoneyMultiplier > 0 ? Array.IndexOf(_times, ApplicationManager.Instance.CollectionManagerRef.Base.LastMoneyMultiplier) : -1;
            _multiplierTimeMax = _times.Sum() * 60;
            UpdateMoneyMultiplierTime();

            if (ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier > 0)
            {
                StartMoneyMultiplier();
                Refresh();
                return;
            }
            
            Reset();
        }

        private void UpdateMoneyMultiplierTime()
        {
            if (string.IsNullOrEmpty(ApplicationManager.Instance.CollectionManagerRef.Base.TimeStop) ||
                ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier <= 0) return;
            
            var timePlay = DateTime.Now;
            var timeStop = DateTime.Parse(ApplicationManager.Instance.CollectionManagerRef.Base.TimeStop);
            var secondPassed = (int) (timePlay - timeStop).TotalSeconds;

            if (secondPassed > ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier)
                ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier = 0;
            else
                ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier -= secondPassed;

        }

        private void Refresh()
        {
            _moneyText.SetActive(!ApplicationManager.Instance.CollectionManagerRef.Base.isMultiplier);
            _timer.SetActive(ApplicationManager.Instance.CollectionManagerRef.Base.isMultiplier);
            _multiplierTimeSum = _times.Take(_multiplierLevel+1).Sum();

            if (_multiplierLevel >= _times.Length - 1)
            {
                _advAllowed = false;
                _window.SetComplete(true);
                _multiplierTime = _times[_multiplierLevel];
                return;
            }

            _multiplierTime = _times[_multiplierLevel+1];
            _window.SetProgress(_multiplierLevel+1, _multiplierTime);
            _window.SetComplete(false);
            _advAllowed = true;
        }

        public void AddMultiplierTime()
        {
            if (_multiplierLevel < _times.Length - 1)
                _multiplierLevel++;
            
            FacebookEventSubscribers.LogMoneyX2BoostEvent(_multiplierTime);
            ApplicationManager.Instance.CollectionManagerRef.Base.LastMoneyMultiplier = _multiplierTime;
            ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier += _multiplierTime*60;
            if (ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier > _multiplierTimeMax)
                ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier = _multiplierTimeMax;

            if (!ApplicationManager.Instance.CollectionManagerRef.Base.isMultiplier)
                StartMoneyMultiplier();

            Refresh();
        }

        private void StartMoneyMultiplier()
        {
            ApplicationManager.Instance.CollectionManagerRef.Base.isMultiplier = true;
            EventManager.ChangeMultiplier.Invoke();
        }

        private void StopMoneyMultiplier()
        {
            EventManager.ChangeMultiplier.Invoke();
            Reset();
        }

        private void Reset()
        {
            ApplicationManager.Instance.CollectionManagerRef.Base.isMultiplier = false;
            ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier = 0;
            ApplicationManager.Instance.CollectionManagerRef.Base.LastMoneyMultiplier = 0;
            _multiplierLevel = -1;
            Refresh();
        }

        private void Update()
        {
            if (!ApplicationManager.Instance.CollectionManagerRef.Base.isMultiplier)
                return;

            ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier -= Time.deltaTime;
            if (ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier <= 0)
            {
                StopMoneyMultiplier();
                return;
            }
            
            _minutes = (int) Math.Truncate(ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier / 60);
            _seconds = (int) ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier % 60;
            _timerText.text = $"{_minutes,3}:{_seconds:D2}";
            _window.UpdateTimer(_timerText.text);

            if ((_multiplierTimeMax - ApplicationManager.Instance.CollectionManagerRef.Base.SecondMoneyMultiplier) > showAdvTimeout && !_advAllowed)
            {
                _advAllowed = true;
                _window.SetButtons(true);
            }
            
            var delta = _multiplierTimeSum - _minutes;
            if (delta <= _times[_multiplierLevel]) return;
            
            _multiplierLevel--;
            Refresh();
        }

        public void OpenWindow()
        {
            _window.OpenWindow();
        }
        
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        private void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus) return;

            UpdateMoneyMultiplierTime();
        }
#endif
    }
}