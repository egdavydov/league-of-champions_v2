using System;
using System.Linq;
using DB;
using DG.Tweening;
using Game.Managers;
using UI.UltraWindow;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Widgets
{
    public class UltraWidget : MonoBehaviour
    {
        [SerializeField] private Button _button;
        private RectTransform _transform;
        private bool _active;
        private BaseDB _baseDb;
        private UltraBonusDB _ultraBonusDb;
        
        private const float hiddenPosition = -250f;
        private const float showedPosition = -152f;
        
        private void Awake()
        {
            _baseDb = ApplicationManager.Instance.CollectionManagerRef.Base;
            _ultraBonusDb = ApplicationManager.Instance.CollectionManagerRef.UltraBonus;
            _transform = GetComponent<RectTransform>();
            _button.onClick.AddListener(ShowUltraWindow);
            EventManager.ChangeLevel.AddListener(Refresh);
            EventManager.UltraBonusFulled.AddListener(Hide);
            Refresh();
        }

        private void Update()
        {
            if (Time.frameCount % 30 != 0)
                return;
            
            if (_active)
                return;

            if (string.IsNullOrEmpty(_baseDb.LastUltraBonusTime))
            {
                if (ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].GetTrainer.Activate)
                    Show();
                return;
            }

            if (!_ultraBonusDb.UltraBonusFulled())
                return;

            var lastUltraBonusTime = DateTime.Parse(_baseDb.LastUltraBonusTime);
            var diff = DateTime.Now - lastUltraBonusTime;
            if (diff.TotalSeconds > _ultraBonusDb.FullResetHours * 3600)
                Show();
        }

        private void Refresh()
        {
            if (ApplicationManager.Instance.CollectionManagerRef.Base.curLevel < 9 
                || _ultraBonusDb.UltraBonusFulled() 
                || !ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB[0].GetTrainer.Activate)
            {
                _transform.anchoredPosition =  new Vector3(hiddenPosition, _transform.anchoredPosition.y, 0f);
                return;
            }
            
            Show();
            EventManager.ChangeLevel.RemoveListener(Refresh);
        }

        private void Show()
        {
            var posX = hiddenPosition;
            var endX = showedPosition;
            DOTween.To(() => posX, x => posX = x, endX, 1f).OnUpdate(() =>
            {
                _transform.anchoredPosition =  new Vector3(posX, _transform.anchoredPosition.y, 0f);
            }).SetEase(Ease.OutExpo).Play();
            _active = true;
        }

        private void Hide()
        {
            var posX = showedPosition;
            var endX = hiddenPosition;
            DOTween.To(() => posX, x => posX = x, endX, 1f).OnUpdate(() =>
            {
                _transform.anchoredPosition =  new Vector3(posX, _transform.anchoredPosition.y, 0f);
            }).SetEase(Ease.OutExpo).Play();
            _active = false;
        }

        private void ShowUltraWindow()
        {
            ApplicationManager.Instance.UISystem.Show(ViewType.UltraBonusView, 
                new UltraWindowParams(
                    ApplicationManager.Instance.CollectionManagerRef.UltraBonus,
                    ApplicationManager.Instance.CollectionManagerRef.Base,
                    ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB
                    )
            );
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(ShowUltraWindow);
        }
    }
}