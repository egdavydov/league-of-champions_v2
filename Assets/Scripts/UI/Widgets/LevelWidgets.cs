﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Game.Managers;
using Services;
using TMPro;
using Tutorial.Steps;
using UI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LevelWidgets : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _levelTxt;
    [SerializeField] private Slider _slider;
    [SerializeField] private GameObject _LevelUpBtn;
    [SerializeField] private GameObject _levelLabel;
    [SerializeField] private LevelUpWindow _levelUpWindow;

    private bool _showLevelUp;

    private void Start()
    {
        Refresh();
        RefreshState();
        EventManager.ChangeExperience.AddListener(UpdateSlider);
        EventManager.ChangeLevel.AddListener(SetNewLevel);
    }

    private void UpdateSlider()
    {
        if (_showLevelUp)
            return;
        
        _slider.value = Mathf.Clamp(ApplicationManager.Instance.CollectionManagerRef.Base.curExperience / ApplicationManager.Instance.CollectionManagerRef.Base.PriceNextLevel, 0, 1);
        if (_slider.value < 1f) return;

        _showLevelUp = true;
        EventManager.VibrateMedium.Invoke();
        RefreshState();
    }

    private void OnDestroy()
    {
        EventManager.ChangeExperience.RemoveListener(UpdateSlider);
        EventManager.ChangeLevel.RemoveListener(SetNewLevel);
    }

    public void OpenWindow()
    {
        _levelUpWindow.OpenWindow();
    }

    private void SetNewLevel()
    {
        _showLevelUp = false;
        ApplicationManager.Instance.CollectionManagerRef.Base.curExperience = 0;
        Refresh();
        RefreshState();
        
        if (ApplicationManager.Instance.TutorialSystem == null)
            return;

        if (ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is UpgradeStep)
            ApplicationManager.Instance.TutorialSystem.CompleteStep();
    }

    private void Refresh()
    {
        _levelTxt.text = $"LEVEL {ApplicationManager.Instance.CollectionManagerRef.Base.curLevel}";
        UpdateSlider();
    }

    private void RefreshState()
    {
        _LevelUpBtn.SetActive(_showLevelUp);
        _levelLabel.SetActive(!_showLevelUp);
    }
}