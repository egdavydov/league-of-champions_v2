﻿using System;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;

namespace UI.Widgets
{
    public class SkillPointsWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _skillPointsMaxTxt;
        
        private void Awake()
        {
            EventManager.ChangeSkillPoints.AddListener(Print);
        }

        private void Start()
        {
            Print();
        }

        private void OnDestroy()
        {
            EventManager.ChangeSkillPoints.RemoveListener(Print);
        }

        private void Print()
        {
            var value = (float) Math.Floor(ApplicationManager.Instance.CollectionManagerRef.Base.curSkillPoints);
            _skillPointsMaxTxt.text = value > 9999 ? Reduction.ReductionNumber(value, 0, true) : value.ToString();
        }
    }
}