﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Managers;
using Services;
using UnityEngine;
using TMPro;
using Random = UnityEngine.Random;

public class ViewersWidget : MonoBehaviour
{
    private TextMeshProUGUI _text;
    private int _count = 0;
    private int _max;
    private int _min;
    private int _limit = 0;
    private bool _isLimit = false;
    private int[] _multiple = new int[] {1, -1};
    private int _viewersOnLeague;
    private int _minViewersInOneTenthSecond;
    private int _maxViewersInOneTenthSecond;

    private RoomsDB _roomDB;

    private void Start()
    {
        _viewersOnLeague = ApplicationManager.Instance.CollectionManagerRef.Base.ViewersOnLeague;
        _minViewersInOneTenthSecond = ApplicationManager.Instance.CollectionManagerRef.Base.MinViewersInSecond / 10;
        _maxViewersInOneTenthSecond = ApplicationManager.Instance.CollectionManagerRef.Base.MaxViewersInSecond / 10;
        _text = GetComponentInChildren<TextMeshProUGUI>();
        
        StartCoroutine(Release());
        
        EventManager.ChangeRoom.AddListener((i => ChangeRoom(i)));
    }

    private IEnumerator Release()
    {
        var delay = new WaitForSeconds(0.1f);
        while (true)
        {
            yield return delay;

            if (_roomDB == null || !_roomDB.Activate)
            {
                _text.text = "0";
                continue;
            }

            _isLimit = _count >= _limit;

            _count = Mathf.Clamp
            (
                value: _count + (Random.Range(_minViewersInOneTenthSecond, _maxViewersInOneTenthSecond) * _multiple[_isLimit ? Random.Range(0, 2) : 0]),
                min: _isLimit ? _min : 0,
                max: _max
            );

            _text.text = Reduction.ReductionNumber((float) _count, 0, true);
        }
    }

    private void ChangeRoom(int number)
    {
        _roomDB = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(number);
        
        if(!_roomDB.Activate)
            return;
        
        SetLimit();
    }

    public void SetLimit()
    {
        if (_roomDB.curLevelLeague < 25)
        {
            _limit = (int)(_roomDB.curLevelLeague * _viewersOnLeague);
            _min = _limit - (_limit / 10);
            _max = _limit + (_limit / 10);
        }
        else
        {
            _limit = Int32.MaxValue;
            _max = Int32.MaxValue;
        }

        _isLimit = false;
    }

    private void OnDestroy()
    {
        EventManager.ChangeRoom.RemoveListener((i => ChangeRoom(i)));
    }
}