﻿using System.Collections;
using System.Collections.Generic;
using Game.Managers;
using Services;
using UnityEngine;

public class ConcentrationWidget : ABoostWidget
{
    private RoomsDB _roomDB;
    
    private void RoomTracking(int numberRoom)
    {
        _roomDB = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(numberRoom);
        _roomDB.EventAddSpeed.AddListener(PutOrRemove);
        PutOrRemove();
    }
    
    protected override void Activate()
    {
        int roomSelect = 0;
        for (int i = 1; i < ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB.Length; i++)
        {
            if (ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(i).Activate)
            {
                roomSelect = i;
            }
            else
            {
                break;
            }
        }
        RoomTracking(roomSelect);

        EventManager.BuyRoom.AddListener(RoomTracking);
    }

    public override BoostsInfoWindowController.BoostType BoostType()
    {
        return BoostsInfoWindowController.BoostType.Concentration;
    }

    private void PutOrRemove()
    {
        if (_roomDB.curLevelSpeed >= 2.5f)
        {
            Boosts.AddBoost(this);
            _roomDB.EventAddSpeed.RemoveListener(PutOrRemove);
        }
        else
        {
            Boosts.RemoveBoost(this);
        }
    }

    public override void InvokeEventStart()
    {
        EventManager.BoostConcentration.Invoke();
        EventManager.BoostConcentrationStart.Invoke();
    }

    public override void InvokeEventEnd()
    {
        EventManager.BoostConcentration.Invoke();
    }
}
