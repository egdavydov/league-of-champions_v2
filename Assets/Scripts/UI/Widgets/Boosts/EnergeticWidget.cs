﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class EnergeticWidget : ABoostWidget
{
    protected override void Activate()
    {
        Boosts.AddBoost(this);
    }

    public override BoostsInfoWindowController.BoostType BoostType()
    {
        return BoostsInfoWindowController.BoostType.Energetic;
    }

    public override void InvokeEventStart()
    {
        EventManager.BoostEnergetic.Invoke();
        EventManager.BoostEnergeticStart.Invoke();
    }

    public override void InvokeEventEnd()
    {
        EventManager.BoostEnergetic.Invoke();
    }
}
