﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Managers;
using UnityEngine;
using Random = UnityEngine.Random;

public class Boosts : MonoBehaviour
{
    [SerializeField] private ABoostWidget[] m_boosts;
    private float _minTime = 60f;
    private float _maxTime = 80f;
    private List<ABoostWidget> _boostsAvaliable = new List<ABoostWidget>();
    
    public void AddBoost(ABoostWidget boost)
    {
        Debug.Log("Add " + boost.gameObject.name);
        _boostsAvaliable.Add(boost);
    }

    public void RemoveBoost(ABoostWidget boost)
    {
        Debug.Log("Remove " + boost.gameObject.name);
        _boostsAvaliable.Remove(boost);
    }

    private void Awake()
    {
        _minTime = ApplicationManager.Instance.CollectionManagerRef.Base.BoostsShowMinTime;
        _maxTime = ApplicationManager.Instance.CollectionManagerRef.Base.BoostsShowMaxTime;
    }

    private void Start()
    {
        StartIssuing();
    }

    public void StartIssuing()
    {
        StartCoroutine(Release());
    }

    private IEnumerator Release()
    {
        var time = Random.Range(_minTime, _maxTime);
        
        Debug.Log("Next Boost time " + time + " boost count " + _boostsAvaliable.Count);
        yield return new WaitForSeconds(time);
        var index = Random.Range(0, _boostsAvaliable.Count-1);
        _boostsAvaliable[index].Open();
    }
}