﻿using System.Collections;
using System.Collections.Generic;
using Advertising;
using Game.Managers;
using UnityEngine;
using UnityEngine.UI;

public abstract class ABoostWidget : MonoBehaviour
{
    public float m_timeBtn = 15f;
    public float m_timeEffect = 15f;
    public Image m_scrolling;
    public AFX m_fx;

    private float m_curTime;
    private Coroutine _corReleaseBtn;
    private Coroutine _corEffect;

    protected Boosts Boosts;
    private AdvertisingAnimBtn _advertisingAnimBtn;

    protected abstract void Activate();
    
    private void Start()
    {
        _advertisingAnimBtn = GetComponent<AdvertisingAnimBtn>();
        Boosts = GetComponentInParent<Boosts>();
        Activate();
        
        _advertisingAnimBtn.Show.AddListener(StartAnimationBtn);
        _advertisingAnimBtn.Hide.AddListener(StopAnimationBtn);
        _advertisingAnimBtn.Reward.AddListener(StartEffect);
    }

    public abstract BoostsInfoWindowController.BoostType BoostType();
    
    public void Open()
    {
        _advertisingAnimBtn.ShowBtn();
    }

    private void StartAnimationBtn()
    {
        _corReleaseBtn = StartCoroutine(AnimationBtn());
    }
    
    private void StopAnimationBtn()
    {
        if (_corReleaseBtn != null)
            StopCoroutine(_corReleaseBtn);
    }
    
    
    private IEnumerator AnimationBtn()
    {
        m_curTime = 0;
        m_scrolling.fillAmount = 0f;
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            m_curTime += 0.1f;
            m_scrolling.fillAmount = Mathf.Clamp(m_curTime / m_timeBtn, 0, 1);

            if (m_curTime >= m_timeBtn)
            {
                _advertisingAnimBtn.HideBtn();
                Boosts.StartIssuing();
                yield break;
            }
        }
    }

    private void StartEffect()
    {
        MyDebug.Log("StartEffect");
        EventManager.VibrateLight.Invoke();
        
//        StopCoroutine(_corReleaseBtn);
        StartCoroutine(Effect());
    }

    private IEnumerator Effect()
    {
        MyDebug.Log("StartEffect Cor");
        InvokeEventStart();
        if (m_fx != null)
        {
            m_fx.PlayFX();
        }

        yield return new WaitForSeconds(m_timeEffect);

        if (m_fx != null)
        {
            m_fx.StopFX();
            ApplicationManager.Instance.AudioManager.StopSound();
            ApplicationManager.Instance.AudioManager.UnPauseAmbient(AmbientType.MainTheme);
        }

        InvokeEventEnd();
        Boosts.StartIssuing();
    }

    private void OnDestroy()
    {
        _advertisingAnimBtn.Show.RemoveListener(StartAnimationBtn);
        _advertisingAnimBtn.Hide.RemoveListener(StopAnimationBtn);
        _advertisingAnimBtn.Reward.RemoveListener(StartEffect);
    }


    public abstract void InvokeEventStart();
    public abstract void InvokeEventEnd();
}