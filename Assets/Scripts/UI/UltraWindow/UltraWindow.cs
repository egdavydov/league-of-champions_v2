using System;
using System.Linq;
using Advertising;
using Core.UI.Base;
using Game.Managers;
using UnityEngine;
using UnityEngine.UI;
using UI.UltraRewardWindow;
using UnityEngine.SceneManagement;

namespace UI.UltraWindow
{
    public class UltraWindow : View<UltraWindowController, UltraWindowParams>
    {
        [SerializeField] private GameObject _bonusesContainer;
        [SerializeField] private GameObject _advAcivateBtn;
        [SerializeField] private GameObject _advUnactivateBtn;
        [SerializeField] private GameObject _advButtons;
        [SerializeField] private GameObject _completeButton;
        [SerializeField] private Button _exitBtn;
        [SerializeField] private Button _advBtn;
        [SerializeField] private Button _okBtn;
        
        private Animator _viewAnimator;
        private UltraBonus[] _ultraBonuses;
        private UltraBonus _activatedBonus;
        private bool _isInitialized;
        private Canvas _canvas;
        
        private void Awake()
        {
            _canvas = gameObject.GetComponent<Canvas>();
            if (_canvas.worldCamera == null)
                _canvas.worldCamera = Camera.main;
            _ultraBonuses = _bonusesContainer.GetComponentsInChildren<UltraBonus>();
            _viewAnimator = GetComponent<Animator>();
            _exitBtn.onClick.AddListener(Close);
            _okBtn.onClick.AddListener(Close);
            _advBtn.onClick.AddListener(GetReward);
            RefreshAdvButton();
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            _canvas.worldCamera = Camera.main;
        }
        
        private void RefreshAdv(string s)
        {
            RefreshAdvButton();
        }
        
        private void RefreshAdvButton()
        {
            var advActivate = MaxAppLovin.Instance.IsRewardedAdReady;
            _advAcivateBtn.SetActive(advActivate);
            _advUnactivateBtn.SetActive(!advActivate);
        }

        private void GetReward()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
        
            MaxAppLovin.Instance.HiddenRevard.AddListener(RewardAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }
    
        private void RewardAdv()
        {
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(RewardAdv);
            _activatedBonus = _ultraBonuses.LastOrDefault(ultraBonus => !ultraBonus.IsActive);
            if (_activatedBonus == null)
                return;
            
            _activatedBonus.Activate();
            Close();
            ApplicationManager.Instance.UISystem.Show(ViewType.UltraBonusRewardView, 
                new UltraRewardWindowParams(_activatedBonus.UltraBonusDb)
            );
            
            if (Params.UltraBonusDb.UltraBonusFulled())
                EventManager.UltraBonusFulled.Invoke();
        }

        private void Refresh()
        {
            var ultraBonusFulled = Params.UltraBonusDb.UltraBonusFulled();

            _completeButton.SetActive(ultraBonusFulled);
            _advButtons.SetActive(!ultraBonusFulled);
        }

        public override void Show()
        {
            if (!_isInitialized)
            {
                Init();
            }

            Controller.CheckLastDate();
            base.Show();
            DontDestroyOnLoad(this);
            foreach (var bonus in _ultraBonuses)
            {
                bonus.RefreshReward();
            }
            Refresh();
            _viewAnimator.SetTrigger("Open");
        }

        public void Init()
        {
            var i = Params.UltraBonusDb.UltraBonuses.Length - 1;
            foreach (var ultraBonus in _ultraBonuses)
            {
                ultraBonus.SetData(Params.UltraBonusDb.UltraBonuses[i], Params.BaseDb, Params.RoomsDb);
                i--;
            }

            _isInitialized = true;
        }
        
        public override void Close()
        {
            _viewAnimator.SetTrigger("Close");
        }
        
        private void OnDestroy()
        {
            _exitBtn.onClick.RemoveListener(Close);
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
        
        public void WindowOpened()
        {
        }
    
        public void WindowClosed()
        {
        }
    }
}