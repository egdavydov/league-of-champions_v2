using Core.UI.Base;
using DB;

namespace UI.UltraWindow
{
    public class UltraWindowParams : ViewParams
    {
        public readonly UltraBonusDB UltraBonusDb;
        public readonly BaseDB BaseDb;
        public readonly RoomsDB[] RoomsDb;

        public UltraWindowParams(UltraBonusDB ultraBonusDb, BaseDB baseDb, RoomsDB[] roomsDb)
        {
            UltraBonusDb = ultraBonusDb;
            BaseDb = baseDb;
            RoomsDb = roomsDb;
        }
    }
}