using System;
using System.Linq;
using Core.UI.Base;
using Game.Managers;

namespace UI.UltraWindow
{
    public class UltraWindowController : ViewController<UltraWindow>
    {
        public void CheckLastDate()
        {
            if (string.IsNullOrEmpty(View.Params.BaseDb.LastUltraBonusTime))
                return;
                
            var lastUltraBonusTime = DateTime.Parse(View.Params.BaseDb.LastUltraBonusTime);
            var diff = DateTime.Now - lastUltraBonusTime;
            var fulledUltra = View.Params.UltraBonusDb.UltraBonusFulled();

            if ((fulledUltra && diff.TotalSeconds > View.Params.UltraBonusDb.FullResetHours * 3600)
                || (!fulledUltra && diff.TotalSeconds > View.Params.UltraBonusDb.ResetHours * 3600))
            {
                foreach (var ultraBonus in View.Params.UltraBonusDb.UltraBonuses)
                {
                    ultraBonus.active = false;
                }
                View.Init();
            }
        }
    }
}