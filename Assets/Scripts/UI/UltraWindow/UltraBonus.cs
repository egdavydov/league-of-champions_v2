using System;
using DB;
using Game.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.UltraWindow
{
    public class UltraBonus : MonoBehaviour
    {
        [SerializeField] private GameObject _activeBackgound;
        [SerializeField] private TextMeshProUGUI _name;
        [SerializeField] private TextMeshProUGUI _reward;
        [SerializeField] private TextMeshProUGUI _nameActive;
        [SerializeField] private TextMeshProUGUI _rewardActive;
        [SerializeField] private GameObject _coinsIcon;
        [SerializeField] private GameObject _perkPointsIcon;
        [SerializeField] private GameObject _timeJumpIcon;
        [SerializeField] private GameObject _defaultText;
        [SerializeField] private GameObject _activeText;

        public bool IsActive => UltraBonusDb.active;
        public UltraBonusDB.UltraBonus UltraBonusDb { get; private set; }

        public void Activate()
        {
            if(UltraBonusDb.active)
                return;
            
            UltraBonusDb.ActivateBonus();
            Refresh();
        }

        private void Refresh()
        {
            _activeBackgound.SetActive(UltraBonusDb.active);
            _defaultText.SetActive(!UltraBonusDb.active);
            _activeText.SetActive(UltraBonusDb.active);
        }

        public void SetData(UltraBonusDB.UltraBonus bonus, BaseDB baseDb, RoomsDB[] roomsDb)
        {
            UltraBonusDb = bonus;
            
            _name.text = UltraBonusDb.type.ToString();
            _nameActive.text = UltraBonusDb.type.ToString();

            _coinsIcon.SetActive(false);
            _perkPointsIcon.SetActive(false);
            _timeJumpIcon.SetActive(false);
            
            UltraBonusDb.SetData(baseDb, roomsDb);
            RefreshReward();
            
            switch (UltraBonusDb.type)
            {
                case UltraBonusDB.UltraBonusType.Gold:
                    _coinsIcon.SetActive(true);
                    _reward.text = Reduction.ReductionNumber(UltraBonusDb.reward, 0, true);
                    _rewardActive.text = Reduction.ReductionNumber(UltraBonusDb.reward, 0, true);

                    break;
                case UltraBonusDB.UltraBonusType.SkillPoints:
                    _perkPointsIcon.SetActive(true);
                    _reward.text = Reduction.ReductionNumber(UltraBonusDb.reward, 0, true);
                    _rewardActive.text = Reduction.ReductionNumber(UltraBonusDb.reward, 0, true);

                    break;
                case UltraBonusDB.UltraBonusType.TimeJump:
                    _timeJumpIcon.SetActive(true);
                    _reward.text = $"{UltraBonusDb.hour} hours";
                    _rewardActive.text = $"{UltraBonusDb.hour} hours";

                    break;
            }

            Refresh();
        }

        public void RefreshReward()
        {
            if (IsActive)
                return;

            if (UltraBonusDb.type == UltraBonusDB.UltraBonusType.Gold || UltraBonusDb.type == UltraBonusDB.UltraBonusType.TimeJump)
                UltraBonusDb.UpdateReward();
            
            if (UltraBonusDb.type != UltraBonusDB.UltraBonusType.Gold)
                return;
            
            _reward.text = Reduction.ReductionNumber(UltraBonusDb.reward, 0, true);
            _rewardActive.text = Reduction.ReductionNumber(UltraBonusDb.reward, 0, true);
        }
    }
}