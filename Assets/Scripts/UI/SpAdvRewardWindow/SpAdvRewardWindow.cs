using System;
using System.Linq;
using Advertising;
using Core.UI.Base;
using DB;
using Game.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.SpAdvRewardWindow
{
    public class SpAdvRewardWindow : View<SpAdvRewardWindowController, SpAdvRewardWindowParams>
    {
        [SerializeField] private TextMeshProUGUI _rewardCoinsLabel;
        [SerializeField] private Button _okBtn;
        
        private Animator _viewAnimator;
        private bool _isInitialized;
        private Canvas _canvas;
        
        private void Awake()
        {
            _viewAnimator = GetComponent<Animator>();
            _okBtn.onClick.AddListener(GetReward);
        }

        private void Refresh()
        {
        }

        public override void Show()
        {
            _rewardCoinsLabel.text = Reduction.ReductionNumber(Params.Reward, 0, true);
            base.Show();
            DontDestroyOnLoad(this);
            _viewAnimator.SetTrigger("Open");
        }

        private void GetReward()
        {
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(Params.Reward);
            ApplicationManager.Instance.SaveSystemRef.Save();
            Close();
        }
        
        public override void Close()
        {
            _viewAnimator.SetTrigger("Close");
        }
        
        private void OnDestroy()
        {
            _okBtn.onClick.RemoveListener(Close);
        }
        
        public void WindowOpened()
        {
        }
    
        public void WindowClosed()
        {
        }
    }
}