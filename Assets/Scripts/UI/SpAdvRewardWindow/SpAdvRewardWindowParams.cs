using Core.UI.Base;
using DB;

namespace UI.SpAdvRewardWindow
{
    public class SpAdvRewardWindowParams : ViewParams
    {
        public float Reward;
        public SpAdvRewardWindowParams(float reward)
        {
            Reward = reward;
        }
    }
}