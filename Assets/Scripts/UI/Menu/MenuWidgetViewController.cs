﻿using System;
using Advertising;
using Core.UI.Base;
using Game.Managers;
using Services;
using UI.RoomCustomizationWindow;
using UI.SpAdvRewardWindow;
using UnityEngine;

namespace UI.Menu
{
   public class MenuWidgetViewController : ViewController<MenuWidget>
   {
      private const string SpAdvDateKey= "ADV_REV_PER";

      private float _timer;
      private bool _roomCustomizationShowed;
   
      public int AdvertisementReward => ApplicationManager.Instance.CollectionManagerRef.Base.AdvertisingReward;
   
      public void GetRewardButtonClickedHandler()
      {
         EventManager.VibrateLight.Invoke();
         var after = DateTime.Now.AddSeconds(ApplicationManager.Instance.CollectionManagerRef.Base.TimePeriod);
         PlayerPrefs.SetString(SpAdvDateKey, after.Ticks.ToString());
         MaxAppLovin.Instance.HiddenRevard.AddListener(OnAdvertisementViewed);
         MaxAppLovin.Instance.ShowRewardedInterstitial();
         CanShowAdvertisementReward();
         FacebookEventSubscribers.LogSpAdvEvent();
      }
   
      public bool CanShowAdvertisementReward()
      {
         if (ApplicationManager.Instance.CollectionManagerRef.Base.curLevel < 8)
         {
            return false;
         }
         
         if (!MaxAppLovin.Instance.IsRewardedAdReady)
         {
            return false;
         }
      
         var ticks = PlayerPrefs.GetString(SpAdvDateKey, string.Empty);
      
         if (string.IsNullOrEmpty(ticks))
         { 
            return true;
         }
   
         var expires = long.Parse(ticks);
         var now = DateTime.Now.Ticks;
         var duration =  TimeSpan.FromTicks(expires - now);
     
         return duration.TotalSeconds < 0;
      }

      private void OnAdvertisementViewed()
      {
         MaxAppLovin.Instance.HiddenRevard.RemoveListener(OnAdvertisementViewed);
         
         ApplicationManager.Instance.UISystem.Show(ViewType.SpAdvRewardView, 
            new SpAdvRewardWindowParams(AdvertisementReward)
         );
      }

      public bool CanShowDailyBonus()
      {
         return ApplicationManager.Instance.CollectionManagerRef.Base.AvailableDailyBonus();
      }

      public bool CanShowRoomCustomization()
      {
         if (ApplicationManager.Instance.CollectionManagerRef.Base.curLevel < 6 || ApplicationManager.Instance.CurrentRoom == null || !ApplicationManager.Instance.CurrentRoom.Activate)
            return false;

         return !_roomCustomizationShowed;
      }

      public void ShowRoomCustomization()
      {
         _roomCustomizationShowed = true;
         ApplicationManager.Instance.UISystem.Show(ViewType.RoomCustomizationView, 
            new RoomCustomizationWindowParams(
               ApplicationManager.Instance.CurrentRoom, 
               ApplicationManager.Instance.CollectionManagerRef.Base.CustomRoomLogos, 
               ApplicationManager.Instance.CollectionManagerRef.Base.NameMaxLength
            )
         );
         EventManager.ChangeRoomCustomization.AddListener(ChangeRoomCustomization);
      }

      private void ChangeRoomCustomization()
      {
         _roomCustomizationShowed = false;
         EventManager.ChangeRoomCustomization.RemoveListener(ChangeRoomCustomization);
      }
   }
}