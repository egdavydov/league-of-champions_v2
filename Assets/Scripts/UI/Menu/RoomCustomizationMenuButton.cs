using System;
using Game.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Menu
{
    public class RoomCustomizationMenuButton : MonoBehaviour
    {
        [SerializeField] private GameObject _locked;
        [SerializeField] private GameObject _unlocked;
        [SerializeField] private Image _customLogo;

        private RoomsDB _roomDb;
        private MenuWidgetButton _menuWidgetButton;

        private void Awake()
        {
            _menuWidgetButton = GetComponent<MenuWidgetButton>();
            EventManager.ChangeRoom.AddListener(ChangeRoom);
            EventManager.ChangeRoomCustomization.AddListener(ChangeRoomCustomization);
        }

        private void SetCustomLogo(int logoId)
        {
            var customLogo = ApplicationManager.Instance.CollectionManagerRef.Base.GetCustomLogo(logoId);
            _customLogo.sprite = customLogo.Image;
        }

        private void ChangeRoomCustomization()
        {
            ChangeRoom(ApplicationManager.Instance.CurrentRoom.Number);
        }

        private void ChangeRoom(int i)
        {
            _roomDb = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(i);
            if(!_roomDb.Activate)
                _menuWidgetButton.HideButton();

            if (_roomDb.CustomLogo == null)
            {
                _unlocked.SetActive(true);
                _customLogo.gameObject.SetActive(false);
                return;
            }
            
            _unlocked.SetActive(false);
            _customLogo.gameObject.SetActive(true);
            SetCustomLogo((int) _roomDb.CustomLogo);
        }

        private void OnDestroy()
        {
            EventManager.ChangeRoom.RemoveListener(ChangeRoom);
            EventManager.ChangeRoomCustomization.RemoveListener(ChangeRoomCustomization);
        }
    }
}