﻿using Core.UI.Base;
using Game.Managers;
using TMPro;
using UI.DailyBonusWindow;
using UnityEngine;

namespace UI.Menu
{
    public class MenuWidget : View<MenuWidgetViewController, MenuWidgetParams>
    {
        [SerializeField] private MenuWidgetButton SpAdvReward;
        [SerializeField] private TextMeshProUGUI LabelButtonReward;
        [SerializeField] private MenuWidgetButton ButtonDailyBonus;
        [SerializeField] private MenuWidgetButton ButtonRoomCustomization;
    
        private void Awake()
        {
            SpAdvReward.BindClick(SpAdvRewardClick);
            ButtonDailyBonus.BindClick(DailyBonusClick);
            ButtonRoomCustomization.BindClick(RoomCustomizationClick);
            LabelButtonReward.text = $"{Controller.AdvertisementReward}";
            UpdateWidgets();
        }
    
        private void SpAdvRewardClick()
        {
            Controller.GetRewardButtonClickedHandler();
            SpAdvReward.HideButton();
        }
    
        private void DailyBonusClick()
        {
            EventManager.VibrateLight.Invoke();
            ApplicationManager.Instance.UISystem.Show(ViewType.DailyBonusView, new DailyBonusWindowParams());   
        }
    
        private void RoomCustomizationClick()
        {
            Controller.ShowRoomCustomization();
            ButtonRoomCustomization.HideButton();
        }

        private void OnDestroy()
        {
            SpAdvReward.UnBindClick(Controller.GetRewardButtonClickedHandler);
            ButtonDailyBonus.UnBindClick(DailyBonusClick);
        }
   
        private void UpdateWidgets()
        {
            if (Controller.CanShowAdvertisementReward())
            {
                SpAdvReward.ShowButton();
            }
        
            if (Controller.CanShowDailyBonus())
            {
                ButtonDailyBonus.ShowButton();
            }
            else
            {
                ButtonDailyBonus.HideButton();
            }

            if (Controller.CanShowRoomCustomization())
            {
                ButtonRoomCustomization.ShowButton();
            }
        }
    
        private void Update()
        {
            if (Time.frameCount % 30 == 0)
            {
                UpdateWidgets();
            }
        }
    }
}
