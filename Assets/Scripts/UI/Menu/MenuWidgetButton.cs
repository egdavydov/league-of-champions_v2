using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Menu
{
    public class MenuWidgetButton : MonoBehaviour
    {
        private TweenerCore<float, float, FloatOptions> _tweener;
        private Transform _transform;
        private Button _button;

        private void Awake()
        {
            _transform = transform;
            FindButton();
        }

        private void FindButton()
        {
            if (_button != null) return;
            _button = GetComponent<Button>();
        }

        public void ShowButton()
        {
            if(_tweener != null)
                return;
            
            var myFloat = -152f;
            _tweener = DOTween.To(() => myFloat, x => myFloat = x, 0f, 1).OnUpdate(() =>
            {
                _transform.localPosition =  new Vector3(myFloat, _transform.localPosition.y, 0f);
            });
            _tweener.SetAutoKill(false);
            _tweener.SetEase(Ease.OutExpo);
            _tweener.Play();
        }
        
        public void HideButton()
        {
            if(_tweener == null)
                return;

            _tweener.timeScale = 2f;
            _tweener.PlayBackwards();
            _tweener = null;
        }

        public void BindClick(UnityAction call)
        {
            FindButton();
            _button.onClick.AddListener(call);
        }
        
        public void UnBindClick(UnityAction call)
        {
            FindButton();
            _button.onClick.RemoveListener(call);
        }
    }
}