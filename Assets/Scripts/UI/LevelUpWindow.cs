﻿using Advertising;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;

namespace UI
{
    public class LevelUpWindow : BasicWindow
    {
        [SerializeField] private TextMeshProUGUI _rewardTxt;
        [SerializeField] private GameObject _advAcivateBtn;
        [SerializeField] private GameObject _advUnactivateBtn;
        private float _reward;

        public override void OpenWindow()
        {
            ApplicationManager.Instance.CollectionManagerRef.Base.levelBonus *= Random.Range(1.01f, 1.6f);
            _reward = Mathf.Round(ApplicationManager.Instance.CollectionManagerRef.Base.levelBonus);
            _rewardTxt.text = Reduction.ReductionNumber(_reward, 0, true);

            RefreshAdvButton();
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            Debug.Log("reward " + _reward);
            base.OpenWindow();
        }
    
        private void RefreshAdvButton()
        {
            var advActivate = MaxAppLovin.Instance.IsRewardedAdReady;
            _advAcivateBtn.SetActive(advActivate);
            _advUnactivateBtn.SetActive(!advActivate);
        }
    
        public void OnBtnAdv()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
        
            MaxAppLovin.Instance.HiddenRevard.AddListener(RewardAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }
    
        private void RewardAdv()
        {
            _reward *= 3;
            Debug.Log("reward Adv " + _reward);
            Reward();
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(RewardAdv);
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton);
            FacebookEventSubscribers.LogLvlUpX3BoostEvent();
        }
    
        public void Reward()
        {
            EventManager.VibrateMedium.Invoke();
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(_reward);
            ApplicationManager.Instance.CollectionManagerRef.Base.AddLevel();
            base.CloseWindow();
        }
    }
}
