﻿using System.Collections;
using UnityEngine;

public class Debuging : MonoBehaviour
{
    [SerializeField] private GameObject _debugBtn;

    private int _countClick = 0;
    private Coroutine _cor = null;

    private void Start()
    {
        
#if PRODUCTION
        _debugBtn.SetActive(false);
#else
        _debugBtn.SetActive(true);
#endif
        
    }
    
    public void WindowOpened()
    {
    }
    
    public void WindowClosed()
    {
    }

    public void OnBtn()
    {
#if UNITY_ANDROID || UNITY_EDITOR
        _countClick++;
        Debug.Log(_countClick);

        if (_cor != null)
        {
            StopCoroutine(_cor);
        }

        _cor = StartCoroutine(Timer());

        if (_countClick >= 5)
        {
            _debugBtn.SetActive(true);
            Debug.Log("Open");
            Destroy(this);
        }
#endif
    }

    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(0.5f);
        _countClick = 0;
    }
}