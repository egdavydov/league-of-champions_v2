﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Aim : MonoBehaviour
{
    private Image m_image;
    private Color m_color;
    private RectTransform m_transform;
    private Vector3 m_pos;

    public IEnumerator Show(WaitForSeconds hideDelay)
    {
        if (m_image == null)
        {
            m_image = GetComponent<Image>();
        }
        
        if (m_transform == null)
        {
            m_transform = GetComponent<RectTransform>();
        }

        m_color = new Color(0f, Random.Range(0f, 1f), 1f);
        m_image.color = m_color;
        m_pos.x = Random.Range(-250f, 250f);
        m_pos.y = Random.Range(-400f, 400f);
        m_pos.z = 0;
        m_transform.localPosition = m_pos;

        yield return hideDelay;

        gameObject.SetActive(false);
    }
}