﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SuperdonatsFX : AFX
{
    [SerializeField] private float _delay = 1f;
    private WaitForSeconds _delaySecond;
    private Animator[] _anim;
    private RectTransform[] _rect;
    private Coroutine _cor;

    private void Start()
    {
        _delaySecond = new WaitForSeconds(_delay);
        _anim = GetComponentsInChildren<Animator>();
        
        _rect = new RectTransform[_anim.Length];
        
        for (int i = 0; i < _anim.Length; i++)
        {
            _rect[i] = _anim[i].GetComponent<RectTransform>();
        }
    }

    public override void PlayFX()
    {
        _cor = StartCoroutine(PlayAnim());
    }

    public override void StopFX()
    {

        StopCoroutine(_cor);
    }

    private IEnumerator PlayAnim()
    {
        float randomX;
        float randomY;
        Vector3 vector;

        while (true)
        {
            for (int i = 0; i < _anim.Length; i++)
            {
               if (IsAnimationPlaying(_anim[i]))
                {
                    randomX = Random.Range(-100f, 0f);
                    randomY = Random.Range(-200f, -100f);
                    vector = new Vector3(randomX, randomY, 0);
                    _rect[i].anchoredPosition = vector;

                    _anim[i].SetTrigger("Play");
                    break;
                }
            }

            yield return _delaySecond;
        }
    }
    

}
