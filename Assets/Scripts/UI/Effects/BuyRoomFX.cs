﻿using System.Collections;
using System.Collections.Generic;
using Game.Managers;
using Services;
using UnityEngine;
using UnityEngine.UI;

public class BuyRoomFX : MonoBehaviour
{
    [SerializeField] private Image _logo;
    [SerializeField] private Image _panel;
    private Animator _anim;

    private void Start()
    {
        _anim = GetComponent<Animator>();
        EventManager.BuyRoom.AddListener((i) => Play(i));
    }

    private void Play(int roomNumber)
    {
        _logo.sprite = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(roomNumber).LogoRoom;
        _panel.color = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(roomNumber).ColorRoom;
        _anim.SetTrigger("Play");
    }

    private void OnDestroy()
    {
        EventManager.BuyRoom.RemoveListener((i) => Play(i));
    }
}
