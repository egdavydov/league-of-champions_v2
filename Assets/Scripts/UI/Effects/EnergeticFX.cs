﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class EnergeticFX : AFX
{
    [SerializeField] private Image _panel;
    [SerializeField] private float _delay = 0.03f;
    private Animator[] _anim;
    private RectTransform[] _raysRect;
    private Coroutine _cor;

    private void Start()
    {
        _anim = GetComponentsInChildren<Animator>();
        _raysRect = new RectTransform[_anim.Length];
        
        for (int i = 0; i < _raysRect.Length; i++)
        {
            _raysRect[i] = _anim[i].GetComponent<RectTransform>();
        }
    }

    public override void PlayFX()
    {
        _panel.DOFade(0.8f, 1f);
        _cor = StartCoroutine(PlayAnimRays());
    }

    public override void StopFX()
    {
        _panel.DOFade(0f, 1f);
        StopCoroutine(_cor);
    }

    private IEnumerator PlayAnimRays()
    {
        float randomX;
        Vector3 vector;
        
        while (true)
        {
            for (int i = 0; i < _raysRect.Length; i++)
            {
                if (IsAnimationPlaying(_anim[i]))
                {
                    randomX = Random.Range(-300f, 300f);
                    vector = new Vector3(randomX, -350f, 0);
                    _raysRect[i].anchoredPosition = vector;
                    
                    randomX = Random.Range(0.3f, 1f);
                    vector = new Vector3(randomX, 1f, 1f);
                    _raysRect[i].localScale = vector;
                    
                    _anim[i].SetTrigger("Play");
                    break;
                }
            }
            
            float random = Random.Range(0.03f, 0.1f);
            yield return new WaitForSeconds(random);
        }
    }
}
