﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConcentrationFX : AFX
{
    [SerializeField] private Animator _animatorLogo;
    private Animator _animator;
    private ParticleSystem[] _particles;

    private void Start()
    {
        _particles = GetComponentsInChildren<ParticleSystem>();
        _animator = GetComponent<Animator>();
    }

    public override void PlayFX()
    {
        foreach (var particle in _particles)
        {
            particle.Play();
        }
        _animator.SetBool("Open", true);
        _animatorLogo.SetBool("Open", true);
    }

    public override void StopFX()
    {
        foreach (var particle in _particles)
        {
            particle.Stop();
        }
        _animator.SetBool("Open", false);
        _animatorLogo.SetBool("Open", false);
    }
}
