﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AFX : MonoBehaviour
{
    public abstract void PlayFX();
    public abstract void StopFX();
    
    protected bool IsAnimationPlaying(Animator anim) {        
        // берем информацию о состоянии
        var animatorStateInfo = anim.GetCurrentAnimatorStateInfo(0);
        // смотрим, есть ли в нем имя какой-то анимации, то возвращаем true
        if (animatorStateInfo.IsName("Idle"))             
            return true;

        return false;
    }
}
