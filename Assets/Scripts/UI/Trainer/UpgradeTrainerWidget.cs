using System;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;

namespace UI.Trainer
{
    public class UpgradeTrainerWidget : MonoBehaviour
    {
        [SerializeField] private TrainerSkillWidget _timeLimitUpgradeWidget;
        [SerializeField] private TrainerSkillWidget _revenueUpgradeWidget;
        [SerializeField] private TrainerWidget _trainerWidget;

        private RoomsDB.Trainer _trainer;
        private BaseDB _baseDb;
        private bool _isInitialized;

        private void Init()
        {
            _baseDb = ApplicationManager.Instance.CollectionManagerRef.Base;
            _timeLimitUpgradeWidget.Init($"+{_baseDb.OfflineEarningsTimelimitStep} min");
            _revenueUpgradeWidget.Init($"+{_baseDb.OfflineEarningsPercentStep*100}%");
            _isInitialized = true;
        }

        public void WriteIn(RoomsDB.Trainer trainer)
        {
            if (!_isInitialized)
            {
                Init();
            }
            _trainer = trainer;
            
            Refresh();
        }

        private void Refresh()
        {
            var timeLimitLevel = _trainer.GetTimeLimitLevel(_baseDb);
            _timeLimitUpgradeWidget.WriteIn(_trainer.GetUpgradeTimeLimitPrice(), timeLimitLevel);
            var revenueLevel = _trainer.GetRevenueLevel(_baseDb);
            _revenueUpgradeWidget.WriteIn(_trainer.GetUpgradeRevenuePrice(), revenueLevel);

            if (timeLimitLevel + revenueLevel < 0)
                FacebookEventSubscribers.LogTrainerUpgradeMaxEvent(ApplicationManager.Instance.CurrentRoom.Number);
        }
        
        public void UpgradeTimeLimitBtn()
        {
            EventManager.VibrateMedium.Invoke();
            var upgradePrice = _trainer.GetUpgradeTimeLimitPrice();
            _trainer.TimeLimitLevel++;
            _baseDb.ChangeSkillPoints(upgradePrice * -1);
            ApplicationManager.Instance.SaveSystemRef.Save();
            
            Refresh();
            _trainerWidget.Refresh();
            
            FacebookEventSubscribers.LogTrainerUpgradeTimeEvent(ApplicationManager.Instance.CurrentRoom.Number);
        }
        public void UpgradeRevenueBtn()
        {
            EventManager.VibrateMedium.Invoke();
            var upgradePrice = _trainer.GetUpgradeRevenuePrice();
            _trainer.RevenueLevel++;
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(upgradePrice * -1);
            ApplicationManager.Instance.SaveSystemRef.Save();
            
            Refresh();
            _trainerWidget.Refresh();
            
            FacebookEventSubscribers.LogTrainerUpgradePercentEvent(ApplicationManager.Instance.CurrentRoom.Number);
        }
    }
}