using System;
using Game.Managers;
using Tutorial.Steps;
using UnityEngine;

namespace UI.Trainer
{
    public class TrainerWindow : BasicWindow
    {
        private RoomsDB.Trainer _trainer;

        [SerializeField] private TrainerWidget _trainerWidget;
        [SerializeField] private BuyTrainerWidget _buyTrainerWidget;
        [SerializeField] private UpgradeTrainerWidget _upgradeTrainerWidget;
        [SerializeField] private HelpWidget _helpWidget;
        
        public void OpenWindow(RoomsDB room)
        {
            _trainer = room.GetTrainer;
            _trainerWidget.WriteIn(_trainer);
            _upgradeTrainerWidget.WriteIn(_trainer);
            _helpWidget.Hide();

            if (_trainer.Activate)
            {
                _buyTrainerWidget.gameObject.SetActive(false);
            }
            else
            {
                _buyTrainerWidget.gameObject.SetActive(true);
                foreach (Transform child in _buyTrainerWidget.transform)
                {
                    child.gameObject.SetActive(true);
                }
                _buyTrainerWidget.WriteIn(room.Number, _trainer);
            }
            base.OpenWindow();
        }
        
        protected override void WindowOpenedHandler()
        {
            if (ApplicationManager.Instance.TutorialSystem != null && ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is TrainerStep)
                ApplicationManager.Instance.TutorialSystem.TutorialWindowController.ShowTarget();
        }
        
        protected override void WindowClosedHandler()
        {
            if (ApplicationManager.Instance.TutorialSystem != null && ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is TrainerStep)
                ApplicationManager.Instance.TutorialSystem.CompleteStep();
        }
    }
}