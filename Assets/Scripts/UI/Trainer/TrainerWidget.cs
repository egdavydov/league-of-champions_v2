using System;
using Game.Managers;
using TMPro;
using UnityEngine;

namespace UI.Trainer
{
    public class TrainerWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _name;
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _description;
        [SerializeField] private TextMeshProUGUI _offlineTimelimit;
        [SerializeField] private TextMeshProUGUI _offlineRevenue;

        private RoomsDB.Trainer _trainer;

        public void WriteIn(RoomsDB.Trainer trainer)
        {
            _trainer = trainer;

            _title.text = _trainer.Title;
            _name.text = _trainer.Name;
            _description.text = _trainer.Description;
            Refresh();
        }

        public void Refresh()
        {
            var timeLimit = _trainer.GetOfflineEarningTimeLimit(ApplicationManager.Instance.CollectionManagerRef.Base);
            var timeLimitHours = timeLimit / 60;
            var timeLimitMinutes = timeLimit % 60;
            _offlineTimelimit.text = $"{timeLimitHours}H {timeLimitMinutes:D2}M";
            
            var revenuePercent = _trainer.GetOfflineEarningRevenue(ApplicationManager.Instance.CollectionManagerRef.Base) * 100;
            _offlineRevenue.text = revenuePercent + "%";
        }
    }
}