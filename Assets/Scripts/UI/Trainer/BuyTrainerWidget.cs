using Game.Managers;
using Services;
using TMPro;
using UnityEngine;

namespace UI.Trainer
{
    public class BuyTrainerWidget : MonoBehaviour
    {
        [SerializeField] private GameObject _active;
        [SerializeField] private GameObject _noActive;
        [SerializeField] private TextMeshProUGUI _price;

        private RoomsDB.Trainer _trainer;
        private Animator _animator;
        private int _numberRoom;
        
        private void Start()
        {
            _animator = GetComponent<Animator>();
            EventManager.ChangeSkillPoints.AddListener(CheckPrice);
        }
        
        public void WriteIn(int numberRoom, RoomsDB.Trainer trainer)
        {
            _trainer = trainer;
            _numberRoom = numberRoom;
            _price.text = Reduction.ReductionNumber(_trainer.Price, 0, true);
            
            CheckPrice();
        }
        
        private void CheckPrice()
        {
            if(_trainer.Activate)
                return;

            var active = _trainer.Price <= ApplicationManager.Instance.CollectionManagerRef.Base.curSkillPoints;
            _active.SetActive(active);
            _noActive.SetActive(!active);
        }
        
        public void BuyTrainerBtn()
        {
            EventManager.VibrateMedium.Invoke();
            _trainer.Activate = true;
            ApplicationManager.Instance.RoomsManagerRef.Rooms[_numberRoom].GetTrainerActivate.Place(ApplicationManager.Instance.RoomsManagerRef.Rooms[_numberRoom].GetRoomDB);
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(_trainer.Price * -1);
            ApplicationManager.Instance.SaveSystemRef.Save();
            
            _animator.SetTrigger("Close");
            
            FacebookEventSubscribers.LogTrainerUnlockEvent(_numberRoom);
        }
        
        private void OnDestroy()
        {
            EventManager.ChangeSkillPoints.RemoveListener(CheckPrice);
        }
    }
}