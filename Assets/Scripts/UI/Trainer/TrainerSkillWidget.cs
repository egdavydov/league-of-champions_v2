using Game.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Trainer
{
    public class TrainerSkillWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _levelText;
        [SerializeField] private TextMeshProUGUI _valueText;
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private GameObject _lockBtn;
        [SerializeField] private GameObject _unlockBtn;
        [SerializeField] private GameObject _allUpgraded;

        private int _price;
        private int _level;

        private void Start()
        {
            EventManager.ChangeSkillPoints.AddListener(CheckPrice);
        }

        public void Init(string value)
        {
            _valueText.text = value;
        }

        public void WriteIn(int price, int level)
        {
            _price = price;
            _level = level;
            _priceText.text = Reduction.ReductionNumber(_price, 0, true);
            _levelText.text = level < 0 ? "<#FF1C89>MAX</color>" : $"lvl {_level}";

            if (_level < 0)
            {
                _unlockBtn.SetActive(false);
                _lockBtn.SetActive(false);
                _allUpgraded.SetActive(true);
            }
            else{
                _allUpgraded.SetActive(false);
            }
            
            CheckPrice();
        }
        
        private void CheckPrice()
        {
            if(_level < 0)
                return;
            
            var available = _price <= ApplicationManager.Instance.CollectionManagerRef.Base.curSkillPoints;
            _unlockBtn.SetActive(available);
            _lockBtn.SetActive(!available);
        }

        private void OnDestroy()
        {
            EventManager.ChangeSkillPoints.RemoveListener(CheckPrice);
        }
    }
}