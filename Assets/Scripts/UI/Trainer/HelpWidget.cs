using System;
using UnityEngine;

namespace UI.Trainer
{
    public class HelpWidget : MonoBehaviour
    {
        [SerializeField] private GameObject _activeBtn;
        [SerializeField] private GameObject _noActiveBtn;
        [SerializeField] private GameObject _helpText;

        private bool _showHelp;
        
        private void Start()
        {
            Refresh();
        }

        private void Refresh()
        {
            _noActiveBtn.SetActive(!_showHelp);
            _activeBtn.SetActive(_showHelp);
            _helpText.SetActive(_showHelp);
        }

        public void Hide()
        {
            _showHelp = false;
            Refresh();
        }

        public void HelpBtnClick()
        {
            _showHelp = !_showHelp;
            Refresh();
        }
    }
}