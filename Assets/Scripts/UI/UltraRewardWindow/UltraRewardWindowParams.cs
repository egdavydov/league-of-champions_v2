using Core.UI.Base;
using DB;

namespace UI.UltraRewardWindow
{
    public class UltraRewardWindowParams : ViewParams
    {
        public readonly UltraBonusDB.UltraBonus UltraBonus;

        public UltraRewardWindowParams(UltraBonusDB.UltraBonus ultraBonus)
        {
            UltraBonus = ultraBonus;
        }
    }
}