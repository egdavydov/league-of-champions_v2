using System;
using DG.Tweening;
using UnityEngine;

namespace UI.UltraRewardWindow
{
    public class TimeJumpArrowAnimation : MonoBehaviour
    {
        private Animator _animator;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void StartAnimation()
        {
            _animator.SetTrigger("Play");
        }

    }
}