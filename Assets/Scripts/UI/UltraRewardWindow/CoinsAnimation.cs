using UnityEngine;

namespace UI.UltraRewardWindow
{
    public class CoinsAnimation : MonoBehaviour
    {
        private float _rotationRadius = 10f;
        private float _angularSpeed = 1.5f;
        private float _posX;
        private float _posY;
        private float _angle = 0f;
        private Vector3 _rotationCenter;

        private void Awake()
        {
            _rotationCenter = transform.localPosition;
        }

        private void Update()
        {
            _posX = _rotationCenter.x + Mathf.Cos(_angle) * _rotationRadius;
            _posY = _rotationCenter.y + Mathf.Sin(_angle) * _rotationRadius;
            transform.localPosition = new Vector3(_posX, _posY, transform.localPosition.z);

            _angle -= Time.deltaTime * _angularSpeed;
            if (_angle <= -360f)
                _angle = 0f;
        }
    }
}