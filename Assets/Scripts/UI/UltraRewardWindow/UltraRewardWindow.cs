using System;
using System.Linq;
using Advertising;
using Core.UI.Base;
using DB;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.UltraRewardWindow
{
    public class UltraRewardWindow : View<UltraRewardWindowController, UltraRewardWindowParams>
    {
        [SerializeField] private GameObject _timeJumpReward;
        [SerializeField] private TimeJumpArrowAnimation _timeJumpRewardArrow;
        [SerializeField] private GameObject _coinsReward;
        [SerializeField] private TextMeshProUGUI _rewardTimeJumpLabel;
        [SerializeField] private TextMeshProUGUI _rewardCoinsLabel;
        [SerializeField] private GameObject _coinsIcon;
        [SerializeField] private GameObject _skillpointsIcon;
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private Button _okBtn;
        
        private Animator _viewAnimator;
        private bool _isInitialized;
        private Canvas _canvas;
        
        private void Awake()
        {
            _viewAnimator = GetComponent<Animator>();
            _okBtn.onClick.AddListener(Close);
        }

        private void Refresh()
        {
        }

        public override void Show()
        {
            _timeJumpReward.SetActive(Params.UltraBonus.type == UltraBonusDB.UltraBonusType.TimeJump);
            _coinsReward.SetActive(Params.UltraBonus.type != UltraBonusDB.UltraBonusType.TimeJump);
            _rewardTimeJumpLabel.text = Reduction.ReductionNumber(Params.UltraBonus.reward, 0, true);
            _rewardCoinsLabel.text = Reduction.ReductionNumber(Params.UltraBonus.reward, 0, true);
            
            switch (Params.UltraBonus.type)
            {
                case UltraBonusDB.UltraBonusType.Gold:
                    _coinsIcon.SetActive(true);
                    _skillpointsIcon.SetActive(false);
                    _title.text = "IT'S ALL YOURS!";
                    break;
                case UltraBonusDB.UltraBonusType.SkillPoints:
                    _coinsIcon.SetActive(false);
                    _skillpointsIcon.SetActive(true);
                    _title.text = "NOW WE'RE TALKING!";
                    break;
                case UltraBonusDB.UltraBonusType.TimeJump:
                    _title.text = "JUMP INTO THE FUTURE!";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            base.Show();
            DontDestroyOnLoad(this);
            _viewAnimator.SetTrigger("Open");
        }
        
        public override void Close()
        {
            _viewAnimator.SetTrigger("Close");
        }
        
        private void OnDestroy()
        {
            _okBtn.onClick.RemoveListener(Close);
        }
        
        public void WindowOpened()
        {
            if (Params.UltraBonus.type == UltraBonusDB.UltraBonusType.TimeJump)
                _timeJumpRewardArrow.StartAnimation();
        }
    
        public void WindowClosed()
        {
        }
    }
}