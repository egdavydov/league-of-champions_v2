using Configs;

namespace UI.TutorialWindow
{
    public interface ITutorialView
    {
        void Show(TutorialTargetConfig tutorialTarget);
        void Hide();
    }
}