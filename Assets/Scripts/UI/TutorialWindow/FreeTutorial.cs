using Configs;
using DG.Tweening;
using UnityEngine;

namespace UI.TutorialWindow
{
    public class FreeTutorial : TutorialView
    {
        private float _positionY;
        
        public override void Show(TutorialTargetConfig tutorialTarget)
        {
            GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, tutorialTarget.Position);
            base.Show(tutorialTarget);
            /*
            _positionY = transform.position.y;
            transform.position = new Vector3(transform.position.x, -10f, transform.position.z);
            _tweener = transform.DOMoveY(_positionY, 2f).SetAutoKill(false).SetEase(Ease.OutExpo).Play();
            */
        }
    }
}