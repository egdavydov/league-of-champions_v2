using Configs;
using Core.UI.Base;
using Game.Managers;
using UnityEngine;

namespace UI.TutorialWindow
{
    public class TutorialWindowController : ViewController<TutorialWindow>
    {
        private int _currentTargetIndex;

        public TutorialTargetConfig CurrentTarget { get; private set; }
        public bool TargetShowed { get; set; }

        public void Init()
        {
            ApplicationManager.Instance.TutorialSystem.TutorialWindowController = this;
        }

        public void InitTarget()
        {
            CurrentTarget = View.Params.TutorialStepConfig.Targets[_currentTargetIndex];
            if(View.Params.TutorialStep.CheckShowTarget(CurrentTarget))
                View.SetTarget();
        }
        
        public void NextTutorialTarget()
        {
            Debug.Log("NextTutorialTarget");
            _currentTargetIndex++;

            if (_currentTargetIndex > View.Params.TutorialStepConfig.Targets.Count - 1)
            {
                ApplicationManager.Instance.TutorialSystem.CompleteStep();
                return;
            }
            
            TargetShowed = false;
            View.Refresh();
            
            CurrentTarget = View.Params.TutorialStepConfig.Targets[_currentTargetIndex];
            if (View.Params.TutorialStep.CheckShowTarget(CurrentTarget))
                View.SetTarget();
        }
        
        public void CompleteTutorialStep()
        {
            Debug.Log("CompleteTutorialStep");
            _currentTargetIndex = 0;
            CurrentTarget = null;
            TargetShowed = false;
            View.Reset();
            View.Hide();
        }

        public void ShowTarget()
        {
            View.SetTarget();
        }

        public void SetCamera()
        {
            View.SetCamera();
        }
    }
}