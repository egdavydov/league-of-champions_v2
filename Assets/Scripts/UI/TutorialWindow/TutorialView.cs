using Configs;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace UI.TutorialWindow
{
    public abstract class TutorialView : MonoBehaviour, ITutorialView
    {
        public enum Types
        {
            Bottom,
            Free
        }
        
        protected Tweener _tweener;
        
        [SerializeField] private TextMeshProUGUI _tutorialText;
        public Types Type;

        public virtual void Show(TutorialTargetConfig tutorialTarget)
        {
            _tutorialText.text = tutorialTarget.Text;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            if (_tweener == null)
            {
                gameObject.SetActive(false);
                return;
            }

            _tweener.PlayBackwards();
            gameObject.SetActive(false);
        }
    }
}