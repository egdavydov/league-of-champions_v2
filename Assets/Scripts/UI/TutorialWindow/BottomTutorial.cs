using System;
using Configs;
using DG.Tweening;
using UnityEngine;

namespace UI.TutorialWindow
{
    public class BottomTutorial : TutorialView
    {
        [SerializeField] private GameObject _kittyIdle;
        [SerializeField] private GameObject _kittySmile;
        [SerializeField] private GameObject _kittyPeace;

        private float _positionY;
        private RectTransform _transform;
        private float _startPositionY;
        
        public enum KittyStates
        {
            Idle,
            Smile,
            Peace
        }

        private void Awake()
        {
            _transform = GetComponent<RectTransform>();
        }

        public override void Show(TutorialTargetConfig tutorialTarget)
        {
            SetKittyState(tutorialTarget.KittyType);
            base.Show(tutorialTarget);
            /*
            _startPositionY = -600f;
            _tweener = DOTween.To(() => _startPositionY, y => _startPositionY = y, 0f, 2f).OnUpdate(() =>
            {
                _transform.anchoredPosition = new Vector3(0f, _startPositionY, 0f);
            }).SetEase(Ease.OutExpo).Play();
            */
        }
        
        private void SetKittyState(KittyStates state)
        {
            _kittyIdle.SetActive(false);
            _kittySmile.SetActive(false);
            _kittyPeace.SetActive(false);
            
            switch (state)
            {
                case KittyStates.Idle:
                    _kittyIdle.SetActive(true);
                    break;
                case KittyStates.Smile:
                    _kittySmile.SetActive(true);
                    break;
                case KittyStates.Peace:
                    _kittyPeace.SetActive(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }
    }
}