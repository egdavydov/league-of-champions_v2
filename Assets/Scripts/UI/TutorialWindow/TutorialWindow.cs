using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Configs;
using Core.UI.Base;
using Tutorial;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Managers;
using Tutorial.Steps;

namespace UI.TutorialWindow
{
    public class TutorialWindow : View<TutorialWindowController, TutorialWindowParams>
    {
        [SerializeField] private TutorialView[] _tutorialViews;
        [SerializeField] private GameObject _hand;

        private TutorialView _tutorialView;
        private Camera _camera;
        private ScrollCamera _scrollCamera;
        private Canvas _canvas;
        private bool _isInit;
        private Vector2 _canvasSizeDelta;
        private RectTransform _handTransform;
        private List<RaycastResult> _raycastResults;
        private TutorialTag _currentTargetObject;
        private float _showTutorialTimer;
        
        public override void Show()
        {
            base.Show();
            if(!_isInit)
                Init();

            Reset();
            Controller.InitTarget();
        }

        private void Init()
        {
            SetCamera();
            _canvasSizeDelta = _canvas.GetComponent<RectTransform>().sizeDelta;
            _handTransform = _hand.GetComponent<RectTransform>();
            Controller.Init();
            DontDestroyOnLoad(this);
            
            _isInit = true;
        }

        public void SetCamera()
        {
            _camera = Camera.main;
            _scrollCamera = _camera.GetComponent<ScrollCamera>();
            _canvas = gameObject.GetComponent<Canvas>();
            if (_canvas.worldCamera == null)
                _canvas.worldCamera = _camera;
        }

        private void Update() {
            if(!Controller.TargetShowed)
                return;

            _showTutorialTimer += Time.deltaTime;
            
#if UNITY_EDITOR            
            if (!Input.GetMouseButtonDown(0)) return;

            var pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1, position = Input.mousePosition,
            };
#else
            if (Input.touchCount == 0) return;

            var pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1, position = Input.touches[0].position,
            };
#endif

            switch (Controller.CurrentTarget.TargetType)
            {
                case TutorialTargetTypes.Button:
                    _raycastResults = new List<RaycastResult>();
                    EventSystem.current.RaycastAll(pointerData, _raycastResults);
                
                    _raycastResults.ForEach(r =>
                    {
                        var tag = r.gameObject.GetComponent<TutorialTag>();
                        if (tag == null) return;
                
                        Debug.Log(tag.Tag);

                        if (!Controller.CurrentTarget.Equals(tag)) return;
                
                        var tagButton = tag.gameObject.GetComponent<Button>();
                        if (tagButton != null)
                            tagButton.onClick.Invoke();
                
                        if( ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep().CheckTargetComplete(Controller.CurrentTarget) )
                            Controller.NextTutorialTarget();
                    });
                    
                    break;
                case TutorialTargetTypes.ScreenTap:
                    if (_showTutorialTimer <= 3f)
                        break;
                    
                    if (ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep().CheckTargetComplete(Controller.CurrentTarget))
                        Controller.NextTutorialTarget();

                    break;
            }
        }

        public void Reset()
        {
            _currentTargetObject = null;
            _showTutorialTimer = 0f;
            Refresh();
        }

        public void SetTarget()
        {
            if (Controller.CurrentTarget == null)
            {
                Controller.TargetShowed = false;
                Refresh();
                return;
            }
            
            EventManager.TutorialStepBegin.Invoke(ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep());
            
            if (Controller.CurrentTarget.TargetType == TutorialTargetTypes.WaitComplete)
                return;
            
            _currentTargetObject = ApplicationManager.Instance.TutorialSystem.RegisteredTags.FirstOrDefault(tutorialTag => Controller.CurrentTarget.Equals(tutorialTag));
            if (_currentTargetObject != null)
            {
                if (!Controller.CurrentTarget.NoScrollCamera)
                    _scrollCamera.SetCameraObject(_currentTargetObject.gameObject.transform.position);

                if (Controller.CurrentTarget.TargetType == TutorialTargetTypes.Button)
                    _handTransform.anchoredPosition = _currentTargetObject.GetCanvasPosition(_canvasSizeDelta);
            }

            Controller.TargetShowed = true;
            Refresh();
        }

        public void Refresh()
        {
            _hand.SetActive(Controller.TargetShowed && Controller.CurrentTarget.TargetType == TutorialTargetTypes.Button);
            if(_scrollCamera != null)
                _scrollCamera.IsSwipe = !Controller.TargetShowed;
            
            foreach (var tutorialView in _tutorialViews)
            {
                tutorialView.Hide();
            }

            if (!Controller.TargetShowed)
            {
                _showTutorialTimer = 0f;
                return;
            }
            
            _tutorialView = _tutorialViews.FirstOrDefault(view => view.Type == Controller.CurrentTarget.ViewType);
            if (_tutorialView != null)
                _tutorialView.Show(Controller.CurrentTarget);
        }
        
        public override void Hide()
        {
            if(_scrollCamera != null)
                _scrollCamera.IsSwipe = true;
            base.Hide();
        }
    }
}