using Configs;
using Core.UI.Base;
using Tutorial.Steps;

namespace UI.TutorialWindow
{
    public class TutorialWindowParams : ViewParams
    {
        public TutorialStepConfig TutorialStepConfig { get; }
        public TutorialStep TutorialStep { get; }

        public TutorialWindowParams(TutorialStepConfig tutorialStepConfig, TutorialStep tutorialStep)
        {
            TutorialStepConfig = tutorialStepConfig;
            TutorialStep = tutorialStep;
        }
    }
}