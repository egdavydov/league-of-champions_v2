﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectRate : MonoBehaviour
{
    private Star[] _stars;
    private int _starsCount;

    public int GetStars => _starsCount;

    private void Start()
    {
        _stars = GetComponentsInChildren<Star>();
        for (int i = 0; i < _stars.Length; i++)
        {
            _stars[i].Number = i;
        }
    }

    public void Refresh(int number)
    {
        _starsCount = number + 1;
        for (int i = 0; i < _stars.Length; i++)
        {
            _stars[i].EnableStar(i <= number);
        }
    }
}