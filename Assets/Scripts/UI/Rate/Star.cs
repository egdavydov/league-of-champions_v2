﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    [SerializeField] private GameObject _On;
    [SerializeField] private GameObject _Off;
    private int _number;
    private SelectRate _selectRate;

    private void Start()
    {
        _selectRate = GetComponentInParent<SelectRate>();
    }

    public int Number
    {
        get => _number;
        set => _number = value;
    }

    public void OnBtn()
    {
        _selectRate.Refresh(_number);
    }

    public void EnableStar (bool enable)
    {
        _On.SetActive(enable);
        _Off.SetActive(!enable);
    }
}
