using System;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI.Tournament
{
    public class GamerWidget : MonoBehaviour
    {
        [SerializeField] private Image avatar;
        [SerializeField] private TextMeshProUGUI winrate;
        
        public AnimationLabels winParticles;
        public AnimationLabels lossParticles;

        private RoomsDB.Gamer Gamer { get; set; }

        public void Init(RoomsDB.Gamer gamer)
        {
            Gamer = gamer;

            avatar.sprite = Gamer.GetAvatar();
            winrate.text = $"{Gamer.curWinRate}%";
        }
    }
}