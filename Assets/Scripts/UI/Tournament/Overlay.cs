﻿using System;
using UI.Interfaces;
using UnityEngine;

namespace UI.Tournament
{
    public class Overlay : MonoBehaviour
    {
        [SerializeField] private GameObject nextWindow;
        private IWindow _nextWindowI;

        private void Awake()
        {
            _nextWindowI = nextWindow.GetComponent<IWindow>();
        }

        public void ShowNextWindow()
        {
            gameObject.SetActive(false);
            _nextWindowI.OpenWindow();
        }
    }
}
