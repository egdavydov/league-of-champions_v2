using System;
using System.Collections.Generic;
using Game.Managers;
using Tournament.Managers;
using UnityEngine;

namespace UI.Tournament
{
    public class GamersWidget : MonoBehaviour
    {
        [SerializeField] private GameObject gamerWidgetPrefab;
        private readonly List<GamerWidget> _gamerWidgets = new List<GamerWidget>(); 

        private void Awake()
        {
            foreach (var gamer in ApplicationManager.Instance.CurrentRoom.GetGamers)
            {
                if(!gamer.activate)
                    continue;
                
                var gamerGameObject = Instantiate(gamerWidgetPrefab, gameObject.transform);
                gamerGameObject.GetComponent<GamerWidget>().Init(gamer);
                _gamerWidgets.Add(gamerGameObject.GetComponent<GamerWidget>());
            }
            
            TournamentManager.GamerWinAction += GamerWinAction;
            TournamentManager.GamerLoseAction += GamerLoseAction;
        }

        private void GamerLoseAction(int number)
        {
            _gamerWidgets[number].lossParticles.Play(0f);
        }

        private void GamerWinAction(int number)
        {
            _gamerWidgets[number].winParticles.Play(0f);
        }

        private void OnDestroy()
        {
            TournamentManager.GamerWinAction -= GamerWinAction;
            TournamentManager.GamerLoseAction -= GamerLoseAction;
        }
    }
}