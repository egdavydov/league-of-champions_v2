using System;
using TMPro;
using UnityEngine;

namespace UI.Tournament
{
    public class ScoresWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI yourScoreTxt;
        [SerializeField] private TextMeshProUGUI enemyScoreTxt;

        public int YourScore { get; set; }
        public int EnemyScore { get; set; }

        private void Update()
        {
            yourScoreTxt.text = YourScore.ToString();
            enemyScoreTxt.text = EnemyScore.ToString();
        }
    }
}