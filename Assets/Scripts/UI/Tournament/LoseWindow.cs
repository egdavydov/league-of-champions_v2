using Advertising;
using Game.Managers;
using TMPro;
using Tournament.Managers;
using UI.Interfaces;
using UnityEngine;

namespace UI.Tournament
{
    public class LoseWindow : BasicWindow
    {
        [SerializeField] private TextMeshProUGUI headerText;
        [SerializeField] private TextMeshProUGUI descriptionText;
        [SerializeField] private GameObject advAcivateBtn;
        [SerializeField] private GameObject advUnactivateBtn;
        [SerializeField] private GameObject advButtons;
        [SerializeField] private TextMeshProUGUI exitBtnText;
        
        private Animator _animator;

        private void Start()
        {
            _animator = GetComponent<Animator>();
        
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
        }

        public void SetData(bool tryAgain)
        {
            if (tryAgain)
            {
                headerText.text = "OH NO!\nYOU LOST!";
                descriptionText.text = "TRY AGAIN!";
                exitBtnText.text = "Next time";
                advButtons.SetActive(true);
            }
            else
            {
                headerText.text = "OH NO!\nYOU LOST AGAIN!";
                descriptionText.text = "YOU SHOULD REST.\nNEXT TIME YOU WILL WIN!";
                exitBtnText.text = "OK";
                advButtons.SetActive(false);
            }
        }
        
        public override void OpenWindow()
        {
            RefreshAdvButton();
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            base.OpenWindow();
        }

        private void RefreshAdv(string s)
        {
            RefreshAdvButton();
        }
    
        private void RefreshAdvButton()
        {
            var advActivate = MaxAppLovin.Instance.IsRewardedAdReady;
            advAcivateBtn.SetActive(advActivate);
            advUnactivateBtn.SetActive(!advActivate);
        }

        public void OnBtnAdv()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
            
            EventManager.VibrateMedium.Invoke();
            MaxAppLovin.Instance.HiddenRevard.AddListener(TryAgainTournament);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }

        private void TryAgainTournament()
        {
            CloseWindow();
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(TryAgainTournament);
            ClearListeners();

            TournamentManager.Instance.TournamentAgainAction.Invoke();
        }
        
        public void OnEndBtn()
        {
            EventManager.VibrateLight.Invoke();
            ApplicationManager.Instance.SaveSystemRef.Save();
            TournamentManager.TournamentExit();
        }

        private void OnDestroy()
        {
            ClearListeners();
        }

        private void OnDisable()
        {
            ClearListeners();
        }

        private void ClearListeners()
        {
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshAdv;
        }
    }
}