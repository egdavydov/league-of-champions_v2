using System;
using Core.StatesMachine.States;
using Game.Managers;
using Game.States;
using Tournament.States;
using UnityEngine;

namespace UI.Tournament
{
    public class PrepareOverlay : MonoBehaviour
    {
        private PrepareState _state;
        
        private void Awake()
        {
            var activeGameState = (TournamentState) ApplicationManager.Instance.StatesManager.ActiveState;
            _state = (PrepareState) activeGameState.StatesMachine.ActiveState;
        }

        public void ShowNextWindow()
        {
            gameObject.SetActive(false);
            _state.NextState();
        }
    }
}