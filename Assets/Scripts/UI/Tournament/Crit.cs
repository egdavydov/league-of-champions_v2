﻿using Game.Managers;
using Tournament.Managers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UI.Tournament
{
    public class Crit : MonoBehaviour
    {
        private RectTransform _transform;
        private float _speed;
        private Animator _animator;
        public bool IsDead { get; private set; }
    
        private void Awake()
        {
            _transform = GetComponent<RectTransform>();
            _animator = GetComponent<Animator>();
            _speed = Random.Range(ApplicationManager.Instance.CollectionManagerRef.Base.TournamentCritMinSpeed, ApplicationManager.Instance.CollectionManagerRef.Base.TournamentCritMaxSpeed);
        }

        private void Update()
        {
            var position = _transform.position;
            if (position.y > 6f)
            {
                Destroy(gameObject);
            }
        
            position = new Vector3(position.x, position.y + Time.deltaTime * _speed);
            _transform.position = position;
        }

        public void Click()
        {
            if (IsDead)
                return;
            
            IsDead = true;
            _animator.SetTrigger("Play");
            TournamentManager.Instance.UI.clickCritAction.Invoke();
        }

        public void DestroySelf()
        {
            Destroy(gameObject);
        }
    }
}
