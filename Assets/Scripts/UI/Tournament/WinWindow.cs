﻿using Advertising;
using Game.Managers;
using Services;
using TMPro;
using Tournament.Managers;
using UI.Interfaces;
using UnityEngine;

namespace UI.Tournament
{
    public class WinWindow : BasicWindow
    {
        [SerializeField] private TextMeshProUGUI skillPointsText;
        [SerializeField] private TextMeshProUGUI skillPointsBonusMultiplierText;
        [SerializeField] private GameObject winProgressBar;
        [SerializeField] private GameObject normalSkillPointsImage;
        [SerializeField] private GameObject bonusSkillPointsImage;
        [SerializeField] private GameObject advAcivateBtn;
        [SerializeField] private GameObject advUnactivateBtn;

        private Animator _animator;
        private float _reward;
        private int _winCount;

        private void Start()
        {
            _animator = GetComponent<Animator>();
        
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
        }

        public void SetData(int skillPoints, int winCount, bool hasBonus, int bonusMultiplier)
        {
            _reward = skillPoints;
            skillPointsText.text = Reduction.ReductionNumber(_reward, 0, true);
            
            normalSkillPointsImage.SetActive(!hasBonus);
            bonusSkillPointsImage.SetActive(hasBonus);
            SetWinProgress(winCount);
            skillPointsBonusMultiplierText.text = $"x{bonusMultiplier}";
        }

        private void SetWinProgress(int value)
        {
            _winCount = value;
            var progressTransform = winProgressBar.transform;
            var childes = progressTransform.childCount;
            for (var i = 0; i < childes; i++)
            {
                progressTransform.GetChild(i).gameObject.SetActive(i<_winCount);
            }

            var lastProgress = progressTransform.GetChild(_winCount - 1);
            childes = lastProgress.childCount;
            for (var i = 0; i < childes; i++)
            {
                lastProgress.GetChild(i).gameObject.SetActive(false);
            }
        }

        public override void OpenWindow()
        {
            RefreshAdvButton();
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            base.OpenWindow();
        }

        public void WinProgressAnimate()
        {
            var progressTransform = winProgressBar.transform;
            progressTransform.GetChild(_winCount-1).GetComponent<Animator>().SetTrigger("Play");
        }

        private void RefreshAdv(string s)
        {
            RefreshAdvButton();
        }
    
        private void RefreshAdvButton()
        {
            var advActivate = MaxAppLovin.Instance.IsRewardedAdReady;
            advAcivateBtn.SetActive(advActivate);
            advUnactivateBtn.SetActive(!advActivate);
        }

        public void OnBtnAdv()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
            
            EventManager.VibrateMedium.Invoke();
            MaxAppLovin.Instance.HiddenRevard.AddListener(RewardAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }
    
        private void RewardAdv()
        {
            _reward *= 2;
            skillPointsText.text = Reduction.ReductionNumber(_reward, 0, true);

            MaxAppLovin.Instance.HiddenRevard.RemoveListener(RewardAdv);
            ClearListeners();

            advAcivateBtn.SetActive(false);
            advUnactivateBtn.SetActive(false);

            var skillPoints = skillPointsText.gameObject.transform.parent;
            var skillPointsPosition = skillPoints.position;
            skillPoints.position = new Vector3(skillPointsPosition.x, 0f, skillPointsPosition.z);
            
            var winCount = ApplicationManager.Instance.CurrentRoom.TournamentWinCount();
            FacebookEventSubscribers.LogTournamentWinX2Event(ApplicationManager.Instance.CurrentRoom.Number, winCount);
        }
    
        public void Reward()
        {
            EventManager.VibrateLight.Invoke();
        
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(_reward);
            ApplicationManager.Instance.SaveSystemRef.Save();
            
            TournamentManager.TournamentExit();
        }
        
        private void OnDestroy()
        {
            ClearListeners();
        }

        private void OnDisable()
        {
            ClearListeners();
        }

        private void ClearListeners()
        {
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshAdv;
        }
    }
}
