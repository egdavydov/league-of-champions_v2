using System.Linq;
using Game.Managers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UI.Tournament
{
    public class CritManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] critPrefabs;

        private float _generateTimer;
        private float _generateTimeout;
        private float _critLimit;

        private void Start()
        {
            _generateTimeout = ApplicationManager.Instance.CollectionManagerRef.Base.TournamentGenerateCritTimeout;
            _critLimit = ApplicationManager.Instance.CollectionManagerRef.Base.TournamentGenerateCritLimit;

            var tournamentGameCount = ApplicationManager.Instance.CollectionManagerRef.GetRoomsDB.Sum(room => room.TournamentCount);
            if (tournamentGameCount <= ApplicationManager.Instance.CollectionManagerRef.Base.TournamentCountDisableCrit)
            {
                _generateTimeout = float.MaxValue; 
                return;
            }
            
            AddRandomCrit();
        }

        private void Update()
        {
            _generateTimer += Time.deltaTime;

            if (!(_generateTimer >= _generateTimeout) || transform.childCount >= _critLimit) return;
            
            AddRandomCrit();
            _generateTimer = 0f;
        }

        public void Clear()
        {
            var childes = transform.childCount;
            for (var i = 0; i < childes; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

        private Vector3 GetRandomVector()
        {
            var vector = Random.insideUnitSphere * 3.2f;
            return new Vector3(vector.x, -3f, 0);
        }
        
        private void AddRandomCrit()
        {
            var critType = Random.Range(0, critPrefabs.Length);
            var randomPosition = GetRandomVector();
            Instantiate(critPrefabs[critType], randomPosition, Quaternion.identity, gameObject.transform);
        }
    }
}