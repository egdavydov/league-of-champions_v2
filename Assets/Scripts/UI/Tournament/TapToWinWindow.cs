﻿using UnityEngine;

namespace UI.Tournament
{
    public class TapToWinWindow : MonoBehaviour
    {
        [SerializeField] private GameObject overlay;
        private void AnimationEnd()
        {
            overlay.SetActive(false);
        }

        public void PlatAnimation()
        {
            GetComponent<Animator>().SetTrigger("Play");
        }
    
        public void CloseWindow()
        {
            GetComponent<Animator>().SetTrigger("Close");
        }
    }
}
