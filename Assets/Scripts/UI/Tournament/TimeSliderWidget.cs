using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Tournament
{
    public class TimeSliderWidget : MonoBehaviour
    {
        [SerializeField] private Slider leftSlider;
        [SerializeField] private Slider rightSlider;

        public float Value { private get; set;}

        private void Awake()
        {
            Value = 1f;
        }

        private void Update()
        {
            if(Math.Abs(leftSlider.value - Value) <= 0f)
                return;
            if(Math.Abs(rightSlider.value - Value) <= 0f)
                return;
            
            leftSlider.value = Value;
            rightSlider.value = Value;
        }
    }
}