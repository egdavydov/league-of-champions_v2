﻿using System;
using System.Collections;
using Game.Managers;
using Game.States;
using Tournament.Managers;
using Tutorial.Steps;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using Image = UnityEngine.UI.Image;

namespace UI.Tournament
{
    public class TournamentUI : MonoBehaviour
    {
        [SerializeField] private GameObject prepareOverlay;
        [SerializeField] private GameObject winOverlay;
        [SerializeField] private GameObject loseOverlay;
        [SerializeField] private GameObject topBar;
        [SerializeField] private GameObject tapOverlay;
        [SerializeField] private WinWindow winWindow;
        [SerializeField] private LoseWindow loseWindow;
        [SerializeField] private ScoresWidget scoresWidget;
        [SerializeField] private TimeSliderWidget timeSliderWidget;
        [SerializeField] private GameObject critManager;
        [SerializeField] private Animator gamerAnimator;
        [SerializeField] private ParticleSystem particles;
        [SerializeField] private GameObject computerImage;
        [SerializeField] private GameObject tapText;

        private ParticleSystem.MainModule _particlesMain;
        
        private const float _particlesDeltaSpeed = 0.2f;
        private const float _particlesMaxSpeed = 3.5f;
        private const float _particlesMinSpeed = .4f;
        private Coroutine _particlesCoroutine;
        private Camera _camera;
        
        public Action clickCritAction;
        private RoomsDB _roomDB;

        private void Awake()
        {
            var activeGameState = (TournamentState) ApplicationManager.Instance.StatesManager.ActiveState;
            activeGameState.TournamentManager.UI = this;

            HideAll();
            HideTopBar();
            _camera = Camera.main;
            _roomDB = ApplicationManager.Instance.CurrentRoom;
            computerImage.GetComponent<Image>().sprite = _roomDB.tournamentComputerImage; 
            
            TournamentManager.Instance.WinAction += OnWinHandler;
            TournamentManager.Instance.LoseAction += OnLoseHandler;
            
            _particlesMain = particles.main;
        }

        public void IncreaseParticlesSpeed()
        {
            _particlesMain.simulationSpeed += _particlesDeltaSpeed;
            if( _particlesCoroutine != null)
                StopCoroutine(_particlesCoroutine);

            if (_particlesMain.simulationSpeed > _particlesMaxSpeed)
                _particlesMain.simulationSpeed = _particlesMaxSpeed;

            _particlesCoroutine = StartCoroutine(DecreaseParticlesSpeed());
        }

        private IEnumerator DecreaseParticlesSpeed()
        {
            yield return new WaitForSeconds(.5f);
            _particlesMain.simulationSpeed -= _particlesDeltaSpeed*2;
            
            if (_particlesMain.simulationSpeed < _particlesMinSpeed)
                _particlesMain.simulationSpeed = _particlesMinSpeed;
            else
                _particlesCoroutine = StartCoroutine(DecreaseParticlesSpeed());
        }

        public void SetYourScores(int score)
        {
            scoresWidget.YourScore = score;
        }

        public void SetEnemyScores(int score)
        {
            scoresWidget.EnemyScore = score;
        }

        public void SetTimeSlider(float gameTime)
        {
            var value = 1 - Mathf.InverseLerp(0, ApplicationManager.Instance.CollectionManagerRef.Base.TournamentGameTime, gameTime);
            timeSliderWidget.Value = value;
        }

        public void ShowTopBar()
        {
            topBar.SetActive(true);
        }

        private void HideTopBar()
        {
            topBar.SetActive(false);
        }
        
        public void ShowPrepareOverlay()
        {
            HideAll();
            prepareOverlay.SetActive(true);
        }
        
        public void ShowWinOverlay()
        {
            HideAll();
            winOverlay.SetActive(true);
        }
        
        public void ShowLoseOverlay()
        {
            HideAll();
            loseOverlay.SetActive(true);
        }

        public void ShowTapToWin()
        {
            tapOverlay.SetActive(true);
            tapOverlay.GetComponentInChildren<TapToWinWindow>().PlatAnimation();
            
            if (ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is TournamentStep)
                ApplicationManager.Instance.TutorialSystem.TutorialWindowController.ShowTarget();
        }
        
        public void HideTapToWin()
        {
            tapOverlay.GetComponentInChildren<TapToWinWindow>().CloseWindow();
        }

        public void ShowCritManager(bool show)
        {
            critManager.SetActive(show);
            critManager.GetComponent<CritManager>().Clear();
        }
        
        public void HideAll()
        {
            prepareOverlay.SetActive(false);
            winOverlay.SetActive(false);
            loseOverlay.SetActive(false);
            tapText.SetActive(false);
        }

        public void GamerAnimate(bool play)
        {
            gamerAnimator.SetTrigger(play ? "Play" : "Stop");
            tapText.SetActive(play);
        }

        private void OnWinHandler()
        {
            var winCount = _roomDB.TournamentWinCount();
            var hasBonus = winCount == 5;
            var winSkillPoints = hasBonus
                ? _roomDB.tournamentWinSkillPoints * _roomDB.tournamentWinSkillPointsBonusMultiplier
                : _roomDB.tournamentWinSkillPoints; 
            winWindow.SetData(winSkillPoints, winCount, hasBonus, _roomDB.tournamentWinSkillPointsBonusMultiplier);
        }
        
        private void OnLoseHandler()
        {
            loseWindow.SetData(TournamentManager.Instance.LostCount < 2);
        }
        
        private void OnDestroy()
        {
            TournamentManager.Instance.WinAction -= OnWinHandler;
            TournamentManager.Instance.LoseAction -= OnLoseHandler;
        }
    }
}