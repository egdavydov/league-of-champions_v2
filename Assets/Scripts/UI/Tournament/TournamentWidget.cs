using System;
using Advertising;
using Game.Managers;
using Game.States;
using Services;
using UI.TournamentFillWindow;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Tournament
{
    public class TournamentWidget : MonoBehaviour
    {
        public readonly string TournamentFillPlayerPrefsKey = "TournamentFill";
        
        [SerializeField] private Slider _tournamenLoadSlider;
        [SerializeField] private GameObject _tournamentLoadState;
        [SerializeField] private GameObject _tournamentReadyState;
        [SerializeField] private Button _advButton;

        public RoomsDB RoomsDb { private get; set; }

        private Animator _animator;
        private float _minPercentShowAdv;
        private float _maxPercentShowAdv;
        private bool _showFillBtn;
        private bool _allowShowAdvBtn;
        private bool _showAdvByPercent;
            
        public bool TournamentReady { get; private set; }

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _tournamentLoadState.SetActive(true);
            _tournamentReadyState.SetActive(false);
            _minPercentShowAdv = ApplicationManager.Instance.CollectionManagerRef.Base.TournamentAdvFillMin;
            _maxPercentShowAdv = ApplicationManager.Instance.CollectionManagerRef.Base.TournamentAdvFillMax;
            _advButton.onClick.AddListener(ShowAdvBtnClicked);
            
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
        }

        private void Start()
        {
            _allowShowAdvBtn = (RoomsDb.Number == 0 && RoomsDb.TournamentCount > 0) || RoomsDb.Number != 0;
            RefreshAdvButton();
            UpdateTournamentSlider();
        }
        
        private void Update()
        {
            if(TournamentReady || !RoomsDb.Activate || ApplicationManager.Instance.CollectionManagerRef.Base.curLevel < 7)
                return;
                
            RoomsDb.TournamentReadyTimer += Time.deltaTime;
            //if (Time.frameCount % 30 == 0)
                UpdateTournamentSlider();
        }

        public void ClickTournamentEnterButton()
        {
            EventManager.VibrateLight.Invoke();
            ApplicationManager.Instance.SaveSystemRef.Save();
            ApplicationManager.Instance.StatesManager.ActiveState.Exit();
            ApplicationManager.Instance.StatesManager.SwitchTo((int) GameStates.TournamentState);
        }
        
        private void UpdateTournamentSlider()
        {
            _tournamenLoadSlider.value = Mathf.InverseLerp(0, RoomsDb.tournamentReadyTimeout, RoomsDb.TournamentReadyTimer);
            TournamentReady = _tournamenLoadSlider.value >= 1f;
            _showAdvByPercent = _tournamenLoadSlider.value >= _minPercentShowAdv &&
                                _tournamenLoadSlider.value <= _maxPercentShowAdv;
            
            if (_allowShowAdvBtn && !_showFillBtn && _showAdvByPercent)
            {
                _showFillBtn = true;
                RefreshAdvButton();
                Debug.Log("Tournament show adv button");
            }
            else if(_showFillBtn && !_showAdvByPercent)
            {
                _showFillBtn = false;
                RefreshAdvButton();
                Debug.Log("Tournament hide adv button");
            }
            
            if (!TournamentReady)
                return;

            _animator.SetTrigger("Play");
            _tournamentReadyState.SetActive(TournamentReady);
            _tournamentLoadState.SetActive(!TournamentReady);
            EventManager.TournamentRoomReady.Invoke(RoomsDb.Number);
        }

        public void ShowAdvBtnClicked()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;

            var isFirstFill = PlayerPrefs.GetInt(TournamentFillPlayerPrefsKey);
            if (isFirstFill == 0)
            {
                ApplicationManager.Instance.UISystem.Show(ViewType.TournamentFillView, 
                    new TournamentFillWindowParams(this)
                );
                return;
            }
            
            FacebookEventSubscribers.LogFillTournament(RoomsDb.Number);
            MaxAppLovin.Instance.HiddenRevard.AddListener(RewardAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }
        
        private void RefreshAdv(string s)
        {
            RefreshAdvButton();
        }
        
        private void RefreshAdvButton()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady)
            {
                _showFillBtn = false;
                return;
            }
            _advButton.gameObject.SetActive(_showFillBtn);
        }
        
        private void RewardAdv()
        {
            RoomsDb.TournamentReadyTimer = RoomsDb.tournamentReadyTimeout;
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(RewardAdv);
        }

        private void OnDestroy()
        {
            _advButton.onClick.RemoveListener(ShowAdvBtnClicked);
        }
    }
}