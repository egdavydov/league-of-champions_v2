using System;
using Advertising;
using Core.UI.Base;
using Game.Managers;
using Services;
using UnityEngine;

namespace UI.DailyBonusWindow
{
    public class DailyBonusWindowController : ViewController<DailyBonusWindow>
    {
        private bool _advViewed;
        public void ShowAdv()
        {
            MaxAppLovin.Instance.HiddenRevard.AddListener(AfterAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }

        private void AfterAdv()
        {
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(AfterAdv);
            _advViewed = true;
            View.AdvViewed();
        }
        
        public void GetReward()
        {
            if(!ApplicationManager.Instance.CollectionManagerRef.Base.AvailableDailyBonus())
                return;

            var dayReward = ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusRewards[ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusDays];
            if (_advViewed)
                dayReward *= 2;
            
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(dayReward);
            ApplicationManager.Instance.CollectionManagerRef.Base.LastDailyBonusTime = DateTime.Now.ToString();
            
            if (_advViewed)
                FacebookEventSubscribers.LogDailyBonusX2Event(ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusDays);
            else
                FacebookEventSubscribers.LogDailyBonusEvent(ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusDays);
            
            ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusDays++;
            
            if (ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusDays == 7)
                ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusDays = 0;

            _advViewed = false;
        }
    }
}