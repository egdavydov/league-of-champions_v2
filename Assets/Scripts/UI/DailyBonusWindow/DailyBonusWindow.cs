using System;
using System.Collections.Generic;
using Advertising;
using Core.UI.Base;
using Game.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.DailyBonusWindow
{
    public class DailyBonusWindow : View<DailyBonusWindowController, DailyBonusWindowParams>
    {
        [SerializeField] private Button _rewardBtn;
        [SerializeField] private Button _okBtn;
        [SerializeField] private Button _advRewardActiveBtn;
        [SerializeField] private GameObject _advRewardUnactiveBtn;
        [SerializeField] private GameObject _bonusContainer;
        
        private Animator _viewAnimator;
        private DailyBonusItem[] _items;
        private DailyBonusItem _itemActive;
        
        private void Awake()
        {
            _viewAnimator = GetComponent<Animator>();
            _items = _bonusContainer.GetComponentsInChildren<DailyBonusItem>();
        }
        
        public override void Show()
        {
            base.Show();
            DontDestroyOnLoad(this);

            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdvButtonOnLoad;
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton); 

            RefreshAdvButton();
            
            _okBtn.gameObject.SetActive(false);
            _rewardBtn.gameObject.SetActive(true);
            
            _rewardBtn.onClick.AddListener(RewardButtonClickHandler);
            _okBtn.onClick.AddListener(Close);
            _advRewardActiveBtn.onClick.AddListener(AdvButtonClickHandler);
            
            var hasDailyBonus = ApplicationManager.Instance.CollectionManagerRef.Base.AvailableDailyBonus();
            var lastDay = ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusDays;
            var rewards = ApplicationManager.Instance.CollectionManagerRef.Base.DailyBonusRewards;

            for (var i = 0; i < _items.Length; i++)
            {
                var day = i + 1;
                _items[i].SetText(rewards[i], day);
                
                if ( day <= lastDay)
                {
                    _items[i].SetComplete();
                    continue;
                }
                
                _items[i].SetDefault();                    
            }

            if (hasDailyBonus || lastDay == 0)
            {
                _itemActive = _items[lastDay];
                _itemActive.SetActive();
            }
            
            _viewAnimator.SetTrigger("Open");
        }

        public override void Close()
        {
            _rewardBtn.onClick.RemoveListener(RewardButtonClickHandler);
            _okBtn.onClick.RemoveListener(Close);
            _advRewardActiveBtn.onClick.RemoveListener(AdvButtonClickHandler);
            
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshAdvButtonOnLoad;
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton); 
            
            _viewAnimator.SetTrigger("Close");
        }

        private void AdvButtonClickHandler()
        {
            EventManager.VibrateMedium.Invoke();
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshAdvButtonOnLoad;
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton); 
            Controller.ShowAdv();
        }
        
        private void RewardButtonClickHandler()
        {
            EventManager.VibrateLight.Invoke();
            _itemActive.GetComponent<Animator>().SetTrigger("Play");
            _itemActive.OnAnimationComplete += ItemAnimationComplete;
            
            _rewardBtn.gameObject.SetActive(false);
            _advRewardActiveBtn.gameObject.SetActive(false);
            _advRewardUnactiveBtn.gameObject.SetActive(false);
        }

        private void ItemAnimationComplete()
        {
            _itemActive.OnAnimationComplete -= ItemAnimationComplete;
            _okBtn.gameObject.SetActive(true);
            _rewardBtn.gameObject.SetActive(false);
            _advRewardActiveBtn.gameObject.SetActive(false);
            _advRewardUnactiveBtn.gameObject.SetActive(false);
            Controller.GetReward();
        }

        private void RefreshAdvButtonOnLoad(string str)
        {
            RefreshAdvButton();
        }
        
        private void RefreshAdvButton()
        {
            var advActive = MaxAppLovin.Instance.IsRewardedAdReady;
            _advRewardActiveBtn.gameObject.SetActive(advActive);
            _advRewardUnactiveBtn.SetActive(!advActive);
        }

        public void AdvViewed()
        {
            _itemActive.SetX2();
            RewardButtonClickHandler();
        }
        
        public void WindowOpened()
        {
        }
    
        public void WindowClosed()
        {
        }
    }
}