using System;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI.DailyBonusWindow
{
    public class DailyBonusItem : MonoBehaviour
    {
        [SerializeField] private GameObject _commonState;
        [SerializeField] private GameObject _activeState;
        [SerializeField] private GameObject _completeState;
        [SerializeField] private TextMeshProUGUI _rewardText;
        [SerializeField] private TextMeshProUGUI _dayText;

        public Action OnAnimationComplete;
        
        public void SetText(int reward, int day)
        {
            _rewardText.text = reward.ToString();
            _dayText.text = $"DAY {day}";
        }

        public void SetX2()
        {
            var reward = int.Parse(_rewardText.text) * 2;
            _rewardText.text = reward.ToString();
        }
        
        public void SetDefault()
        {
            _dayText.gameObject.SetActive(true);
            _commonState.SetActive(true);
            _activeState.SetActive(false);
            _completeState.SetActive(false);
        }
        
        public void SetActive()
        {
            _dayText.gameObject.SetActive(true);
            _commonState.SetActive(true);
            _activeState.SetActive(true);
            _completeState.SetActive(false);
        }
        
        public void SetComplete()
        {
            _dayText.gameObject.SetActive(false);
            _commonState.SetActive(false);
            _activeState.SetActive(false);
            _completeState.SetActive(true);
        }

        public void AnimationComplete()
        {
            OnAnimationComplete.Invoke();
        }
    }
}