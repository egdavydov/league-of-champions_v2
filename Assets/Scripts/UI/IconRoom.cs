﻿using System.Collections;
using System.Collections.Generic;
using Game.Managers;
using Services;
using UnityEngine;
using UnityEngine.UI;

public class IconRoom : MonoBehaviour
{
    private Image _image;
    private Color _color;
    private WaitForSeconds _delay;
    private RoomsDB _roomDB;
    private Animator _anim;
    
    private void Start()
    {
        _delay = new WaitForSeconds(0.1f);
        _image = GetComponentInChildren<Image>();
        _anim = GetComponent<Animator>();

        EventManager.ChangeRoom.AddListener((i => Open(i)));
    }
    
    public void Open(int number)
    {
        _roomDB = ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(number);
        StartCoroutine(ChangeState());
    }
    private IEnumerator ChangeState()
    {
        if (_anim.GetBool("Open"))
        {
            _anim.SetBool("Open", false);
        }

        if (!_roomDB.Activate)
        {
            yield break;
        }

        while (!IsAnimationPlaying())
        {
            yield return _delay;
        }
        
        Generate();
        
        _anim.SetBool("Open", true);
    }

    private void Generate()
    {
        _image.sprite = _roomDB.LogoRoom;
        var color = _roomDB.ColorRoom;
        color.a = 0.8f;
        _image.color = color;

    }
    
    private bool IsAnimationPlaying()
    {        
        var animatorStateInfo = _anim.GetCurrentAnimatorStateInfo(0);
        if (animatorStateInfo.IsName("Close") || animatorStateInfo.IsName("Idle"))             
            return true;

        return false;
    }
    
}
