using UI.Interfaces;
using UnityEngine;

namespace UI
{
    public class BasicWindow : MonoBehaviour, IWindow
    {
        [SerializeField] private Animator _animator;

        private bool _isOpen;
        
        private void Awake()
        {
            if(_animator == null)
                _animator = GetComponent<Animator>();
        }
        
        public virtual void OpenWindow()
        {
            _animator.SetTrigger("Open");
        }

        public virtual void CloseWindow()
        {
            _animator.SetTrigger("Close");
        }

        public void WindowOpened()
        {
            if (!_isOpen)
                WindowOpenedHandler();
                
            _isOpen = true;
        }
        
        public void WindowClosed()
        {
            if (_isOpen)
                WindowClosedHandler();
                    
            _isOpen = false;
        }

        protected virtual void WindowOpenedHandler()
        {
        }
        
        protected virtual void WindowClosedHandler()
        {
        }
    }
}