﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class AnimationParticles : MonoBehaviour
{
    [SerializeField] private float m_delay = 0.5f;
    [SerializeField] private float m_shiftX = 0.5f;
    [SerializeField] private float m_shiftY = 0.5f;


    private Animator[] m_animators;
    private RectTransform[] m_arrows;
    private WaitForSeconds m_delayWait;

    private void Start()
    {
        m_delayWait = new WaitForSeconds(m_delay);
        m_animators = GetComponentsInChildren<Animator>();
        m_arrows = new RectTransform[m_animators.Length];

        for (int i = 0; i < m_animators.Length; i++)
        {
            m_arrows[i] = m_animators[i].GetComponent<RectTransform>();
        }
    }

    public void Play()
    {
        for (int i = 0; i < m_animators.Length; i++)
        {
            float posX = Random.Range(-m_shiftX, m_shiftX);
            float posY = Random.Range(0, m_shiftY);
            Vector2 pos = new Vector2(posX, posY);
            m_arrows[i].anchoredPosition = pos;
        }

        StartCoroutine(PlayAllAnimation());
    }

    private IEnumerator PlayAllAnimation()
    {
        for (int i = 0; i < m_animators.Length; i++)
        {
            m_animators[i].SetTrigger("Play");
            yield return m_delayWait;
        }
    }
}
