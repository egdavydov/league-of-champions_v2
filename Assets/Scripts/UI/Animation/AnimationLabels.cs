﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AnimationLabels : MonoBehaviour
{
    [SerializeField] private float m_positionShift;
    
    private Animator[] m_animators;
    private RectTransform[] m_labelPosition;
    private RectTransform[] m_animationPosition;
    private TextMeshProUGUI[] m_coinsTxt;
    
    private void Start()
    {
        m_animators = GetComponentsInChildren<Animator>();
        m_labelPosition = new RectTransform[m_animators.Length];
        m_animationPosition = new RectTransform[m_animators.Length];
        m_coinsTxt = GetComponentsInChildren<TextMeshProUGUI>();

        for (var i = 0; i < m_animators.Length; i++)
        {
            m_labelPosition[i] = m_animators[i].transform.GetChild(0).GetComponent<RectTransform>();
            m_animationPosition[i] = m_animators[i].GetComponent<RectTransform>();
        }
    }

    public void Play(float coins)
    {
        for (var i = 0; i < m_animators.Length; i++)
        {
            if (!(m_labelPosition[i].anchoredPosition.y <= -1f)) continue;
            
            ////random position
            var randomX = Random.Range(-m_positionShift, m_positionShift);
            var randomY = Random.Range(-m_positionShift, m_positionShift);
            var pos = new Vector2(randomX, randomY);
            m_animationPosition[i].anchoredPosition = pos;
            if(coins > 0f)
                m_coinsTxt[i].text = Reduction.ReductionNumber(coins, 0, true);
                
            ///play animation
            m_animators[i].SetTrigger("Play");
            return;
        }
    }
}