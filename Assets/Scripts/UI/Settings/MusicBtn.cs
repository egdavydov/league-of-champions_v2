﻿using System.Collections;
using System.Collections.Generic;
using Game.Managers;
using UnityEngine;

public class MusicBtn : AButtonSetting
{
    private bool _musicEnabled;
    
    protected override void Links()
    {
        _musicEnabled = PlayerPrefs.GetInt("Music") == 1;
    }

    protected override bool StartValue()
    {
        return _musicEnabled;
    }

    protected override void SetValue(bool value)
    {
        _musicEnabled = value;
        PlayerPrefs.SetInt("Music", (_musicEnabled ? 1 : 0));
        ApplicationManager.Instance.AudioManager.MusicEnabled = _musicEnabled;

        if (_musicEnabled)
        {
            ApplicationManager.Instance.AudioManager.UnMute();
        }
        else
        {
            ApplicationManager.Instance.AudioManager.Mute();
        }
        
    }
}
