﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationBtn : AButtonSetting
{
    //private Taptic _vibrate;
    
    protected override void Links()
    {
        //_vibrate = GameObject.Find("Taptic").GetComponent<Taptic>();
    }

    protected override bool StartValue()
    {
        return Taptic.IsTaptic;
    }

    protected override void SetValue(bool value)
    {
        //_vibrate.IsEnabled = value;
        Taptic.IsTaptic = value;
    }
}
