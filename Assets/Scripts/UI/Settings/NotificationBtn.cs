﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationBtn : AButtonSetting
{
    private NotificationTest _notification;
    
    protected override void Links()
    {
        _notification = GameObject.Find("Notification").GetComponent<NotificationTest>();
    }

    protected override bool StartValue()
    {
        return _notification.GetIsEnable;
    }

    protected override void SetValue(bool value)
    {
        _notification.SetIsEnable = value;
    }
}
