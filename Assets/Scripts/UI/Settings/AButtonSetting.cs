﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AButtonSetting : MonoBehaviour
{
    [SerializeField] private GameObject _onBtn;
    [SerializeField] private GameObject _ofBtn;

    protected abstract void Links();

    protected abstract bool StartValue();
    protected abstract void SetValue(bool value);

    private void Start()
    {
        Links();
        Refresh(StartValue());
    }
    
    private void Refresh(bool value)
    {
        _onBtn.SetActive(!value);
        _ofBtn.SetActive(value);
    }

    public void OnBtn(bool value)
    {
        SetValue(value);
        Refresh(value);
    }

}
