using System;
using Advertising;
using Game.Managers;
using TMPro;
using UI.Widgets;
using UnityEngine;

namespace UI
{
    public class MoneyMultiplierWindow : BasicWindow
    {
        [SerializeField] private TextMeshProUGUI _timeText;
        [SerializeField] private TextMeshProUGUI _nextTimeText;
        [SerializeField] private GameObject _progressBar;
        [SerializeField] private GameObject _advAcivateBtn;
        [SerializeField] private GameObject _advUnactivateBtn;
        [SerializeField] private GameObject _advButtons;
        [SerializeField] private GameObject _completeProgress;
        [SerializeField] private GameObject _completeBtn;
        [SerializeField] private MoneyMultiplierWidget _widget;

        private void Awake()
        {
            RefreshAdvButton();
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
        }
        
        private void RefreshAdv(string s)
        {
            RefreshAdvButton();
        }
        
        private void RefreshAdvButton()
        {
            var advActivate = MaxAppLovin.Instance.IsRewardedAdReady;
            _advAcivateBtn.SetActive(advActivate);
            _advUnactivateBtn.SetActive(!advActivate);
        }
        
        public void UpdateTimer(string timeText)
        {
            _timeText.text = timeText;
        }

        public void SetProgress(int progress, int nextTime)
        {
            _progressBar.GetComponent<RectTransform>().sizeDelta = new Vector2(75f*progress, _progressBar.GetComponent<RectTransform>().sizeDelta.y);
            _nextTimeText.text = $"+{nextTime}m";
        }

        public void SetComplete(bool isComplete)
        {
            _completeProgress.SetActive(isComplete);
            SetButtons(!isComplete);
        }

        public void SetButtons(bool advShowed)
        {
            _completeBtn.SetActive(!advShowed);
            _advButtons.SetActive(advShowed);
        }
        
        public void OnBtnAdv()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
        
            MaxAppLovin.Instance.HiddenRevard.AddListener(RewardAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }
    
        private void RewardAdv()
        {
            _widget.AddMultiplierTime();
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(RewardAdv);
        }

        private void OnDestroy()
        {
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshAdv;
        }
    }
}