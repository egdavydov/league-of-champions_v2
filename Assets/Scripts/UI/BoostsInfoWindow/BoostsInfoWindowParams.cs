﻿using System;
using Core.UI.Base;

public class BoostsInfoWindowParams : ViewParams
{
    public readonly ABoostWidget BoostWidget;
    public readonly Action Callback;
    public BoostsInfoWindowParams(ABoostWidget widget, Action callback)
    {
        BoostWidget = widget;
        Callback = callback;
    }
    
}
