﻿using Core.UI.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BoostsInfoWindow : View<BoostsInfoWindowController, BoostsInfoWindowParams>
{
    [SerializeField] private Button _buttonClose;
    [SerializeField] private Button _buttonActivate;
    [SerializeField] private GameObject _buttonInactiveContainer;
    [SerializeField] private TextMeshProUGUI _labelBoostName;
    [SerializeField] private TextMeshProUGUI _labelBoostShortDescription;
    [SerializeField] private TextMeshProUGUI _labelBoostDescription;
    
    [SerializeField] private GameObject _energeticContainer;
    [SerializeField] private GameObject _superdonatContainer;
    [SerializeField] private GameObject _concentrationContainer;
    [SerializeField] private Animator _viewAnimator;
        
    public override void Show()
    {
        base.Show();
        DontDestroyOnLoad(this);

        _labelBoostName.text = Controller.BoostName;
        _labelBoostShortDescription.text = Controller.BoostShortDescription;
        _labelBoostDescription.text = Controller.BoostDescription;

        var type = Params.BoostWidget.BoostType();
        _energeticContainer.SetActive(type == BoostsInfoWindowController.BoostType.Energetic);
        _superdonatContainer.SetActive(type == BoostsInfoWindowController.BoostType.Superdonat);
        _concentrationContainer.SetActive(type == BoostsInfoWindowController.BoostType.Concentration);
        
        _buttonClose.onClick.AddListener(Close);
        _buttonActivate.onClick.AddListener(ActivateButtonClickHandler);
        _viewAnimator.SetTrigger("Open");
    }

    public override void Close()
    {
        _buttonClose.onClick.RemoveListener(Close);
        _buttonActivate.onClick.RemoveListener(ActivateButtonClickHandler);
        _viewAnimator.SetTrigger("Close");
    }

    private void ActivateButtonClickHandler()
    {
        Controller.ActivateBoost();
        Close();
    }

    public void WindowOpened()
    {
    }
    
    public void WindowClosed()
    {
    }
}
