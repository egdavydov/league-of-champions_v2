﻿using System.Collections;
using System.Collections.Generic;
using Core.UI.Base;
using UnityEngine;

public class BoostsInfoWindowController : ViewController<BoostsInfoWindow>
{
    public enum BoostType
    {
        Energetic,
        Concentration,
        Superdonat
    }

    public string BoostName
    {
        get
        {
            var type = View.Params.BoostWidget.BoostType();
            return type == BoostType.Concentration ? "Concentration" : type == BoostType.Energetic ? "Energy drink" : "love of fans";
        }
    }
    
    public string BoostShortDescription
    {
        get
        {
            var type = View.Params.BoostWidget.BoostType();
            return type == BoostType.Concentration ? "PLAY BETTER!" : type == BoostType.Energetic ? "GET MORE LOVE!" : "BE FASTER!";
        }
    }
    
    public string BoostDescription
    {
        get
        {
            var type = View.Params.BoostWidget.BoostType();
            return type == BoostType.Concentration ? "WINRATE OF ALL PLAYERS BECOME 100%" : type == BoostType.Energetic ? "THE SPEED OF ALL PLAYERS INCREASES SIGNIFICANTLY" : "ALL FANS GIVE YOU A LOT OF DONATIONS";
        }
    }

    public void ActivateBoost()
    {
        PlayerPrefs.SetInt(View.Params.BoostWidget.BoostType().ToString(), 1);
        View.Params.Callback();
    }
}
