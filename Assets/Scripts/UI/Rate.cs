﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rate : MonoBehaviour
{
    [SerializeField] private Animator _thanks;
    private SelectRate _rate;
    
    public string ANDROID_RATE_URL = "market://details?id=com.TapOn.LeagueOfChampions";
    public string IOS_RATE_URL = "itms-apps://itunes.apple.com/app/idcom.TapOn.LeagueOfChampions";

    private void Start()
    {
        _rate = GetComponentInChildren<SelectRate>(true);
    }

    public void OnBtnRate()
    {
        if (_rate.GetStars > 3)
        {
            #if UNITY_ANDROID
                Application.OpenURL(ANDROID_RATE_URL);
            #elif UNITY_IOS
                Application.OpenURL(IOS_RATE_URL);
            #endif
            Debug.Log("Load URL");
        }
        else
        {
            _thanks.SetBool("Open", true);   
        }
    }
}