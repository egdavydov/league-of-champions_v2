using System;
using System.Linq;
using Game.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.GamerWindow
{
    public class GamerSelector : MonoBehaviour
    {
        [SerializeField] private GamerVariantItem[] _gamerVariantItems = new GamerVariantItem[3];

        public void SetGamerVariants(RoomsDB.GamerVariant[] gamerVariants)
        {
            for (var i = 0; i < gamerVariants.Length; i++)
            {
                var gamerVariant = gamerVariants[i];
                _gamerVariantItems[i].SetGamerData(gamerVariant);
                _gamerVariantItems[i].Init();
            }
        }

        public void SetActive()
        {
            var gamerBySkillPoints = _gamerVariantItems.FirstOrDefault(item =>
                item.GamerVariant.Type == RoomsDB.GamerVariant.Types.BySkillPoints);
            var gamerByCoins = _gamerVariantItems.FirstOrDefault(item =>
                item.GamerVariant.Type == RoomsDB.GamerVariant.Types.ByCoins);
            var gamerByAdv = _gamerVariantItems.FirstOrDefault(item =>
                item.GamerVariant.Type == RoomsDB.GamerVariant.Types.ByAdv);

            if (gamerBySkillPoints != null && gamerBySkillPoints.GamerVariant.Price <=
                ApplicationManager.Instance.CollectionManagerRef.Base.curSkillPoints)
            {
                gamerBySkillPoints.Select();
                return;
            }
            
            if (gamerByCoins != null && gamerByCoins.GamerVariant.Price <=
                ApplicationManager.Instance.CollectionManagerRef.Base.curCoins)
            {
                gamerByCoins.Select();
                return;
            }

            if (gamerByAdv != null)
                gamerByAdv.Select();
        }

        public RoomsDB.GamerVariant GetSelectedGamerVariant()
        {
            var selected = _gamerVariantItems.First(item => item.Selected);
            return selected.GamerVariant;
        }
    }
}