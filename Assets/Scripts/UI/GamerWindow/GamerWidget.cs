﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.GamerWindow
{
    public class GamerWidget : MonoBehaviour
    {
        [SerializeField] private Image m_avatar;
        [SerializeField] private TextMeshProUGUI m_name;
        [SerializeField] private TextMeshProUGUI m_winrate;

        private RoomsDB.Gamer _gamer;

        private void Start()
        {
            EventManager.ChangeSkill.AddListener(Refresh);
            EventManager.ChangePerks.AddListener(Refresh);
        }

        public void WriteIn(RoomsDB.Gamer gamer)
        {
            _gamer = gamer;

            m_avatar.sprite = _gamer.GetAvatar();
            m_name.text = _gamer.GetName();
            m_winrate.text = $"{_gamer.curWinRate}%";
        }

        private void Refresh()
        {
            _gamer.SetWinRate();
            m_winrate.text = "" + _gamer.curWinRate + "%";
        }

        private void OnDestroy()
        {
            EventManager.ChangeSkill.RemoveListener(Refresh);
            EventManager.ChangePerks.RemoveListener(Refresh);
        }
    }
}
