﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.Linq;
using Game.Managers;
using Services;

public class SkillWidget : MonoBehaviour
{
    [SerializeField] private Image m_icon;
    [SerializeField] private TextMeshProUGUI m_name;
    [SerializeField] private TextMeshProUGUI m_stepWinrate;
    [SerializeField] private TextMeshProUGUI m_level;
    [SerializeField] private TextMeshProUGUI m_price;

    [SerializeField] private GameObject m_allPurcgased;
    [SerializeField] private GameObject m_buyBtns;
    [SerializeField] private GameObject m_lockBtn;
    [SerializeField] private GameObject m_unLockBtn;
    
    private RoomsDB.Skill m_skill;
    private RoomsDB.Gamer m_gamer;
    private RoomsDB _roomDb;

    private void Start()
    {
        EventManager.ChangeCoins.AddListener(Refresh);
        EventManager.ChangeSkillPoints.AddListener(Refresh);
        EventManager.ChangeSkill.AddListener(Refresh);
        EventManager.ChangePerks.AddListener(Refresh);
    }

    public void WriteIn(Sprite sprite, RoomsDB.Skill skill, RoomsDB.Gamer gamer, RoomsDB roomDB)
    {
        m_skill = skill;
        m_gamer = gamer;
        _roomDb = roomDB;

        m_icon.sprite = sprite;
        m_name.text = m_skill.name;
        m_stepWinrate.text = "winrate +" + m_skill.stepWinRate + "%";

        Refresh();
    }

    private void Refresh()
    {
        m_level.text = m_skill.curLevel + "/4";
        m_price.text = Reduction.ReductionNumber(m_skill.curPrice, 0, true);

        var active = m_skill.curLevel == 4;
        m_allPurcgased.SetActive(active);
        m_buyBtns.SetActive(!active);

        if (active) { return; }

        active = m_skill.IsAvailableToBuy();
        m_unLockBtn.SetActive(active);
        m_lockBtn.SetActive(!active);
    }

    public void BuySkill()
    {
        if (m_skill.BySkillPoints)
        {
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(m_skill.curPrice * -1);            
        }
        else
        {
            ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(m_skill.curPrice * -1);
        }

        m_skill.curLevel++;
        m_skill.SetCurPrice(_roomDb, m_gamer);
        EventManager.ChangeSkill.Invoke();
        EventManager.VibrateMedium.Invoke();
        CheckGamerSkillFulled();
    }

    private void CheckGamerSkillFulled()
    {
        var allSkillFulled = m_gamer.skills.All(skill => skill.curLevel == 4);
        if(allSkillFulled)
        {
            FacebookEventSubscribers.LogUpgrade_Gamer_Max_N_MEvent(m_gamer.roomNumber, m_gamer.gamerNumber);
        }
    }

    private void OnDestroy()
    {
        EventManager.ChangeCoins.RemoveListener(Refresh);
        EventManager.ChangeSkillPoints.RemoveListener(Refresh);
        EventManager.ChangeSkill.RemoveListener(Refresh);
        EventManager.ChangePerks.RemoveListener(Refresh);
    }
}