﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.GamerWindow
{
    public class PerkWidget : MonoBehaviour
    {
        [SerializeField] private GameObject iconActive;
        [SerializeField] private GameObject iconInactive;
        [SerializeField] private Image iconBig;
        [SerializeField] private TextMeshProUGUI name;
        [SerializeField] private TextMeshProUGUI description;
        [SerializeField] private GameObject getWindow;
        
        private bool _active;

        public bool Active => _active;

        private PerksDB.Perk _perk;

        public void IsActive(bool value)
        {
            _active = value;
            Refresh();
            getWindow.SetActive(false);
        }

        public void Init(PerksDB.Perk perk)
        {
            _perk = perk;
            iconActive.GetComponent<Image>().sprite = _perk.iconActive;
            iconInactive.GetComponent<Image>().sprite = _perk.iconInactive;
        }

        private void Refresh()
        {
            iconActive.SetActive(_active);
            iconInactive.SetActive(!_active);
        }

        private void RefreshWindow()
        {
            name.text = _perk.name.Replace(" ", "\n");

            if (_perk.coinsVictoryMultiplier > 1f)
            {
                description.text = $"Every win\n<#1DF1FF>money x{_perk.coinsVictoryMultiplier}</color>";
            }
            else if(_perk.gamerWinrate > 0f)
            {
                description.text = $"Max <#1DF1FF>winrate +{_perk.gamerWinrate}%</color>\npermanent";
            }
            else if(_perk.bonusEveryVictory > 0 && _perk.bonusEveryVictoryMultiplier > 0f)
            {
                description.text = $"Every {_perk.bonusEveryVictory} win\n <#1DF1FF>money x{_perk.bonusEveryVictoryMultiplier}</color>";
            }

            iconBig.sprite = _perk.icon;
        }

        public void ShowGetPerkWindow()
        {
            RefreshWindow(); 
            getWindow.SetActive(true);
        }

        public void HideGetPerkWindow()
        {
            getWindow.SetActive(false);
        }
    }
}
