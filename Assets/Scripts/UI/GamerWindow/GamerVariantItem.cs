using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.GamerWindow
{
    public class GamerVariantItem : MonoBehaviour
    {
        [SerializeField] private Image _gamerAvatar;
        [SerializeField] private GameObject _selectBorder;
        
        private Button _button;
        private bool _isInitialized;
        public RoomsDB.GamerVariant GamerVariant { get; private set; }

        public bool Selected { get; private set; }

        public static Action<int> OnSelect;
        
        public void Init()
        {
            if(_isInitialized)
                return;
            
            _button = GetComponent<Button>();
            _button.onClick.AddListener(Select);
            OnSelect += UnSelect;

            _isInitialized = true;
        }

        public void SetGamerData(RoomsDB.GamerVariant gamerVariant)
        {
            GamerVariant = gamerVariant;
            _gamerAvatar.sprite = GamerVariant.Avatar;
            Selected = false;
            _selectBorder.SetActive(Selected);
        }

        public void Select()
        {
            Selected = true;
            _selectBorder.SetActive(Selected);
            OnSelect.Invoke(GamerVariant.Number);
        }

        private void UnSelect(int number)
        {
            if(number == GamerVariant.Number)
                return;

            Selected = false;
            _selectBorder.SetActive(Selected);
        }

        private void OnDestroy()
        {
            
            _button.onClick.RemoveListener(Select);
            OnSelect -= UnSelect;
        }
    }
}