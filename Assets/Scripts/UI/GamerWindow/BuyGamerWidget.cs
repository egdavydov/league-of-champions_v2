﻿using System;
using System.Linq;
using Advertising;
using Game.Managers;
using Services;
using TMPro;
using UnityEngine;

namespace UI.GamerWindow
{
    public class BuyGamerWidget : MonoBehaviour
    {
        [SerializeField] private GameObject m_lock;
        [SerializeField] private GameObject m_unLock;
        [SerializeField] private GameObject m_active;
        [SerializeField] private GameObject m_noActive;
        [SerializeField] private TextMeshProUGUI m_price;
        [SerializeField] private TextMeshProUGUI m_level;
    
        [SerializeField] private TextMeshProUGUI _gamerName;
        [SerializeField] private TextMeshProUGUI _gamerWinrate;
        [SerializeField] private GameObject _coinIcon;
        [SerializeField] private GameObject _skillPointIcon;
        [SerializeField] private GameObject _advIcon;
    
        [SerializeField] private GamerSelector _gamerSelector;

        private RoomsDB.Gamer _gamerDB;
        private RoomsDB.GamerVariant[] _gamerVariants;
        private RoomsDB.GamerVariant _selectedGamerVariant;
        private Animator m_animator;
        private int _numberRoom;

        [NonSerialized] public Action BuyGamer;

        private void Awake()
        {
            m_animator = GetComponent<Animator>();
            EventManager.ChangeCoins.AddListener(CheckPrice);
            EventManager.ChangeSkillPoints.AddListener(CheckPrice);
            GamerVariantItem.OnSelect += GamerVariantSelect;
        }

        private void OnEnable()
        {
            _gamerSelector.SetActive();
        }

        public void WriteIn (int numberRoom, RoomsDB.Gamer gamer)
        {
            _numberRoom = numberRoom;
            _gamerDB = gamer;
            _gamerVariants = _gamerDB.GetVariants();
            _gamerSelector.SetGamerVariants(_gamerVariants);

            m_lock.SetActive(false);
            m_unLock.SetActive(false);

            if (_gamerDB.accessLevel > ApplicationManager.Instance.CollectionManagerRef.Base.curLevel)
            {
                m_lock.SetActive(true);
                m_level.text = "LEVEL " + _gamerDB.accessLevel;
                return;
            }

            m_unLock.SetActive(true);
        }

        private void CheckPrice()
        {
            if(!gameObject.activeSelf)
                return;
        
            if(m_lock.activeSelf || _selectedGamerVariant == null)
                return;
        
            bool active;
            switch (_selectedGamerVariant.Type)
            {
                case RoomsDB.GamerVariant.Types.ByAdv:
                    active = MaxAppLovin.Instance.IsRewardedAdReady;
                    break;
                case RoomsDB.GamerVariant.Types.BySkillPoints:
                    active = _selectedGamerVariant.Price <= ApplicationManager.Instance.CollectionManagerRef.Base.curSkillPoints;
                    break;
                case RoomsDB.GamerVariant.Types.ByCoins:
                    active = _selectedGamerVariant.Price <= ApplicationManager.Instance.CollectionManagerRef.Base.curCoins;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        
            m_active.SetActive(active);
            m_noActive.SetActive(!active);
        }

        public void OnBuyGamerBtn()
        {
            EventManager.VibrateMedium.Invoke();
            switch (_selectedGamerVariant.Type)
            {
                case RoomsDB.GamerVariant.Types.BySkillPoints:
                    ApplicationManager.Instance.CollectionManagerRef.Base.ChangeSkillPoints(_selectedGamerVariant.Price * -1);
                    FacebookEventSubscribers.LogRoomBuyGamerBySkillPoints(_numberRoom, _gamerDB.gamerNumber);
                    break;
                case RoomsDB.GamerVariant.Types.ByCoins:
                    ApplicationManager.Instance.CollectionManagerRef.Base.ChangeCoins(_selectedGamerVariant.Price * -1);
                    FacebookEventSubscribers.LogRoomBuyGamerByCoins(_numberRoom, _gamerDB.gamerNumber);
                    break;
                case RoomsDB.GamerVariant.Types.ByAdv:
                    MaxAppLovin.Instance.HiddenRevard.AddListener(BuyGamerByAdv);
                    MaxAppLovin.Instance.ShowRewardedInterstitial();
                    return;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        
            ActivateGamer();
        }

        private void BuyGamerByAdv()
        {
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(BuyGamerByAdv);
            FacebookEventSubscribers.LogRoomBuyGamerByAdv(_numberRoom, _gamerDB.gamerNumber);
            ActivateGamer();
        }

        private void ActivateGamer()
        {
            _gamerDB.activate = true;
            _gamerDB.Variant = _selectedGamerVariant.Number;
            _gamerDB.SetWinRate();
            ApplicationManager.Instance.SaveSystemRef.Save();
            BuyGamer.Invoke();
        
            FacebookEventSubscribers.LogUnlock_New_Gamer_N_MEvent(_numberRoom, _gamerDB.gamerNumber);
        
            var gamer = ApplicationManager.Instance.RoomsManagerRef.GetGamer(_numberRoom, _gamerDB.gamerNumber);
            gamer.GamerPlace(ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(_numberRoom), _gamerDB);
        
            m_animator.SetTrigger("Close");
        }
    
        private void GamerVariantSelect(int number)
        {
            _selectedGamerVariant = _gamerVariants.First(variant => variant.Number == number);

            _gamerName.text = _selectedGamerVariant.Name;
            _gamerWinrate.text = $"{_selectedGamerVariant.StartWinRate}%";
        
            _coinIcon.SetActive(false);
            _skillPointIcon.SetActive(false);
            _advIcon.SetActive(false);

            if (_selectedGamerVariant.Type == RoomsDB.GamerVariant.Types.ByAdv)
            {
                _advIcon.SetActive(true);
                m_price.text = "FREE";
            }
            else
            {
                m_price.text = Reduction.ReductionNumber(_selectedGamerVariant.Price, 0, true);
            
                if (_selectedGamerVariant.Type == RoomsDB.GamerVariant.Types.BySkillPoints)
                {
                    _skillPointIcon.SetActive(true);
                }
                else
                {
                    _coinIcon.SetActive(true);
                }
            }
        
            CheckPrice();
        }

        private void OnDestroy()
        {
            EventManager.ChangeCoins.RemoveListener(CheckPrice);
            EventManager.ChangeSkillPoints.RemoveListener(CheckPrice);
            GamerVariantItem.OnSelect -= GamerVariantSelect;
        }
    }
}
