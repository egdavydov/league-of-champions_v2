﻿using Game.Managers;
using Services;
using Tutorial.Steps;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI.GamerWindow
{
    public class GamerWindow : BasicWindow
    {
        [SerializeField] private GamerWidget m_gamerWidget;
        [SerializeField] private SkillWidget[] m_skillWidgets;
        [SerializeField] private PerksWidget perksWidget;
        [SerializeField] private BuyGamerWidget m_buyGamerWidget;
        [SerializeField] private DismissGamerWidget _dismissGamerWidget;
        [SerializeField] private Button _dismissGamerBtn;

        private RoomsDB.Gamer _gamer;
        private RoomsDB _room;
        
        private void Start()
        {
            EventManager.OpenGamerWindow.AddListener(OpenWindow);
            _dismissGamerBtn.onClick.AddListener(DismissGamerClick);
            _dismissGamerWidget.OnDismissGamer += DismissGamer;
            m_buyGamerWidget.BuyGamer += Refresh;
        }

        private void OnDestroy()
        {
            EventManager.OpenGamerWindow.RemoveListener(OpenWindow);
            _dismissGamerBtn.onClick.RemoveListener(DismissGamerClick);
            _dismissGamerWidget.OnDismissGamer -= DismissGamer;
            m_buyGamerWidget.BuyGamer -= Refresh;
        }

        private void OpenWindow(RoomsDB room, RoomsDB.Gamer gamer)
        {
            _gamer = gamer;
            _room = room;
            Refresh();
            base.OpenWindow();
        }
        
        protected override void WindowOpenedHandler()
        {
            if (ApplicationManager.Instance.TutorialSystem != null && ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is IntroductionStep)
                ApplicationManager.Instance.TutorialSystem.TutorialWindowController.ShowTarget();
        }
        
        protected override void WindowClosedHandler()
        {
            if (ApplicationManager.Instance.TutorialSystem != null && ApplicationManager.Instance.TutorialSystem.GetActiveTutorialStep() is IntroductionStep)
                ApplicationManager.Instance.TutorialSystem.CompleteStep();
        }

        private void Refresh()
        {
            _dismissGamerWidget.gameObject.SetActive(false);
            
            m_gamerWidget.WriteIn(_gamer);
            for (var i = 0; i < m_skillWidgets.Length; i++)
            {
                m_skillWidgets[i].WriteIn(_room.IconSkills[i], _gamer.skills[i], _gamer, _room);
            }

            perksWidget.Init(_room, _gamer);
            
            if (_gamer.activate)
            {
                m_buyGamerWidget.gameObject.SetActive(false);
                _dismissGamerWidget.Init();
                
                var isFirstGamer = _gamer.roomNumber == 0 && _gamer.gamerNumber == 0;
                _dismissGamerBtn.gameObject.SetActive(!isFirstGamer);
            }
            else
            {
                m_buyGamerWidget.WriteIn(_room.Number, _gamer);
                m_buyGamerWidget.gameObject.SetActive(true);
                foreach (Transform child in m_buyGamerWidget.transform)
                {
                    child.gameObject.SetActive(true);
                }
                _dismissGamerBtn.gameObject.SetActive(false);
            }
        }

        private void DismissGamerClick()
        {
            EventManager.VibrateLight.Invoke();
            _dismissGamerWidget.Show(_gamer);
        }

        private void DismissGamer()
        {
            FacebookEventSubscribers.LogRoomDismissGamer(_room.Number, _gamer.gamerNumber);
            
            _gamer.Reset(_room);
            ApplicationManager.Instance.SaveSystemRef.Save();
            
            var gamer = ApplicationManager.Instance.RoomsManagerRef.GetGamer(_room.Number, _gamer.gamerNumber);
            gamer.GamerPlace(ApplicationManager.Instance.CollectionManagerRef.GetRoomDB(_room.Number), _gamer);
            
            Refresh();
        }
    }
}