using System;
using Advertising;
using UnityEngine;
using UnityEngine.UI;

namespace UI.GamerWindow
{
    public class DismissGamerWidget : MonoBehaviour
    {
        [SerializeField] private GamerWidget _gamerWidget;
        [SerializeField] private GameObject _activeDismissBtn;
        [SerializeField] private GameObject _inactiveDismissBtn;
        [SerializeField] private Button _dismissBtn;
        [SerializeField] private Button _cancelBtn;
        [SerializeField] private Button _dismissGamerBtn;

        private Animator _animator;
        private bool _isInitialized;

        public Action OnDismissGamer;
        
        public void Init()
        {
            if(_isInitialized)
                return;

            _animator = GetComponent<Animator>();
            RefreshAdvButton();
            MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
            
            _dismissBtn.onClick.AddListener(DismissBtnClick);
            _cancelBtn.onClick.AddListener(Close);
            _isInitialized = true;
        }
        
        private void RefreshAdv(string s)
        {
            RefreshAdvButton();
        }
        
        private void RefreshAdvButton()
        {
            var advActivate = MaxAppLovin.Instance.IsRewardedAdReady;
            _activeDismissBtn.SetActive(advActivate);
            _inactiveDismissBtn.SetActive(!advActivate);
        }

        private void DismissBtnClick()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
            
            EventManager.VibrateMedium.Invoke();
            MaxAppLovin.Instance.HiddenRevard.AddListener(DismissGamer);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }

        private void DismissGamer()
        {
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(DismissGamer);
            OnDismissGamer.Invoke();
        }

        public void Show(RoomsDB.Gamer gamer)
        {
            gameObject.SetActive(true);
            _gamerWidget.WriteIn(gamer);
            _animator.SetTrigger("Open");
            _dismissGamerBtn.gameObject.SetActive(false);
        }
        
        private void Close()
        {
            EventManager.VibrateLight.Invoke();
            _animator.SetTrigger("Close");
            _dismissGamerBtn.gameObject.SetActive(true);
        }

        private void OnDestroy()
        {
            _dismissBtn.onClick.RemoveListener(DismissBtnClick);
            _cancelBtn.onClick.RemoveListener(Close);
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent -= RefreshAdv;
        }
    }
}