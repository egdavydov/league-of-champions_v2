﻿using System;
using System.Linq;
using Advertising;
using Services;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI.GamerWindow
{
    public class PerksWidget : MonoBehaviour
    {
        [SerializeField] private PerkWidget[] _perks = new PerkWidget[3];
        [SerializeField] private PerksDB _perksDB;
        [SerializeField] private GameObject getPerkWindow;
        [SerializeField] private GameObject advAcivateBtn;
        [SerializeField] private GameObject advUnactivateBtn;
        [SerializeField] private GameObject window;
        
        private RoomsDB.Gamer _gamer;
        private RoomsDB _roomsDB;

        private bool _perksIsInit;
        private RectTransform _rectTransformWindow;

        public void Init(RoomsDB roomsDB, RoomsDB.Gamer gamer)
        {
            if (!_perksIsInit)
            {
                for(var i = 0; i < _perks.Length; i++)
                {
                    _perks[i].Init(_perksDB.perks[i]);
                }
                _rectTransformWindow = window.GetComponent<RectTransform>();
                EventManager.ChangeSkill.AddListener(CheckPerks);
                RefreshAdvButton();
                MaxAppLovin.Instance.CashRevard.AddListener(RefreshAdvButton);
                MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
                _perksIsInit = true;
            }
            
            _gamer = gamer;
            _roomsDB = roomsDB;
            CheckPerks();
        }

        private void UpdatePerks()
        {
            for (var i = 0; i < _perks.Length; i++)
            {
                _perks[i].IsActive(i < _gamer.Perks);
            }
        }
        
        private void CheckPerks()
        {
            if (_gamer.Perks > 0)
            {
                gameObject.SetActive(true);
                _rectTransformWindow.offsetMin = new Vector2(_rectTransformWindow.offsetMin.x, 185);
                _rectTransformWindow.offsetMax = new Vector2(_rectTransformWindow.offsetMax.x, -185);
                UpdatePerks();
            }
            else
            {
                gameObject.SetActive(false);
                _rectTransformWindow.offsetMin = new Vector2(_rectTransformWindow.offsetMin.x, 220);
                _rectTransformWindow.offsetMax = new Vector2(_rectTransformWindow.offsetMax.x, -220);
            }
            
            var allSkillFulled = _gamer.skills.All(skill => skill.curLevel == 4);
            if (allSkillFulled && _gamer.Perks < 3)
            {
                ShowGetNextPerkWindow();
            }
            else
            {
                HideGetPerkWindow();
            }
        }

        private void ShowGetNextPerkWindow()
        {
            _perks[_gamer.Perks].ShowGetPerkWindow();
        }

        private void HideGetPerkWindow()
        {
            getPerkWindow.SetActive(false);
        }

        private void RefreshAdv(string s)
        {
            RefreshAdvButton();
        }
        
        private void RefreshAdvButton()
        {
            var advActivate = MaxAppLovin.Instance.IsRewardedAdReady;
            advAcivateBtn.SetActive(advActivate);
            advUnactivateBtn.SetActive(!advActivate);
        }

        public void GetPerkAdv()
        {
            if (!MaxAppLovin.Instance.IsRewardedAdReady) return;
            
            EventManager.VibrateMedium.Invoke();
            MaxAppLovin.Instance.HiddenRevard.AddListener(RewardAdv);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }
    
        private void RewardAdv()
        {
            _gamer.AddPerk(_roomsDB);
            CheckPerks();
            UpdatePerks();
            
            FacebookEventSubscribers.LogPrestigeEvent(_gamer.roomNumber, _gamer.gamerNumber, _gamer.Perks);
            MaxAppLovin.Instance.HiddenRevard.RemoveListener(RewardAdv);
        }

        private void OnDestroy()
        {
            EventManager.ChangeSkill.RemoveListener(CheckPerks);
            MaxAppLovin.Instance.CashRevard.RemoveListener(RefreshAdvButton);
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += RefreshAdv;
        }
    }
}
