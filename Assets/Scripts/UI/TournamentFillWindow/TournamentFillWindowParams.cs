using Core.UI.Base;
using UI.Tournament;

namespace UI.TournamentFillWindow
{
    public class TournamentFillWindowParams : ViewParams
    {
        public TournamentWidget TournamentWidget;
        public TournamentFillWindowParams(TournamentWidget widget)
        {
            TournamentWidget = widget;
        }
    }
}