using Core.UI.Base;
using Game.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.TournamentFillWindow
{
    public class TournamentFillWindow : View<TournamentFillWindowController, TournamentFillWindowParams>
    {
        [SerializeField] private Button _exitBtn;
        [SerializeField] private Button _fillBtn;
        
        private Animator _viewAnimator;
        
        private void Awake()
        {
            _viewAnimator = GetComponent<Animator>();
            _exitBtn.onClick.AddListener(Close);
            _fillBtn.onClick.AddListener(GetReward);
        }

        private void Refresh()
        {
        }

        public override void Show()
        {
            base.Show();
            _viewAnimator.SetTrigger("Open");
        }

        private void GetReward()
        {
            PlayerPrefs.SetInt(Params.TournamentWidget.TournamentFillPlayerPrefsKey, 1);
            Params.TournamentWidget.ShowAdvBtnClicked();
            Close();
        }
        
        public override void Close()
        {
            _viewAnimator.SetTrigger("Close");
        }
        
        private void OnDestroy()
        {
            _exitBtn.onClick.RemoveListener(Close);
            _fillBtn.onClick.RemoveListener(Close);
        }
        
        public void WindowOpened()
        {
        }
    
        public void WindowClosed()
        {
        }
    }
}