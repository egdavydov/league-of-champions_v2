namespace UI.Interfaces
{
    public interface IWindow
    {
        void OpenWindow();
        void CloseWindow();
        void WindowOpened();
        void WindowClosed();
    }
}