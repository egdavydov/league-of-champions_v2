﻿using System.Collections;
using System.Collections.Generic;
using Advertising;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    [SerializeField] private Text _text;
    private Image _image;
    

    private void Start()
    {
        _image = GetComponent<Image>();
        Refresh();
        MaxAppLovin.Instance.CashRevard.AddListener(Refresh);
    }

    private void Refresh()
    {
        if (MaxAppLovin.Instance.IsPreloadingRewardedVideo)
        {
            _image.color = Color.green;
        }
        else
        {
            _image.color = Color.red;
        }
    }

    public void OnBtn()
    {
        if (MaxAppLovin.Instance.IsPreloadingRewardedVideo)
        {
            _text.text = "show video";
            MaxAppLovin.Instance.HiddenRevard.AddListener(Reward);
            MaxAppLovin.Instance.ShowRewardedInterstitial();
        }
    }

    private void Reward()
    {
        _text.text = "reward ok";
        MaxAppLovin.Instance.HiddenRevard.RemoveListener(Reward);
        MaxAppLovin.Instance.CashRevard.RemoveListener(Refresh);
    }
}
