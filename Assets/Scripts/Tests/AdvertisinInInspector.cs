﻿using System.Collections;
using System.Collections.Generic;
using Advertising;
using UnityEngine;
using UnityEngine.Events;
using GoogleMobileAds.Api;
using Services;

public class AdvertisinInInspector : MonoBehaviour
{
    [SerializeField] private SaveSystem _save;
    private bool _connection;
    [SerializeField] private GameObject _panel;
    public UnityEvent FinishAdv = new UnityEvent();

    public bool Connection => _connection;
    private void Awake()
    {
        _connection = _save.AdvConnecInInspecor;
        _panel.SetActive(false);
    }

    private void Start()
    {
        MobileAds.Initialize(initStatus => { });
    }

    private IEnumerator PlayAdvCor()
    {
        _panel.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        _panel.SetActive(false);
        FinishAdv.Invoke();
    }

    public void StartAdv()
    {
        if (MaxAppLovin.Instance.IsPreloadingRewardedVideo || MaxAppLovin.Instance.IsPreloadingInterstitialVideo)
        {
            StartCoroutine(PlayAdvCor());
        }
    }
}