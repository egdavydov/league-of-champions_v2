﻿using UnityEngine;
using System;
using System.Linq;
using Game.Managers;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "Room", menuName = "RoomDB")]
public class RoomsDB : ScriptableObject
{
    [Serializable]
    public class Skill
    {
        public string name;
        public float basePrice;
        public int stepWinRate;
        public int curLevel = 0;
        [NonSerialized] public float curPrice = 0;
        [NonSerialized] private bool _bySkillPoints;

        public void SetCurPrice(RoomsDB roomDB, Gamer gamer)
        {
            if (curLevel >= 4) return;

            var multiplier = BySkillPoints ? roomDB.priceMultiplierSkillPoint : roomDB.priceMultiplierSkill;
            var perkMultiplier = gamer.Perks > 0 ? Mathf.Pow(roomDB.priceSkillMultiplierPerk, gamer.Perks) : 1;
            curPrice = Mathf.Ceil(basePrice * (Mathf.Pow(multiplier, curLevel)) * perkMultiplier);
        }

        public bool IsAvailableToBuy()
        {
            return _bySkillPoints
                ? curPrice <= ApplicationManager.Instance.CollectionManagerRef.Base.curSkillPoints
                : curPrice <= ApplicationManager.Instance.CollectionManagerRef.Base.curCoins;
        }

        public bool BySkillPoints
        {
            get => _bySkillPoints;
            set => _bySkillPoints = value;
        }
    }

    [Serializable]
    public class Gamer
    {
        public string name;
        public Sprite avatar;
        public int accessLevel;
        public float accesPrice;
        public int startWinRate;
        public bool activate = false;
        public GamerVariant[] Variants = new GamerVariant[2];
        public Skill[] skills = new Skill[4];
        private int _perks;

        [NonSerialized] public float coinsVictory;
        [NonSerialized] public int curWinRate;
        [NonSerialized] public int gamerNumber;
        [NonSerialized] public int roomNumber;
        [NonSerialized] public float coinsInSecond;
        [NonSerialized] public float matchInSecond;
        [NonSerialized] public float winMatchInSecond;
        [NonSerialized] public int Variant;

        public int Perks
        {
            get => _perks;
            set => _perks = value > 3 ? 3 : value;
        }

        public void SetCoinsVictory(RoomsDB roomDB)
        {
            var bet = (float) Math.Round(
                roomDB.baseBetLeague *
                (float) Math.Pow(roomDB.baseMultipleBet, roomDB.curLevelLeague));

            var perkMultiplier = 1f;
            var multiplier = ApplicationManager.Instance.CollectionManagerRef.Base.isMultiplier ? 2f : 1f;

            if (_perks > 0)
            {
                var perkWinRateCoinsVictory =
                    ApplicationManager.Instance.CollectionManagerRef.Perks.perks.FirstOrDefault(perk =>
                        perk.coinsVictoryMultiplier > 1f);
                if (perkWinRateCoinsVictory != null)
                {
                    perkMultiplier =
                        _perks >= Array.IndexOf(ApplicationManager.Instance.CollectionManagerRef.Perks.perks,
                            perkWinRateCoinsVictory) + 1
                            ? perkWinRateCoinsVictory.coinsVictoryMultiplier
                            : perkMultiplier;
                }
            }

            coinsVictory = (float) Math.Round(bet * roomDB.curLevelSponsors * perkMultiplier * multiplier);
        }

        public void SetWinRate()
        {
            var addWinRate = skills.Sum(skill => skill.curLevel * skill.stepWinRate);
            var variantWinRate = Variant > 0 ? Variants.First(variant => variant.Number == Variant).WinRate : 0;
            var perkWinRateAdd = 0;

            if (_perks > 0)
            {
                var perkWinRate =
                    ApplicationManager.Instance.CollectionManagerRef.Perks.perks.FirstOrDefault(perk =>
                        perk.gamerWinrate > 0);

                if (perkWinRate != null)
                {
                    perkWinRateAdd =
                        _perks >= Array.IndexOf(ApplicationManager.Instance.CollectionManagerRef.Perks.perks,
                            perkWinRate) + 1
                            ? perkWinRate.gamerWinrate
                            : perkWinRateAdd;
                }
            }

            var winRate = startWinRate + variantWinRate;
            curWinRate = Mathf.Clamp(winRate + addWinRate + perkWinRateAdd, winRate, 100);
        }

        public void SetCoinsInSecond(RoomsDB roomDB)
        {
            matchInSecond = roomDB.curLevelSpeed /
                            ApplicationManager.Instance.CollectionManagerRef.Base.baseDelayBetweenMatches;

            var matchWinCoins = roomDB.baseBetLeague * (float) Math.Pow(roomDB.baseMultipleBet, roomDB.curLevelLeague) * roomDB.curLevelSponsors;
            coinsInSecond = matchWinCoins * matchInSecond * (curWinRate / 100f);

            winMatchInSecond = matchInSecond * (curWinRate / 100f);
        }

        public void AddPerk(RoomsDB roomDB)
        {
            Perks++;
            ResetSkills(roomDB);
            EventManager.ChangePerks.Invoke();
        }

        private void ResetSkills(RoomsDB roomDB)
        {
            foreach (var skill in skills)
            {
                skill.curLevel = 0;
                skill.SetCurPrice(roomDB, this);
            }
        }

        public void Reset(RoomsDB roomDB)
        {
            activate = false;
            ResetSkills(roomDB);
            Perks = 0;
            Variant = 0;
        }
        
        public string GetName()
        {
            return Variant > 0 ? Variants.First(variant => variant.Number == Variant).Name : name;
        }
        
        public Sprite GetAvatar()
        {
            return Variant > 0 ? Variants.First(variant => variant.Number == Variant).Avatar : avatar;
        }

        public GamerVariant[] GetVariants()
        {
            var variants = new GamerVariant[3];

            var baseVariant = new GamerVariant
            {
                Number = 0, Name = name, Avatar = avatar, Price = accesPrice, Type = GamerVariant.Types.ByCoins, StartWinRate = startWinRate
            };
            variants[0] = baseVariant;

            variants[1] = Variants[0];
            variants[1].StartWinRate = startWinRate + variants[1].WinRate;
            variants[1].Type = GamerVariant.Types.BySkillPoints;

            variants[2] = Variants[1];
            variants[2].StartWinRate = startWinRate + variants[2].WinRate;
            variants[2].Type = GamerVariant.Types.ByAdv;
            
            return variants;
        }
    }

    [Serializable]
    public class GamerVariant
    {
        public enum Types
        {
            ByCoins,
            BySkillPoints,
            ByAdv
        }
        
        public int Number;
        public string Name;
        public Sprite Avatar;
        public int WinRate;
        public float Price;
        [NonSerialized] public Types Type;
        [NonSerialized] public int StartWinRate;
    }
    
    [Serializable]
    public class Trainer
    {
        public bool Activate = false;
        public int TimeLimitLevel = 1;
        public int RevenueLevel = 1;
        public int TimeLimitBasePrice = 5;
        public float TimeLimitPriceMultiplier = 1.05f;
        public int RevenueBasePrice = 10;
        public float RevenuePriceMultiplier = 1.07f;
        public string Title;
        public string Name;
        public string Description;
        public int Price;

        public int GetTimeLimitLevel(BaseDB baseDb)
        {
            return GetOfflineEarningTimeLimit(baseDb) >= baseDb.OfflineEarningsMaxTimelimit ? -1 : TimeLimitLevel;
        }
        public int GetRevenueLevel(BaseDB baseDb)
        {
            return GetOfflineEarningRevenue(baseDb) >= baseDb.OfflineEarningsMaxPercent ? -1 : RevenueLevel;
        }
        
        public int GetOfflineEarningTimeLimit(BaseDB baseDb)
        {
            return (TimeLimitLevel - 1) * baseDb.OfflineEarningsTimelimitStep + baseDb.OfflineEarningsTimelimit;
        }
    
        public float GetOfflineEarningRevenue(BaseDB baseDb)
        {
            return (RevenueLevel - 1) * baseDb.OfflineEarningsPercentStep + baseDb.OfflineEarningsPercent;
        }
        
        public int GetUpgradeTimeLimitPrice()
        {
            return TimeLimitLevel <= 1 ? TimeLimitBasePrice : (int) Mathf.Ceil(TimeLimitBasePrice * TimeLimitPriceMultiplier * TimeLimitLevel);
        }
        public int GetUpgradeRevenuePrice()
        {
            return RevenueLevel <= 1 ? RevenueBasePrice : (int) Mathf.Ceil(RevenueBasePrice * RevenuePriceMultiplier * RevenueLevel);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    private int _number;

    [Header("General")] [SerializeField] private bool m_activate = false;
    [NonSerialized] public int advShow = 0;

    public int LevelAccess;
    public float PriceAccess;
    [SerializeField] private Sprite m_logoRoom;
    [SerializeField] private Color _colorRoom;
    public string Name;
    [NonSerialized] public int? CustomLogo;
    [NonSerialized] public string CustomTeamName;
    public Sprite[] IconSkills;

    public int TournamentCount { get; set; }
    public int TournamentWins { get; set; }
    public int TournamentWinsInRow { get; set; }
    public int TournamentLosesInRow { get; set; }
    public float TournamentReadyTimer { get; set; }

    public int TournamentWinCount()
    {
        var winsByFive = TournamentWins % 5;
        return winsByFive == 0 ? 5 : winsByFive;
    }

    [Header("Gamers Skills")]
    public float priceMultiplierSkill = 1.12f;
    public float priceMultiplierSkillPoint = 1.1f;
    [Header("Gamers Perks")]
    public float priceSkillMultiplierPerk = 1.5f;

    [Header("Speed Room Upgrade")]
    public float basePriceSpeed = 120f;
    public float priceMultiplierSpeed = 1.7f;
    [NonSerialized] public float stepSpeed = 0.5f;
    [NonSerialized] public float curLevelSpeed = 1f;
    [NonSerialized] public float maxLevelSpeed = 10f;
    [NonSerialized] public float CurPriceSpeed;
    [NonSerialized] public UnityEvent EventAddSpeed = new UnityEvent();

    [Header("Sponsors Room Upgrade")]
    public float basePriceSponsors = 85f;
    public float priceMultiplierSponsors = 1.26f;
    [NonSerialized] public float stepSponsors = 0.1f;
    [NonSerialized] public float curLevelSponsors = 1f;
    [NonSerialized] public float maxLevelSponsors = 5f;
    [NonSerialized] public float CurPriceSponsors;

    [Header("Rooms League")]
    public float basePriceLeague = 230f;
    public float priceMultiplierLeague = 1.43f;
    public float baseBetLeague = 5;
    public float baseMultipleBet = 1.25f;
    [NonSerialized] public float stepLeague = 1;
    [NonSerialized] public float curLevelLeague = 1f;
    [NonSerialized] public float maxLevelLeague = 25f;
    [NonSerialized] public float curBaseBetLeague;
    [NonSerialized] public float CurPriceLeague;
    [NonSerialized] public UnityEvent EventAddLeague = new UnityEvent();

    [Header("Room Tournament")]
    public float tournamentReadyTimeout = 90f;
    public int tournamentWinSkillPoints = 10;
    public int tournamentWinSkillPointsBonusMultiplier = 5;
    public float tournamentGamerClickTime = 3f;
    public Sprite tournamentComputerImage;

    public int Number
    {
        get => _number;
        set => _number = value;
    }

    public void AddLevelSpeed()
    {
        curLevelSpeed = (float) Math.Round(curLevelSpeed + stepSpeed, 1);
        EventAddSpeed.Invoke();
    }

    public void AddLevelLeague()
    {
        curLevelLeague += stepLeague;
        EventAddLeague.Invoke();
    }
    
    public void AddLevelSponsors()
    {
        curLevelSponsors = (float) Math.Round(curLevelSponsors + stepSponsors, 1);
    }

    public void SetCurBaseBetLeague()
    {
        curBaseBetLeague = (float) Math.Round(baseBetLeague * (float) Math.Pow(baseMultipleBet, curLevelLeague));
    }

    public void SetUpgradesPrice()
    {
        SetUpgradeSpeedPrice();
        SetUpgradeSponsorsPrice();
        SetUpgradeLeaguePrice();
    }
    
    public void SetUpgradeSpeedPrice()
    {
        var nextNumberUpgrade = (int) ((curLevelSpeed - 1) / stepSpeed);
        CurPriceSpeed = curLevelSpeed >= maxLevelSpeed ? float.MaxValue : basePriceSpeed * (float) Math.Pow(priceMultiplierSpeed, nextNumberUpgrade);
    }
    public void SetUpgradeLeaguePrice()
    {
        var nextNumberUpgrade = (int) curLevelLeague - 1;
        CurPriceLeague = curLevelLeague >= maxLevelLeague ? float.MaxValue : basePriceLeague * (float) Math.Pow(priceMultiplierLeague, nextNumberUpgrade);
    }
    public void SetUpgradeSponsorsPrice()
    {
        var nextNumberUpgrade = (int) Mathf.Round(curLevelSponsors*10f);
        nextNumberUpgrade -= 10;
        CurPriceSponsors = curLevelSponsors >= maxLevelSponsors ? float.MaxValue : basePriceSponsors * (float) Math.Pow(priceMultiplierSponsors, nextNumberUpgrade);
    }

    public bool Activate
    {
        get => m_activate;
        set => m_activate = value;
    }

    public Sprite LogoRoom => m_logoRoom;

    public Color ColorRoom => _colorRoom;

    [Header("Gamers in Room:")] [SerializeField]
    private Gamer[] m_gamers = new Gamer[6];

    public Gamer[] GetGamers => m_gamers;

    public Gamer GetGamerByNumber(int gameNumber)
    {
        return m_gamers[gameNumber];
    }

    [Header("Trainer:")] [SerializeField]
    private Trainer _trainer = new Trainer();
    public Trainer GetTrainer => _trainer;
}