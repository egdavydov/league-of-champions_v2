﻿using UnityEngine;
using System;

[Serializable]
public class CustomRoomLogo
{
    public int Id;
    public Sprite Image;
    public int Level;
    public int Price;
}
