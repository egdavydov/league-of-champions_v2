﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Perks", menuName = "PerksDB")]
public class PerksDB : ScriptableObject
{
    public Perk[] perks = new Perk[3];
    
    [Serializable]
    public class Perk
    {
        public string name;
        public float coinsVictoryMultiplier = 1f;
        public int gamerWinrate = 0;
        public int bonusEveryVictory = 0;
        public float bonusEveryVictoryMultiplier = 0f;
        public Sprite icon;
        public Sprite iconActive;
        public Sprite iconInactive;
    }
}