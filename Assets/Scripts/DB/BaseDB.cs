﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[CreateAssetMenu(fileName = "Base", menuName = "BaseDB")]
public class BaseDB : ScriptableObject
{
    [Header("AppLovin")] 
    public string SdkKey = "p-F7Uy0OEaOFb05FpHorQGUKHPrpyWEhRdK6S-kesZfhfWL7zbA-DHfIpRzhNozTirVs1PilH-HKqWxBUeJb2y";
    public string AndroidRV = "3eb28f7cb3667482";
    public string IosRv = "80693ba728b1d0fc";
    
    [Header("Advertisement widget")]
    public int AdvertisingReward = 10;
    public int TimePeriod = 43200;
    
    [Header("Interstitial Advertisement")]
    public string IosInterstitialAdId = "e4065feb60850dbc";
    public string AndroidInterstitialAdId = "ea086612062b05c2";
    public float DelayBetweenAd = 60f;
    
    [Header("LevelUp")]
    public float levelBonus = 100f;

    [Header("Match")]
    public float baseBet = 5f;
    public float baseDelayBetweenMatches = 5f;

    [Header("Coins")]
    public float coinsMultiplier = 1.35f;
    [NonSerialized] public float curCoins = 0f;
    
    [Header("SkillPoints")]
    [NonSerialized] public float curSkillPoints = 0f;

    [Header("Experience")]
    public float basePriceExperience = 30f;
    public float priceMultiplierExperience = 1.2f;
    [NonSerialized] public float curExperience = 0f;
    [NonSerialized] public int curLevel = 1;

    [Header("Viewers")]
    public int ViewersOnLeague = 100000;
    public int MinViewersInSecond = 100;
    public int MaxViewersInSecond = 200;

    [Header("Tournament")]
    public int TournamentGameTime = 15;
    public int TournamentEnemyWinRateLessDeltaMin = 5;
    public int TournamentEnemyWinRateLessDeltaMax = 10;
    public int TournamentEnemyWinRateLargerDeltaMin = 2;
    public int TournamentEnemyWinRateLargerDeltaMax = 5;
    public float TournamentCritMinSpeed = .8f;
    public float TournamentCritMaxSpeed = 2f;
    public float TournamentGenerateCritTimeout = 2f;
    public int TournamentGenerateCritLimit = 3;
    public int TournamentCountDisableCrit = 5;
    public float TournamentAdvFillMin = 0.1f;
    public float TournamentAdvFillMax = 0.9f;

    [Header("OfflineEarnings")]
    public int OfflineEarningsTimelimit = 120;
    public int OfflineEarningsMaxTimelimit = 2880;
    public int OfflineEarningsTimelimitStep = 30;
    public float OfflineEarningsPercent = .5f;
    public float OfflineEarningsMaxPercent = 1.5f;
    public float OfflineEarningsPercentStep = .02f;

    [Header("UpgradeRoom")]
    public int UpgradeRoomAdvShowed = 7;
    public int UpgradeRoomAdvRepeatMin = 30;
    public int UpgradeRoomAdvRepeatMax = 60;
    public int UpgradeRoomAdvLimitPrice = 10000;
    public float UpgradeRoomAdvStartPercent = 0.15f;
    public float UpgradeRoomAdvEndPercent = 0.25f;

    [Header("DailyBonus")] 
    public int[] DailyBonusRewards = {20, 50, 100, 150, 200, 250, 500};

    [Header("Room Customization")]
    public CustomRoomLogo[] CustomRoomLogos;
    public int NameMaxLength = 16;
    public List<int> PurchasedCustomLogos = new List<int>();

    [Header("Boosts configuration")]
    public float BoostsShowMinTime = 30f;
    public float BoostsShowMaxTime = 50f;
    
    [Header("Build settings")] 
    public string AppsFlyerIosAppId = "1467451991";
    public string AppsFlyerAppId = "zcKrZYJWnrWWctCxcLNnyT";
    
    [NonSerialized] public float SecondMoneyMultiplier;
    [NonSerialized] public int LastMoneyMultiplier;
    [NonSerialized] public string TimeStop;
    [NonSerialized] public float PriceNextLevel;
    [NonSerialized] public bool isMultiplier = false;
    [NonSerialized] public int DailyBonusDays;
    [NonSerialized] public string LastDailyBonusTime;
    [NonSerialized] public string LastUltraBonusTime;
    
    public void ChangeCoins(float coins)
    {
        curCoins = (float) Math.Round(curCoins + coins);
        EventManager.ChangeCoins.Invoke();
    }

    public void AddExperience(int value = 1)
    {
        curExperience += value;
        EventManager.ChangeExperience.Invoke();
    }

    public void AddLevel()
    {
        curLevel++;
        SetNextLevelPrice();
        EventManager.ChangeLevel.Invoke();
    }

    public void SetNextLevelPrice()
    {
        PriceNextLevel = (basePriceExperience * (float) Math.Pow(priceMultiplierExperience, curLevel + 1));
    }
    
    public void ChangeSkillPoints(float points)
    {
        curSkillPoints = (float) Math.Round(curSkillPoints + points);
        EventManager.ChangeSkillPoints.Invoke();
    }

    public bool AvailableDailyBonus()
    {
        if (curLevel < 3)
            return false;
        
        if (string.IsNullOrEmpty(LastDailyBonusTime))
            return true;
        
        var lastDailyBonusTime = DateTime.Parse(LastDailyBonusTime);
        var now = DateTime.Now;

        if ((now - lastDailyBonusTime).TotalHours < 24) return now.Day > lastDailyBonusTime.Day || now.Month > lastDailyBonusTime.Month;
        
        DailyBonusDays = 0;
        return true;
    }

    public CustomRoomLogo GetCustomLogo(int id)
    {
        return CustomRoomLogos.FirstOrDefault(logo => logo.Id == id);
    }
}