using System;
using System.Linq;
using Game.Managers;
using Services;
using UnityEngine;
using UnityEngine.Serialization;

namespace DB
{
    [CreateAssetMenu(fileName = "UltraBonus", menuName = "UltraBonusDB", order = 0)]
    public class UltraBonusDB : ScriptableObject
    {
        public int ResetHours = 3;
        public int FullResetHours = 12;
        public UltraBonus[] UltraBonuses = new UltraBonus[7];

        
        public enum UltraBonusType
        {
            Gold,
            SkillPoints,
            TimeJump
        }

        public bool UltraBonusFulled()
        {
            var inactiveBonus = UltraBonuses.FirstOrDefault(ultraBonus => !ultraBonus.active);
            return inactiveBonus == null;
        }
        
        [Serializable]
        public class UltraBonus
        {
            public int number;
            public int hour;
            public float reward;
            public UltraBonusType type;
            public bool active;

            private BaseDB _baseDb;
            private RoomsDB[] _roomsDbs;

            public void SetData(BaseDB baseDb, RoomsDB[] roomsDb)
            {
                _baseDb = baseDb;
                _roomsDbs = roomsDb;
            }
            
            public void ActivateBonus()
            {
                switch (type)
                {
                    case UltraBonusType.Gold:
                        _baseDb.ChangeCoins(reward);
                        break;
                    case UltraBonusType.SkillPoints:
                        _baseDb.ChangeSkillPoints(reward);
                        break;
                    case UltraBonusType.TimeJump:
                        _baseDb.ChangeCoins(reward);
                        var rewardExperience = (int) (_baseDb.PriceNextLevel - _baseDb.curExperience);
                        _baseDb.AddExperience(rewardExperience);
                        foreach (var roomDb in _roomsDbs)
                        {
                            if (!roomDb.Activate) continue;
                            roomDb.TournamentReadyTimer = roomDb.tournamentReadyTimeout;
                        }

                        break;
                }

                active = true;
                _baseDb.LastUltraBonusTime = DateTime.Now.ToString();
                FacebookEventSubscribers.LogRewardUltraBonus(number);
            }

            public void UpdateReward()
            {
                reward = 0f;
                var seconds = hour * 3600;

                foreach (var roomDb in _roomsDbs)
                {
                    if (!roomDb.Activate) continue;
                
                    var secondsMultiplierSeconds = Mathf.Clamp(_baseDb.SecondMoneyMultiplier, 0, seconds);
                    var secondsPassedEarnings = seconds - secondsMultiplierSeconds;

                    var roomCoins = 0f;
                    foreach (var gamer in roomDb.GetGamers)
                    {
                        if (!gamer.activate) continue;
                    
                        roomCoins += gamer.coinsInSecond * secondsPassedEarnings;
                        roomCoins += (gamer.coinsInSecond * 2) * secondsMultiplierSeconds;
                        roomCoins = Mathf.Clamp(
                            value: roomCoins,
                            min: roomDb.curBaseBetLeague * roomDb.curLevelSponsors,
                            max: float.MaxValue);
                    
                    }

                    reward += roomCoins;
                }

                reward = Mathf.Round(reward);
            }
        }
    }
}
