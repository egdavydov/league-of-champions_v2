﻿using System;
using System.Threading.Tasks;
using Core.Common.Extensions;
using Newtonsoft.Json;
using UnityEngine;

namespace Managers
{
    /**
     * Used for embedded params for client
     * Params will load from Resources folder
     */
    public sealed class EmbeddedParamsManager
    {
        public static EmbeddedParamsManager Instance => _instance ?? (_instance = new EmbeddedParamsManager());
        private static EmbeddedParamsManager _instance;
        
       // private Dictionary<string, Object> _cachedInstances = new Dictionary<string, Object>();
       
        public async Task<T> Get<T>() where T : class
        {
            try
            {
                var settingName = typeof(T).Name;
                var result = await Resources.LoadAsync($"EmbeddedConfigs/{settingName}") as TextAsset;
                if (result != null)
                {
                    var converted = JsonConvert.DeserializeObject<T>(result.text);
                    return converted;
                }
            }
            catch (Exception e)
            {
               Debug.LogException(e);
            }

            return default;
        } 
    }
    
}