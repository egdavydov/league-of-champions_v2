# Changelog

## 5.5.0.0
* Certified with SDK 5.5.0.

## 5.4.0.2
* Re-certified with SDK 5.4.0.
* Add support for initialization status.

## 5.4.0.1
* Revert back to depending on FAN SDK 5.3.2 as FAN SDK 5.4.0 depends on `FBSDKCoreKit.framework` which conflicts with manual integrations already bundling that framework in Unity. 

## 5.4.0.0
* Certified with SDK 5.4.0.
* Update logging.
* Better error code mapping.
* Remove custom initialization code for older versions of FAN (< 5.2.0).

## 5.3.2.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).

## 5.3.2.0
* Certified with SDK 5.3.2.
* Add support for extra reward options.

## 5.3.1.1
* Fix initialization related to last update.

## 5.3.1.0
* Certified with SDK 5.3.1.

## 5.2.0.3
* Fix using wrong AppLovin MAX callback when failing fullscreen ad displagy. Using `didFailToDisplayInterstitialAdWithError:` and `didFailToDisplayRewardedAdWithError:` now.

## 5.2.0.2
* Minor adapter improvements.

## 5.2.0.1
* Set mediation provider as APPLOVIN_X.X.X:Y.Y.Y.Y where X.X.X is AppLovin's SDK version and Y.Y.Y.Y is the adapter version.

## 5.2.0.0
* Support for FAN SDK 5.2.0.
* Use new FAN SDK initialization APIs.

## 5.1.1.1
* Synchronize usage of `FBAdSettings` as that is not thread-safe.

## 5.1.1.0
* Update to Facebook SDK 5.1.1 with fix for header bidding.

## 5.1.0.1
* Turn off Facebook SDK verbose logging.

## 5.1.0.0
* Initial commit.
