# Changelog

## 5.5.0.0
* Certified with SDK 5.5.0.

## 5.4.1.1
* Listen to Facebook's `onInterstitialActivityDestroyed()` callback for firing the [AD HIDDEN] callback.

## 5.4.1.0
* Certified with SDK 5.4.1.

## 5.4.0.1
* Better error code mapping; take into consideration FAN SDK creating new enums instead of using their pre-defined enums which breaks pointer equality.
* Add `com.android.support:appcompat-v7:28.+` to the Unity `Dependencies.xml` file.
* Add support for initialization status.

## 5.4.0.0
* Certified with SDK 5.4.0.
* Update logging.
* Better error code mapping.
* Remove custom initialization code for older versions of FAN (< 5.2.0).

## 5.3.1.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).

## 5.3.1.0
* Certified with SDK 5.3.1.
* Add support for extra reward options.

## 5.3.0.2
* Listen to Facebook's `onRewardedVideoActivityDestroyed()` callback for firing our [AD HIDDEN] and [REWARDED VIDEO COMPLETED] callbacks. Will not reward in this case, as it may be due to launching app from app icon and Facebook's Activity being destroyed by the OS.

## 5.3.0.1
* Check whether fullscreen ad has expired already before showing.

## 5.3.0.0
* Certified with Facebook Audience Network SDK 5.3.0 as it contains bidding improvements.

## 5.2.1.2
* Dynamically reference against Facebook SDK version number.

## 5.2.1.1
* In the Facebook adapters Unity Plugin, moved the dependencies bundled in 5.2.1.0 into `Assets/MaxSdk/Plugins/Android/Shared Dependencies`.

**Please delete the following from the `Assets/MaxSdk/Plugins/Android/Facebook` folder:**

    1. `exoplayer-core.aar`
    2. `exoplayer-dash.aar`
    3. `recyclerview-v7.aar`

## 5.2.1.0
* Certified with Facebook Audience Network SDK 5.2.1.
* Bundle in Unity Plugin the following Android dependencies:
    1. `exoplayer-core.aar`
    2. `exoplayer-dash.aar`
    3. `recyclerview-v7.aar`

## 5.2.0.1
* Set mediation provider as APPLOVIN_X.X.X:Y.Y.Y.Y where X.X.X is AppLovin's SDK version and Y.Y.Y.Y is the adapter version.

## 5.2.0.0
* Support for FAN SDK 5.2.0.
* Use new FAN SDK initialization APIs.

## 5.1.0.2
* Use unique package name in Android Manifest.

## 5.1.0.1
* Removed Redundant `activity` tags from AndroidManifest.

## 5.1.0.0
* Initial commit.
