# Changelog

## 17.2.1.5
* Add support for initialization status.

## 17.2.1.4
* Add back missing import in Unity's Post

## 17.2.1.3
* Fix leader ads using `AdSize.LARGE_BANNER` instead of `AdSize.LEADERBOARD`.

## 17.2.1.2
* Enforce versioning on Unity.

## 17.2.1.1
* Fix incorrect versioning.

## 17.2.1.0
* Certified with AdMob SDK version 17.2.1.
* Please make sure to have your Google Application ID declared in the Android Manifest file. For more information, please refer to our [documentation](https://dash.applovin.com/documentation/mediation/android/mediation-adapters)
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 15.0.1.3
* Minor adapter improvements.

## 15.0.1.2
* Use unique package name in Android Manifest.

## 15.0.1.1
* Fixed a rare crash caused due to AndroidManifest merge.

## 15.0.1.0
* Initial commit.
