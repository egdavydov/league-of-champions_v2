# Changelog

## 7.44.0.1
* Add support for initialization status.

## 7.44.0.0
* Certified with AdMob SDK 7.44.0.
* Adapter sets the `tagForChildDirectedTreatment` status on AdMob SDK before initializing it.
* Please make sure to have your Google Application ID declared in your app's `Info.plist`. For more information, please refer to our [documentation](https://dash.applovin.com/documentation/mediation/ios/mediation-adapters)
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 7.40.0.2
* Minor adapter improvements.

## 7.40.0.1
* Minor improvements.

## 7.40.0.0
* Certified with AdMob SDK 7.40.0.

## 7.36.0.0
* Initial commit.
