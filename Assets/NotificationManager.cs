﻿#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
#endif

public class NotificationManager 
{
    const string CHANNEL_ID = "channel_id";



    public static void SendNotification(int id, int delay, string title, string message, UnityEngine.Color32 color)
    {
#if UNITY_ANDROID
        AndroidNotificationChannel channel  = new AndroidNotificationChannel
        {
            Id = CHANNEL_ID,
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications"
        };
        AndroidNotificationCenter.RegisterNotificationChannel(channel);

        AndroidNotification notification = new AndroidNotification();
        notification.Title = title;
        notification.Text = message;
        notification.FireTime = System.DateTime.Now.AddSeconds(delay);
        notification.Color = color;

        AndroidNotificationCenter.SendNotificationWithExplicitID(notification, "channel_id", id);
        
#elif UNITY_IOS
        var iosNotification = new UnityEngine.iOS.LocalNotification
        {
            fireDate =  System.DateTime.Now.AddSeconds(delay), 
            alertBody = message,
            alertTitle = title,
        };
        UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(iosNotification);
#endif
    }


    public static void ClearNotifications()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidNotificationCenter.CancelAllNotifications();
#elif UNITY_IOS && !UNITY_EDITOR
        UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
        iOSNotificationCenter.RemoveAllScheduledNotifications();
#endif
    }
}
