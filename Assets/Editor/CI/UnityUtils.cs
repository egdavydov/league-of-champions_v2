﻿using UnityEngine;

namespace Core.Utils
{
    public class UnityUtils
    {
        public static string GetStreamingAssetsPath(bool needFile = true)
        {
            string path;
#if UNITY_EDITOR_OSX
        path = ( needFile ? "file://" : string.Empty)+ Application.dataPath + "/StreamingAssets/";
#elif UNITY_EDITOR
        path = ( needFile ? "file:" : string.Empty)+ Application.dataPath + "/StreamingAssets/";
#elif UNITY_ANDROID
		path = "jar:file://"+ Application.dataPath + "!/assets/";
#elif UNITY_IOS
		path = "file:" + Application.dataPath + "/Raw/";
#else
            //Desktop (Mac OS or Windows)
            path = (needFile ? "file:" : "") + Application.dataPath + "/StreamingAssets/";
#endif
            return path;
        }

        /// <summary>
        ///     Get right or left border of the screen into Vector3
        /// </summary>
        /// <param name="offset">Offset for border</param>
        /// <param name="isRight">Choose left or right border</param>
        /// <returns></returns>
        public static Vector3 GetScreenBorderIntoWorld(int offset, bool isRight = true)
        {
            return Camera.main.ScreenToWorldPoint(new Vector3((isRight ? Screen.width : 0) + offset,
                Screen.height * 0.5f, 0f));
        }
        
        public static string GetCommandLineArg(string name)
        {
            var args = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == name && args.Length > i + 1)
                {
                    return args[i + 1];
                }
            }
            
            return string.Empty;
        }
        
    }
    
}