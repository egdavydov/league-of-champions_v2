﻿using System;
using UnityEditor;

public class EditorCI 
{
    public static void BuildFromEditor(EditorBuildProcessArguments args)
    {
        switch (args.BuildTarget)
        {
            case BuildTarget.Android:
                BuildAndroid(args);
                break;
            case BuildTarget.iOS:
                BuildIos(args);
                break;
            case BuildTarget.StandaloneWindows:
            case BuildTarget.StandaloneWindows64:
                BuildWindows(args);
                break;
            case BuildTarget.StandaloneOSX:
                BuildMac(args);
                break;
            default:
                throw new TypeLoadException($"Invalid build target: {args.BuildTarget}");
        }
    }
    
    private static void BuildAndroid(EditorBuildProcessArguments args)
    {
        var process = new AndroidBuildProcess();
        process.SetEditorBuildProcessArguments(args);
        process.Build();
    }
    
    private static void BuildIos(EditorBuildProcessArguments args)
    {
        var process = new IosBuildProcess();
        process.SetEditorBuildProcessArguments(args);
        process.Build();
    }
    
    private static void BuildWindows(EditorBuildProcessArguments args)
    {
        var process = new WindowsBuildProcess();
        process.SetEditorBuildProcessArguments(args);
        process.Build();
    }
    
    private static void BuildMac(EditorBuildProcessArguments args)
    {
        var process = new MacBuildProcess();
        process.SetEditorBuildProcessArguments(args);
        process.Build();
    }
}
