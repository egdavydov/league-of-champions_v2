﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WindowsBuildProcess : BuildProcess
{
    private ApplicationPlayerSettings _appPlayerSettings;
    
    public override void Build()
    {
        Setup();
        SetupPlayerSettings();
        SetupLogTypes();
        SetupUnityDebugSettings();
        Refresh();
        base.Build();
    }

    protected override string GetPackageName()
    {
        var packageName = $"Software.Neurotech.{Application.productName}";
        return packageName;
    }

    protected override void Setup()
    {
        BuildTarget = BuildTarget.StandaloneWindows64;
        base.Setup();
    }
    
    protected override void SetupSymbols()
    {
        var symbols = BuildProcessArguments.BuildSymbols;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(GetBuildTargetGroup(), symbols);
    }
 
    protected override ApplicationPlayerSettings GetApplicationPlayerSettings()
    {
        if (_appPlayerSettings != null)
        {
            return _appPlayerSettings;
        }
        
        _appPlayerSettings = new ApplicationPlayerSettings(Application.productName, GetPackageName(), BuildProcessArguments.CompanyName, default(SigningSettings));

        return _appPlayerSettings;
    }

    protected override string GetProductName()
    {
        var productionName = ApplicationPlayerSettings.ProductName;
        return productionName;
    }

    protected override string GetFileName()
    {
        var behaviour = BuildProcessArguments.BehaviourType;
        
        var fileName = $"{Application.productName}.exe";
        Debug.Log("[BUILD PROJECT] Output file name: " + fileName);
        return $"/{fileName}";
    }

}
