﻿using System;
using UnityEditor;
using UnityEngine;

public class IosBuildProcess : BuildProcess 
{
    private ApplicationPlayerSettings _appPlayerSettings;
    
    public override void Build()
    {
        Setup();
        SetupPlayerSettings();
        SetupLogTypes();
        SetupUnityDebugSettings();
        Refresh();
        base.Build();
    }

    protected override string GetPackageName()
    {
        var packageName = $"com.TapOn.LeagueOfChampions";
        return packageName;
    }

    protected override void Setup()
    {
        BuildTarget = BuildTarget.iOS;
        base.Setup();
    }
    
    protected override void SetupPlayerSettings()
    {
        base.SetupPlayerSettings();
        PlayerSettings.iOS.buildNumber = BuildProcessArguments.BuildNumber.ToString();
        PlayerSettings.SetArchitecture(BuildTargetGroup.iOS, 1);
        PlayerSettings.iOS.targetOSVersionString = "9.0";
        PlayerSettings.iOS.appleDeveloperTeamID = GetTeamId();
    }
    
    protected override void SetupSymbols()
    {
        var symbols = BuildProcessArguments.BuildSymbols;
        DisableGooglePlayGamesForIOS(ref symbols);
        PlayerSettings.SetScriptingDefineSymbolsForGroup(GetBuildTargetGroup(), symbols);
    }
  
    protected override ApplicationPlayerSettings GetApplicationPlayerSettings()
    {
        if (_appPlayerSettings != null)
        {
            return _appPlayerSettings;
        }
     
        _appPlayerSettings = new ApplicationPlayerSettings("League Of Champions", GetPackageName(), BuildProcessArguments.CompanyName, default(SigningSettings));

        return _appPlayerSettings;
    }

    protected override string GetProductName()
    {
        var productionName = ApplicationPlayerSettings.ProductName;

        if (BuildProcessArguments.BehaviourType == BehaviourType.Production)
        {
            Debug.Log($"[BUILD PROJECT] Product name: {productionName}");
            return productionName;
        }

        var distribution = BuildProcessArguments.DistributionType;
        var behaviour = GetShortBehaviorType();
        var developmentName = $"Development Build";
        Debug.Log($"[BUILD PROJECT] Product name: {developmentName}");
        return developmentName;
    }

    protected override string GetFileName()
    {
        return string.Empty;
    }

    private string GetTeamId()
    {
        string teamId = string.Empty;
        Debug.Log($"[BUILD PROJECT] Team ID: {teamId}");
        return teamId;
    }

    private void DisableGooglePlayGamesForIOS(ref string buildSymbols)
    {
        if (!buildSymbols.Contains("NO_GPGS"))
        {
            buildSymbols += ";NO_GPGS";
        }
    }
}
