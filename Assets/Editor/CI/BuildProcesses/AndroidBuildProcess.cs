﻿using UnityEditor;
using UnityEngine;

public class AndroidBuildProcess : BuildProcess
{
    private ApplicationPlayerSettings _appPlayerSettings;
    private SigningSettings _cachedSigningSettings;

    public override void Build()
    {
        Setup();
        SetupPlayerSettings();
        SetupLogTypes();
        SetupUnityDebugSettings();
        Refresh();
        base.Build();
    }

    protected override string GetPackageName()
    {
        var packageName = $"com.TapOn.LeagueOfChampions";
        return packageName;
    }

    protected override void Setup()
    {
        BuildTarget = BuildTarget.Android;
        base.Setup();
    }
    
    protected override void SetupSymbols()
    {
        var symbols = BuildProcessArguments.BuildSymbols;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(GetBuildTargetGroup(), symbols);
    }
    
    protected override void OnBuildFailedHandler()
    {
        Refresh();
    }

    protected override ApplicationPlayerSettings GetApplicationPlayerSettings()
    {
        if (_appPlayerSettings != null)
        {
            return _appPlayerSettings;
        }
     
        _appPlayerSettings = new ApplicationPlayerSettings(Application.productName, GetPackageName(), BuildProcessArguments.CompanyName, default(SigningSettings));
        
        return _appPlayerSettings;
    }

    protected override void SetupPlayerSettings()
    {
        base.SetupPlayerSettings();
        PlayerSettings.Android.bundleVersionCode = BuildProcessArguments.BuildNumber;
        
        Debug.Log($"[BUILD PROJECT] Set scripting backend: {BuildProcessArguments.BackendType}");
        
        EditorUserBuildSettings.androidETC2Fallback = AndroidETC2Fallback.Quality16Bit;
        EditorUserBuildSettings.exportAsGoogleAndroidProject = BuildProcessArguments.ExportToAndroidProject;
        
        PlayerSettings.Android.keystoreName = ApplicationPlayerSettings.Signing.KeystoreName;
        PlayerSettings.Android.keystorePass = ApplicationPlayerSettings.Signing.KeystorePass;
        PlayerSettings.Android.keyaliasName = ApplicationPlayerSettings.Signing.KeyaliasName;
        PlayerSettings.Android.keyaliasPass = ApplicationPlayerSettings.Signing.KeyaliasPass;
    }

    protected override string GetFileName()
    {
        var distribution = BuildProcessArguments.DistributionType;
        var behaviour = GetShortBehaviorType();
        var version = BuildProcessArguments.GetBuildVersion();
        var fileName = $"{Application.productName} ({behaviour}){version}.apk";
        Debug.Log("[BUILD PROJECT] Output file name: " + fileName);
        return $"/{fileName}";
    }

    protected override string GetProductName()
    {
        var productionName = ApplicationPlayerSettings.ProductName;
        Debug.Log($"[BUILD PROJECT] Product name: {productionName}");
        return productionName;
    }

}
