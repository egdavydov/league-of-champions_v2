﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class BuildProjectEditorWindow : EditorWindow
{

    public enum BuildPlatform
    {
        Android,
        Ios
    }
    
    private const string PackagePattern = "com.TapOn.LeagueOfChampions";
    private const string CompanyName = "TapOn";
    
    private BuildPlatform _buildTarget = BuildPlatform.Android;
    private BuildProcess.DistributionType _distributionType = BuildProcess.DistributionType.Development;
    private BuildProcess.BehaviourType _behaviourType = BuildProcess.BehaviourType.Development;
    private ScriptingImplementation _spriptingBackend = ScriptingImplementation.IL2CPP;
    private BuildProcessArguments.TargetServerType _targetServer = BuildProcessArguments.TargetServerType.Prod;
    private string _version;
    private string _buildSymbols;
    private int _buildNumber = 0;
    private bool _debugEnabled = true;
    private bool _exportAndroid = false;
    private bool _cleanLastBuild = true;

    
    private GUIStyle ToggleButtonStyleNormal => GetNormalStyle();
    private GUIStyle ToggleButtonStyleToggled => GetToggledStyle();

    private GUIStyle _normalButton;
    private GUIStyle _toggledButton;

    [MenuItem("CI/Build Window %u", false, 400)]
    static void Init()
    {
        var window = (BuildProjectEditorWindow) GetWindow(typeof (BuildProjectEditorWindow), false, "Build Project");
        window.Show();
    }

    private GUIStyle GetToggledStyle()
    {
        if (_toggledButton != null) return _toggledButton;
        _toggledButton = new GUIStyle(ToggleButtonStyleNormal);
        _toggledButton.normal.background = _toggledButton.active.background;
        return _toggledButton;
    }
    
    private GUIStyle GetNormalStyle()
    {
        if (_normalButton != null) return _normalButton;
        _normalButton = "Button";
        return _normalButton;
    }

    private void DrawToggleButton(ref bool val, string name)
    {
        var oldBackgorundColor = GUI.backgroundColor;
        GUI.backgroundColor = val ? Color.green : oldBackgorundColor;
        val = GUILayout.Toggle(val, name, val ? ToggleButtonStyleNormal : ToggleButtonStyleToggled);
        GUI.backgroundColor = oldBackgorundColor;
    }

    private void OnGUI()
    {
      
        GUILayout.Space(15);
        
        _buildTarget = (BuildPlatform)EditorGUILayout.EnumPopup("Target platform", _buildTarget);
//        GUILayout.Space(5);
//        _distributionType = (BuildProcess.DistributionType)EditorGUILayout.EnumPopup("Distribution type", _distributionType);
        GUILayout.Space(5);
        _behaviourType = (BuildProcess.BehaviourType)EditorGUILayout.EnumPopup("Behaviour type", _behaviourType);
        GUILayout.Space(5);
        _spriptingBackend = (ScriptingImplementation)EditorGUILayout.EnumPopup("Scripting implementation", _spriptingBackend);
        GUILayout.Space(5);
        _version = EditorGUILayout.TextField("Build version:", _version);
        GUILayout.Space(5);
        _buildNumber = EditorGUILayout.IntField("Build number (optional):", _buildNumber);
        GUILayout.Space(15);
        
        GUILayout.Label("BUILD SYMBOLS", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
            //DrawToggleButton(ref _helpshiftDisabled, "Disable Helpshift");
            //DrawToggleButton(ref _cheatsEnabled, "Cheats");
            //DrawToggleButton(ref _debugPanelEnabled, "Debug Panel");
            //DrawToggleButton(ref _realInApps, "Real In Apps");
            //DrawToggleButton(ref _qaBuild, "QA");
            //DrawToggleButton(ref _isPtr, "PTR");
            //DrawToggleButton(ref _noGpgs, "No GPGS");
            SetProductionSettings();
        GUILayout.EndHorizontal();
        
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
            GUILayout.Label( ConstructBuildSymbols(), EditorStyles.label);
        GUILayout.EndHorizontal();
        
        //_prodGroup = _behaviourType == BuildProcess.BehaviourType.Production || _behaviourType == BuildProcess.BehaviourType.PreProduction;
        
        GUILayout.Space(15);
        GUILayout.Label("PRODUCTION SETTINGS", EditorStyles.boldLabel);
        /*GUI.enabled = _prodGroup;
            _targetServer = (BuildProcessArguments.TargetServerType)EditorGUILayout.EnumPopup("Target server", _targetServer);
            GUILayout.Space(5);
            _gitCommit = EditorGUILayout.TextField("Commit Hash:", _gitCommit);
            GUILayout.Space(5);

            var branch = $"{defaultBranch}{GetBranch()}";
            _gitBranch = EditorGUILayout.TextField($"Branch: {branch}", _gitBranch);
           
            GUILayout.Space(5);
            _sharedLogicVersion = EditorGUILayout.TextField("Shared Logic Version:", _sharedLogicVersion);
            GUILayout.Space(5);
            _statProject = EditorGUILayout.TextField("Stat Project:", _statProject);
            GUILayout.Space(5);
        
        GUI.enabled = true;*/

        GUILayout.Space(15);
        GUILayout.Label("SETTINGS", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
            DrawToggleButton(ref _debugEnabled, "Is Debuggable");
            GUI.enabled = _buildTarget == BuildPlatform.Android;
                DrawToggleButton(ref _exportAndroid, "Export Android Project");
            GUI.enabled = true;
            DrawToggleButton(ref _cleanLastBuild, "Remove last build");
        GUILayout.EndHorizontal();
        
//        GUILayout.Label("Custom connections", EditorStyles.miniLabel);
        //_deviceId = EditorGUILayout.TextField("Device ID", _deviceId);
        
        GUILayout.Space(20);
        if (GUILayout.Button("BUILD"))
        {
            Build();
        }
    }

    private void SetProductionSettings()
    {
        if (_behaviourType != BuildProcess.BehaviourType.Production) return;
        _debugEnabled = false;
    }

    private string ConstructBuildSymbols()
    {
        var symbols = string.Empty;

        if (_behaviourType == BuildProcess.BehaviourType.Development)
        {
            symbols += "DEVELOPMENT;";
        }
        
        if (_behaviourType == BuildProcess.BehaviourType.Production)
        {
            symbols += "PRODUCTION;";
        }

        symbols = symbols.TrimEnd(';');
        
        return symbols;
    }

    private void Build()
    {
        if (_cleanLastBuild)
        {
            var directoryPath = Application.dataPath.Replace("/Assets", string.Empty);
            var buildPath = directoryPath + "/Build";
            
            Debug.Log(buildPath);
            
            if (Directory.Exists(buildPath))
            {
                Directory.Delete(buildPath, true);
                Debug.Log("Build directory removed!");
            }
        }

        var systemBuildTarget = _buildTarget == BuildPlatform.Android
            ? BuildTarget.Android
            : _buildTarget == BuildPlatform.Ios ? BuildTarget.iOS : BuildTarget.StandaloneWindows64;

        if (EditorUserBuildSettings.activeBuildTarget != systemBuildTarget)
        {
            if (!EditorUtility.DisplayDialog("Switch platform?", "You need to switch to " + _buildTarget + ". Start switching now?", "Ok, start reimport", "No"))
            {
                return;
            }
    
#pragma warning disable 618
            EditorUserBuildSettings.SwitchActiveBuildTarget(systemBuildTarget);
#pragma warning restore 618
        }
    
        _buildSymbols = ConstructBuildSymbols();
        
        var preparedArguments = new EditorBuildProcessArguments
        {
            BuildTarget = systemBuildTarget,
            IsDebug = _debugEnabled,
            DistributionType = _distributionType,
            BuildSymbols = _buildSymbols,
            BuildNumber = _buildNumber,
            BackendType = _spriptingBackend,
            BehaviourType = _behaviourType,
            CompanyName = CompanyName,
            PackagePattern = PackagePattern,
            ExportToAndroidProject = _exportAndroid,
            Version = _version
        };
        
        EditorCI.BuildFromEditor(preparedArguments);
    }
}